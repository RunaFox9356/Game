//=============================================================================
//
// BOSS
// Author:hamada ryuuga
//
//=============================================================================

#include "boss.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "stage_imgui.h"
#include "reaction.h"
#include "manager.h"
//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBoss::CBoss()
{
	m_nouevent = 0;
	m_frameCount = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBoss::~CBoss()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBoss::Init()
{
	// 現在のモーション番号の保管
	CEnemy::Init();
	SetUp(CObject::ENEMY);
	m_hit = false;
	m_isMove = false;

	for (int i = 0; i < 12; i++)
	{
		if (Line[i] != nullptr)
		{
			Line[i] = nullptr;
		}
	}


	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CBoss::Uninit()
{
	CEnemy::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBoss::Update()
{
	//CEnemy::Update();

	Move();

	CPlayer*Player = CGame::GetStage()->GetPlayer();
	D3DXVECTOR3  pPos = Player->GetPos();
	D3DXVECTOR3  pPosOld = Player->GetPosOld();
	D3DXVECTOR3  pSize = Player->GetSize();
	CollisionModel(&pPos, &pPosOld, &pSize);
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CBoss::Draw()
{
	CEnemy::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBoss *CBoss::Create(D3DXVECTOR3 pos)
{
	CBoss *pEnemy = nullptr;

	pEnemy = new CBoss;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::BOSS);
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CBoss::Move()
{
	if (GeteventListSize() != 0 && GetBossMoveTypetSize() != 0)
	{//eventリストがゼロじゃなかったら起こる
		m_frameCount++;

		if (m_frameCount >= GeteventList(m_nouevent))
		{
			switch (GetBossMoveType(m_nouevent))
			{
			case CBoss::NONE:
				break;
			case CBoss::METEO:
				break;
			case CBoss::MOVE:
				break;
			case CBoss::MAXBOSS:
				break;
			default:
				break;
			}

			if (m_nouevent >= GeteventListSize())
			{
				m_nouevent = 0;
			}
			m_frameCount = 0;
		}
	}
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CBoss::Hit()
{
	CStageImgui* imgui = CGame::GetStageImgui();
	imgui->ImguiEnemy();
}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CBoss::NotHit()
{
	m_hit = false;
}

//--------------------------------------------------
// Bossの動きのセーブ
//--------------------------------------------------
void CBoss::BossEventSeve()
{
	SetCurrentDirectory(CManager::GetdefaultPath());
	int nIndex = 0;

	for (int meshCount = 0; meshCount < GeteventListSize(); meshCount++)
	{
		std::string name = "BOSSEVENT";
		std::string Number = std::to_string(nIndex);
		name += Number;
	
		JBoss[name] = {
			{ "EVENTLIST", GeteventList(nIndex) } ,
			{ "BOSSMOVETYPE", GetBossMoveType(nIndex) } };

		nIndex++;
	}
	JBoss["INDEX"] = nIndex;
	auto jobj = JBoss.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = "data\\BOSS\\KUMA.json";
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();
}

//--------------------------------------------------
// Bossの動きのロード
//--------------------------------------------------
void CBoss::BossEventLood()
{
	SetCurrentDirectory(CManager::GetdefaultPath());
	
	std::ifstream ifs("data\\BOSS\\KUMA.json");

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JBoss;
		nIndex = JBoss["INDEX"];
		int EventList = 0;
		int ModelType = 0;

		for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
		{
			std::string name = "BOSSEVENT";
			std::string Number = std::to_string(nCntEnemy);
			name += Number;

			EventList = JBoss[name]["EVENTLIST"];
			ModelType = JBoss[name]["BOSSMOVETYPE"];

			AddeventList(EventList);
			AddeBossMoveType((BOSSMOVETYPE)ModelType);
		}
	}
}
