//=============================================================================
//
// 反応設定
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _REACTION_H_			// このマクロ定義がされてなかったら
#define _REACTION_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "3dpolygon.h"
#include "texture.h"

class CReaction : public C3dpolygon
{
public:


	static CReaction *CReaction::Create();

	CReaction(const int list);
	~CReaction() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	const D3DXVECTOR3 *GetPos() const override;
	void SetPos(const D3DXVECTOR3 &pos) override;


private:
	void move();
	D3DXVECTOR3 m_Move;

};

#endif

