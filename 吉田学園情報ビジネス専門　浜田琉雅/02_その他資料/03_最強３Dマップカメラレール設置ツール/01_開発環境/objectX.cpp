//==================================================
// objectX.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "objectX.h"
#include "renderer.h"
#include "camera.h"
#include "light.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CObjectXManager* CObjectXManager::ms_ObjectXManager = nullptr;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CObjectX::CObjectX(int nPriority /* =0 */) : CObject(nPriority)
{
	m_scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	// 位置
	m_posOrigin = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 向き
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 最小値
	m_vtxMinModel = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	// 最大値
	m_vtxMaxModel = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	// 大きさ
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	SetQuat(false);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CObjectX::~CObjectX()
{
	//assert(m_pVtxBuff == nullptr);
	//assert(m_mesh == nullptr);
	//assert(m_buffMat == nullptr);
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CObjectX::Init()
{
	

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CObjectX::Uninit()
{
	if (m_modelXData != nullptr)
	{
		delete m_modelXData;
		m_modelXData = nullptr;
	}

	Release();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CObjectX::Update()
{
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CObjectX::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->CManager::GetRenderer()->GetDevice();
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxScale;
	// 現在のマテリアル保存用
	D3DMATERIAL9 matDef;
	// マテリアルデータへのポインタ
	D3DXMATERIAL *pMat;
	//D3DXVECTOR3 scale(5.0f, 5.0f, 5.0f);

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 行列拡縮関数
	D3DXMatrixScaling(&mtxScale, m_scale.x, m_scale.y, m_scale.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxScale);

	if (m_quat)
	{
		quat();
	}
	else
	{// 向きを反映
		D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);
	}

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 現在のマテリアル保持
	pDevice->GetMaterial(&matDef);

	// マテリアルデータへのポインタを取得
	pMat = (D3DXMATERIAL*)m_modelXData->m_buffMat->GetBufferPointer();

	for (int i = 0; i < (int)m_modelXData->m_numMat; i++)
	{
		pMat[i].MatD3D.Ambient = pMat[i].MatD3D.Diffuse;
		// マテリアルの設定
		pDevice->SetMaterial(&pMat[i].MatD3D);

		// テクスチャの設定
		pDevice->SetTexture(0, m_modelXData->m_texture[i]);

		// モデルパーツの描画
		m_modelXData->m_mesh->DrawSubset(i);
	}

	//CObjectX::DrawMaterial();

	//// 保存していたマテリアルを戻す
	pDevice->SetMaterial(&matDef);

	// テクスチャの設定
	pDevice->SetTexture(0, NULL);
}


//=============================================================================
// 描画
// Author : Yuda Kaito
// 概要 : 描画を行う
//=============================================================================
void CObjectX::DrawMaterial()
{
}


//--------------------------------------------------
// モデルのセット
//--------------------------------------------------
void CObjectX::SetModel(const char *filename)
{
	m_modelXData = new CModelX;

	//LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->CManager::GetRenderer()->GetDevice();

	//// 頂点座標の最小値
	//m_vtxMinModel = D3DXVECTOR3(100.0f, 100.0f, 100.0f);
	//// 頂点座標の最大値
	//m_vtxMaxModel = D3DXVECTOR3(-100.0f, -100.0f, -100.0f);

	CObjectXManager *Manager = CObjectXManager::GetManager();

	CModelX* Data = Manager->LoadXfile(filename);

	//ここにコピーしてるからよくない読み込んでからDataをそのまま使え浜田琉雅

	m_modelXData->m_buffMat = Data->m_buffMat;
	m_modelXData->m_numMat = Data->m_numMat;
	m_modelXData->m_mesh = Data->m_mesh;

	// 各メッシュのマテリアル情報を取得する
	for (int i = 0; i < (int)m_modelXData->m_numMat; i++)
	{
		m_modelXData->m_texture[i] = Data->m_texture[i];
	}

	m_vtxMaxModel = Data->GetVtxMax();
	m_vtxMinModel = Data->GetVtxMin();
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CObjectX::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CObjectX::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}

//=============================================================================
// マネージャーのコンストラクタ
// Author :浜田琉雅
// 概要 : 数値の初期化
//=============================================================================
CObjectXManager::CObjectXManager()
{
	for (int i = 0; i < MODEL_MAX; i++)
	{
		m_modelXList[i] = nullptr;
	}
}

//=============================================================================
//  マネージャーのデス
// Author : 浜田琉雅
// 概要 : 
//=============================================================================
CObjectXManager::~CObjectXManager()
{

}

//=============================================================================
// カウントのリセット
// Author : 浜田琉雅
// 概要 : カウントのリセット
//=============================================================================
CObjectXManager * CObjectXManager::GetManager()
{
	if (ms_ObjectXManager == nullptr)
	{
		ms_ObjectXManager = new CObjectXManager;
	}

	return ms_ObjectXManager;
}

//=============================================================================
// Xfileのよみこみ
// Author : 浜田琉雅
// 概要 : Xfileのよみこみ
//=============================================================================
CModelX * CObjectXManager::LoadXfile(const char * pXFileName)
{
	for (int i = 0; i < MODEL_MAX; i++)
	{
		if (m_modelXList[i] == nullptr)
		{
			continue;
		}

		if (strcmp(&m_modelXList[i]->m_pXFileName[0], &pXFileName[0]) == 0)
		{
			return m_modelXList[i];
		}
	}

	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->CManager::GetRenderer()->GetDevice();

	for (int i = 0; i < MODEL_MAX; i++)
	{
		if (m_modelXList[i] == nullptr)
		{
			m_modelXList[i] = new CModelX;

			strcpy(m_modelXList[i]->m_pXFileName, pXFileName);

			// Xファイルの読み込み
			D3DXLoadMeshFromX(pXFileName,
				D3DXMESH_SYSTEMMEM,
				CManager::GetInstance()->GetRenderer()->GetDevice(),
				NULL,
				&m_modelXList[i]->m_buffMat,
				NULL,
				&m_modelXList[i]->m_numMat,
				&m_modelXList[i]->m_mesh);

			// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
			D3DXMATERIAL *pMat = (D3DXMATERIAL*)m_modelXList[i]->m_buffMat->GetBufferPointer();

			// 各メッシュのマテリアル情報を取得する
			for (int idxMat = 0; idxMat < (int)m_modelXList[i]->m_numMat; idxMat++)
			{
				m_modelXList[i]->m_texture[idxMat] = nullptr;

				if (pMat[idxMat].pTextureFilename != nullptr)
				{// マテリアルで設定されているテクスチャ読み込み
					D3DXCreateTextureFromFileA(pDevice,
						pMat[idxMat].pTextureFilename,
						&m_modelXList[i]->m_texture[idxMat]);
				}
			}

			m_modelXList[i]->m_nType = i;
			// 最小値
			m_modelXList[i]->m_vtxMinModel = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
			// 最大値
			m_modelXList[i]->m_vtxMaxModel = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
			int nNumVtx;		// 頂点数
			DWORD pSizeFVF;		// 頂点フォーマットのサイズ
			BYTE *pVtxBuff;		// 頂点バッファのポインタ

								// 頂点数の取得
			nNumVtx = m_modelXList[i]->m_mesh->GetNumVertices();

			// 頂点フォーマットのサイズを取得
			pSizeFVF = D3DXGetFVFVertexSize(m_modelXList[i]->m_mesh->GetFVF());

			// 頂点バッファのロック
			m_modelXList[i]->m_mesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVtxBuff);

			for (int nCntVtx = 0; nCntVtx < nNumVtx; nCntVtx++)
			{
				// 頂点座標の代入
				D3DXVECTOR3 vtx = *(D3DXVECTOR3*)pVtxBuff;

				// 比較(最小値を求める)x
				if (vtx.x < m_modelXList[i]->m_vtxMinModel.x)
				{
					m_modelXList[i]->m_vtxMinModel.x = vtx.x;
				}
				// 比較(最小値を求める)y
				if (vtx.y < m_modelXList[i]->m_vtxMinModel.y)
				{
					m_modelXList[i]->m_vtxMinModel.y = vtx.y;
				}
				// 比較(最小値を求める)z
				if (vtx.z < m_modelXList[i]->m_vtxMinModel.z)
				{
					m_modelXList[i]->m_vtxMinModel.z = vtx.z;
				}

				// 比較(最大値を求める)x
				if (vtx.x > m_modelXList[i]->m_vtxMaxModel.x)
				{
					m_modelXList[i]->m_vtxMaxModel.x = vtx.x;
				}
				// 比較(最大値を求める)y
				if (vtx.y > m_modelXList[i]->m_vtxMaxModel.y)
				{
					m_modelXList[i]->m_vtxMaxModel.y = vtx.y;
				}
				// 比較(最大値を求める)z
				if (vtx.z > m_modelXList[i]->m_vtxMaxModel.z)
				{
					m_modelXList[i]->m_vtxMaxModel.z = vtx.z;
				}

				// 頂点フォーマットのサイズ分ポインタを進める
				pVtxBuff += pSizeFVF;
			}

			return m_modelXList[i];
		}
	}

	assert(false);
	return nullptr;
}

//=============================================================================
//  モデルの破棄
// Author : 浜田琉雅
// 概要 : モデルの破棄
//=============================================================================
void CObjectXManager::ReleaseAll()
{
	CObjectXManager *Manager = CObjectXManager::GetManager();

	if (Manager != nullptr)
	{
		for (int i = 0; i < MODEL_MAX; i++)
		{
			if (Manager->m_modelXList[i] != nullptr)
			{
				Manager->m_modelXList[i]->Release();
				delete Manager->m_modelXList[i];
				Manager->m_modelXList[i] = nullptr;
			}
		}

		if (ms_ObjectXManager != nullptr)
		{
			delete ms_ObjectXManager;
			ms_ObjectXManager = nullptr;
		}
	}
}

//--------------------------------------------------
// 描画しないワールドマトリックスだけ計算
//--------------------------------------------------
void CObjectX::NotDraw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->CManager::GetRenderer()->GetDevice();
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxScale;

	//D3DXVECTOR3 scale(5.0f, 5.0f, 5.0f);

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 行列拡縮関数
	//D3DXMatrixScaling(&mtxScale, scale.x, scale.y, scale.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	//D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxScale);

	if (m_quat)
	{
		quat();
	}
	else
	{// 向きを反映
		D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
		D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);
	}

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);
}

void CModelX::Release()
{
	for (int i = 0; i < (int)m_numMat; i++)
	{
		if (m_texture[i] != nullptr)
		{// テクスチャの解放
			m_texture[i]->Release();
			m_texture[i] = nullptr;
		}
	}
	// メッシュの破棄
	if (m_mesh != nullptr)
	{
		m_mesh->Release();
		m_mesh = nullptr;
	}
	// マテリアルの破棄
	if (m_buffMat != nullptr)
	{
		m_buffMat->Release();
		m_buffMat = nullptr;
	}
}

