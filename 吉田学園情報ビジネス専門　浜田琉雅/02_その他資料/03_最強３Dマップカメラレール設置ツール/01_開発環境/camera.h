//============================
//
// カメラ設定ヘッター
// Author:hamada ryuuga
//
//============================

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "renderer.h"

class CCameraAnimation;

class CCamera
{
private: // プライベート定数
	static const D3DXVECTOR3 INIT_POSV;		// 位置の初期位置
	static const float MOVE_ATTENUATION;	// 減衰係数
	static const float FIELD_OF_VIEW;		// 視野角
	static const float NEAR_VALUE;			// ニア
	static const float FUR_VALUE;			// ファー

public:

	CCamera();
	~CCamera();

	void Init(void);	// 初期化
	void Uninit(void);	// 破棄
	void Update(void);	// 更新
	void Set(int Type);	// 画面設定

	void GameInit();
	D3DXMATRIX *GetMtxProje() { return &m_MtxProje; }
	D3DXMATRIX *GetMtxView() { return &m_MtxView; }
	D3DXVECTOR3 *GetPos() { return &m_posV; }
	D3DXVECTOR3 *GetPosR() { return &m_posR; }
	D3DXVECTOR3 *GetRot() { return &m_rot; }
	
	void Setisfree() { m_IsFree = !m_IsFree; }
	bool Getisfree() {return m_IsFree; }
	//	Animationの読み込み
	void LandAnimation(std::string);

	//	からのAnimationを追加
	void AddAnimation();

	//	Animationをゲット
	CCameraAnimation* GetAnimation(const int IsPoptime);

	//	Animationのさいずの取得
	int GetAnimationSize();

	void PlayAnimation(int Animation, bool Isloop);
	void PlayAnimation(std::string AnimationName, bool Isloop);
private:
	int m_Type;

	float m_Speed = 1.0f;
	float m_rotSpeed = 0.05f;
	float m_rotSpeed2 = D3DX_PI / 2;
	float m_fDistance;					// 距離
	float m_fDistanceY;

	D3DXVECTOR3 m_posV;				// 位置
	D3DXVECTOR3 m_posR;				// 注視点
	D3DXVECTOR3 m_vecU;				// ベクトル
	D3DXVECTOR3 m_directionV;			// 回転の向き位置
	D3DXVECTOR3 m_directionR;			// 回転の向き注視点
	D3DXVECTOR3 m_rot;					// 位置回転
	
	D3DVIEWPORT9 m_viewport;
	D3DXVECTOR3 m_posVDest;
	D3DXVECTOR3 m_posRDest;
	D3DXMATRIX m_MtxProje;				// プロジェクションマトリックス//ポリゴンの位置や回転行列すべてをつめてるナニカ
	D3DXMATRIX m_MtxView;				// ビューマトリックス//ポリゴンの位置や回転行列すべてをつめてるナニカ
	
	bool m_IsAnimation;
	bool m_IsFree;
	bool m_IsLoop;
	std::vector <CCameraAnimation*> m_Animation;

	int m_NouAnimation;

};


#endif

