//=============================================================================
//
// プレイヤー設定
// Author:hamada ryuuga
//
//=============================================================================
//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <assert.h>
#include "player.h"
#include "input.h"
#include "camera.h"
#include "motion.h"
#include "manager.h"
#include "motion.h"
#include "particle_manager.h"
#include "utility.h"
#include "game.h"
#include "tutorial.h"
#include "title.h"
#include "mesh.h"
#include "building.h"
#include "stage.h"
#include "ball.h"
#include "wood_spawn.h"
#include "Trajectory.h"

#include "imgui_property.h"
#include "hamada.h"
//=============================================================================
// static変数
//=============================================================================
const int CPlayer::MAXLIFE = 300;				// 最大体力
const float CPlayer::MOVE_ATTENUATION = 0.1f;	// 移動減衰係数
const float CPlayer::MOVE_ROTATTENUATION = 0.5f;	// 移動減衰係数
const float CPlayer::SPEED = 1.0f;		 		// 移動量
const float CPlayer::WIDTH = 10.0f;				// モデルの半径
const int CPlayer::MAX_PRAYER = 16;				// 最大数
const int CPlayer::MAX_MOVE = 9;				// アニメーションの最大数
const int CPlayer::INVINCIBLE = 30;				// 無敵時間
const int CPlayer::MAX_MODELPARTS = 9;			// モデルの最大数

//=============================================================================
// コンストラクタ
//=============================================================================
CPlayer::CPlayer()
{
}

//=============================================================================
// デストラクタ
//=============================================================================
CPlayer::~CPlayer()
{

}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CPlayer::Init()
{
	// 現在のモーション番号の保管

	CMotionModel3D::Init();
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_moveDash = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3	Size(100.0f, 100.0f, 100.0f);
	m_MoveSpeed = 7.0f;
	SetSize(Size);
	m_Gura = false;
	m_Trajectory = CTrajectory::Create();
	m_MoveSet = 0;
	m_Trajectory->SetMtx(GetMtxWorld());
	m_rotMove = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CPlayer::Uninit()
{
	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();
}

//=============================================================================
// 更新
//=============================================================================
void CPlayer::Update()
{
	//switch (*CManager::GetMode())
	//{
	//case CManager::MODE_TITLE:
	//	TitleMove();	//動きセット
	//	break;
	//case CManager::MODE_GAME:
		Move();	//動きセット	
	//	break;
	//case CManager::MODE_RESULT:
	//	ResetMove();
	//	break;
	//case CManager::MODE_RANKING:
	//	break;
	//case CManager::MODE_TUTORIAL:
	//	TutorialMove();
	//	break;
	//default:
	//	break;
	//}

	// 現在のモーション番号の保管
	CMotionModel3D::Update();
}

//=============================================================================
// 描画
//=============================================================================
void CPlayer::Draw()
{
	if (m_LineHit == false)
	{
		
		D3DXQUATERNION quat;
		D3DXQuaternionIdentity(&quat);
		D3DXVECTOR3 vecY = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		D3DXQuaternionRotationAxis(&quat, &vecY, D3DX_PI);
		//クオータニオンノーマライズ
		D3DXQuaternionNormalize(&quat, &quat);
		CGame::GetStage()->GetPlayer()->SetQuat(quat);
	}
	else
	{
		
	}
	CMotionModel3D::Draw();
}

//=============================================================================
// create
//=============================================================================
CPlayer *CPlayer::Create()
{
	CPlayer * pObject = nullptr;
	pObject = new CPlayer;

	if (pObject != nullptr)
	{
		pObject->Init();
	}

	return pObject;
}

//=============================================================================
// Move
//=============================================================================
void CPlayer::Move()	//動きセット
{
	GetTrajectory()->SetIsDraw(true);
	SetLineHit(false);
	m_Friction = 0.0f;
	CInput *CInputpInput = CInput::GetKey();
	D3DXVECTOR3 *Camerarot = CRenderer::GetCamera()->GetRot();
	D3DXVECTOR3 Pos = GetPos();
	float consumption = 0.0f;

	if (CInputpInput->Press(CInput::KEY_RIGHT))
	{
		m_move.x += sinf(D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
		m_move.z += cosf(D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
		consumption = m_rotMove.x + (D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
	}
	if (CInputpInput->Press(CInput::KEY_LEFT))
	{
		m_move.x += sinf(-D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
		m_move.z += cosf(-D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_MoveSpeed;

		consumption = m_rotMove.x - (D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
	}
	if (CInputpInput->Press(CInput::KEY_DOWN))
	{	
		m_move.x += sinf(Camerarot->y - D3DX_PI) * SPEED * m_MoveSpeed;
		m_move.z += cosf(Camerarot->y - D3DX_PI) * SPEED * m_MoveSpeed;

		consumption = (m_rotMove.x - D3DX_PI - m_rot.y+ Camerarot->y);
		//consumption = m_rotMove.x + m_rot.y - Camerarot->y;
	}
	if (CInputpInput->Press(CInput::KEY_UP))
	{	
		m_move.x += sinf(Camerarot->y) * SPEED * m_MoveSpeed;
		m_move.z += cosf(Camerarot->y) * SPEED * m_MoveSpeed;

		consumption = m_rotMove.x - m_rot.y + Camerarot->y;
	}
 	else
	{
		m_Pow = 20;
	}
	//動きの加速度
	if (!m_LineHit && (CInputpInput->Press(CInput::KEY_UP)
		|| CInputpInput->Press(CInput::KEY_LEFT)
		|| CInputpInput->Press(CInput::KEY_RIGHT)
		|| CInputpInput->Press(CInput::KEY_DOWN)))
	{
		m_MoveSet += 0.1f;
	}
	else
	{
		m_MoveSet = 0.0f;
	}
	//	必殺技
	if (CInputpInput->Trigger(CInput::KEY_DECISION))
	{
		CGame::GetStage()->CreateModel(Pos);
	}

	if (CInputpInput->Trigger(CInput::KEY_SHOT))
	{
		CGame::GetStage()->CreateMesh(Pos);
	}

	int Size = CGame::GetStage()->GetNumAllBuilding();


	m_posOld = Pos;//移動を加算
	if (m_move.x <= -15.0f)
	{
		m_move.x = -15.0f;
	}
	if (m_move.x >= 15.0f)
	{
		m_move.x = 15.0f;
	}
	if (m_move.z <= -15.0f)
	{
		m_move.z = -15.0f;
	}
	if (m_move.z >= 15.0f)
	{
		m_move.z = 15.0f;
	}

	if (!m_LineHit)
	{//加速してる時とラインに当たってない時の判定
		if (m_MoveSet >= 1.0f)
		{
			m_MoveSet = 1.0f;
		}
		float MoveSize = hmd::easeInSine(m_MoveSet);
		m_move.x *= MoveSize;
		m_move.z *= MoveSize;
	}
	m_moveDash.x += (0.0f - m_moveDash.x) * (MOVE_ATTENUATION + m_Friction);	//（目的の値-現在の値）＊減衰係数
	m_moveDash.y += (0.0f - m_moveDash.y) * (MOVE_ATTENUATION + m_Friction);
	m_moveDash.z += (0.0f - m_moveDash.z) * (MOVE_ATTENUATION + m_Friction);

	m_move.x += (0.0f - m_move.x) * (MOVE_ATTENUATION + m_Friction);	//（目的の値-現在の値）＊減衰係数
	m_move.z += (0.0f - m_move.z) * (MOVE_ATTENUATION + m_Friction);

	if (m_Gura)
	{
		m_move.y += -1.0f;
	}
	else
	{
		m_move.y = 0.0f;
	}

	Pos += (m_move + m_moveDash);//移動を加算
	SetPos(Pos);

	NormalizeAngle(&consumption);	//正規化
	for (int i = 0; i < 100; i++)
	{
		CMesh* hitMesh = CGame::GetStage()->GetPlayerHitMesh(i);
		if (hitMesh != nullptr && !hitMesh->GetDeath())
		{
			hitMesh->CollisionMesh(&Pos);
			hitMesh->CreateMesh(&Pos);
			SetPos(Pos);
		}
	}
	for (int i = 0; i < Size; i++)
	{// 当たり判定
		if (CObject::SelectModelObject(i, CObject::BUILDING) != nullptr)
		{
			CBuilding* Obj = dynamic_cast<CBuilding*>(CObject::SelectModelObject(i, CObject::BUILDING));  // ダウンキャスト 

			if (Obj->CollisionModel(&Pos, &m_posOld, &GetSize()))
			{
				m_posOld = Pos;//移動を加算
				Pos = GetPos();
			}
		}
	}
	//減算設定（感性）
	m_rot.y += (consumption)* MOVE_ROTATTENUATION;//目的の値-現在の値）＊減衰係数

	NormalizeAngle(&m_rot.y);	 //正規化

	CMotionModel3D::SetRot(m_rot);
}


//=============================================================================
// TitleのときのMove
//=============================================================================
void CPlayer::TitleMove()
{
	D3DXVECTOR3 Pos = GetPos();

	D3DXVECTOR3 *Camerarot = CRenderer::GetCamera()->GetRot();
	float consumption = 0.0f;
	
	m_move.x += sinf(-D3DX_PI *0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
	m_move.z += cosf(-D3DX_PI *0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
	consumption = m_rotMove.x + -(D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
	
	Pos.y = 250.0f;

	m_move.x += (0.0f - m_move.x)*MOVE_ATTENUATION;	//（目的の値-現在の値）＊減衰係数
	m_move.z += (0.0f - m_move.z)*MOVE_ATTENUATION;
	m_move.y += (0.0f - m_move.y)*MOVE_ATTENUATION;

	Pos += m_move;	// 移動を加算

	NormalizeAngle(&consumption);	// 正規化

	// 減算設定（感性）
	m_rot.y += consumption * MOVE_ATTENUATION;	// 目的の値-現在の値） ＊ 減衰係数

	NormalizeAngle(&m_rot.y);	// 正規化

	if (Pos.x <= -SCREEN_WIDTH*0.5f-100.0f)
	{
		Pos.x = SCREEN_WIDTH*0.5f;
	}
}

//=============================================================================
// ResetのときのMove
//=============================================================================
void CPlayer::ResetMove()
{
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 *Camerarot = CRenderer::GetCamera()->GetRot();
	float consumption = 0.0f;

	m_move.x += sinf(-D3DX_PI *0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
	m_move.z += cosf(-D3DX_PI *0.5f + Camerarot->y) * SPEED * m_MoveSpeed;
	consumption = m_rotMove.x + -(D3DX_PI*0.5f) - m_rot.y + Camerarot->y;

	Pos.y = 250.0f;

	m_move.x += (0.0f - m_move.x) * MOVE_ATTENUATION;	//（目的の値-現在の値）＊減衰係数
	m_move.z += (0.0f - m_move.z) * MOVE_ATTENUATION;
	m_move.y += (0.0f - m_move.y) * MOVE_ATTENUATION;

	Pos += m_move;	// 移動を加算

	// 正規化
	if (consumption > D3DX_PI)
	{
		consumption += D3DX_PI * 2.0f;
	}
	if (consumption < -D3DX_PI)
	{
		consumption += -D3DX_PI * 2.0f;
	}

	// 減算設定（感性）
	m_rot.y += (consumption)* MOVE_ATTENUATION;	// 目的の値-現在の値）＊減衰係数

	// 正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y += -D3DX_PI * 2;
	}
	if (m_rot.y <= -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2;
	}

	if (Pos.x <= -SCREEN_WIDTH * 0.5f - 100.0f)
	{
		Pos.x = SCREEN_WIDTH * 0.5f;
	}
}

//=============================================================================
// Move
//=============================================================================
void CPlayer::TutorialMove()	//動きセット
{


}