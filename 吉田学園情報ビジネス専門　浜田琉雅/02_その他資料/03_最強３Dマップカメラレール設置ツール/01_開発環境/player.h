//=============================================================================
//
// プレイヤー設定ヘッター
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _PLAYER_H_
#define _PLAYER_H_

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "motion_model3D.h"
#include "Trajectory.h"
//-----------------------------------------------------------------------------
// 前方宣言
//-----------------------------------------------------------------------------
class CMotion;
class CMagicBox;
class CTrajectory;
//-----------------------------------------------------------------------------
// プレイヤー操作キャラクタークラス
//-----------------------------------------------------------------------------
class CPlayer : public CMotionModel3D
{
public:

	//modelデータの構造体//
	struct ModelData
	{
		int key;		// 時間管理
		int nowKey;		// 今のキー
		int loop;		// ループするかどうか[0:ループしない / 1 : ループする]
		int num_key;  	// キー数
		/*MyKeySet KeySet[MAX_KEY];*/
	};

public: // 定数
	static const int MAXLIFE;				// 最大体力
	static const float MOVE_ATTENUATION;	// 減衰係数
	static const float MOVE_ROTATTENUATION;	// 移動減衰係数
	static const float SPEED;				// スピード
	static const float WIDTH;				// モデルの半径
	static const int MAX_PRAYER;			// 最大数
	static const int MAX_MOVE;				// アニメーションの最大数
	static const int INVINCIBLE;			// 無敵時間
	static const int MAX_MODELPARTS;		// モデルの最大数
	
public:
	CPlayer();
	~CPlayer();

	HRESULT Init()override;	// 初期化
	void Uninit()override;	// 破棄
	void Update()override;	// 更新
	void Draw()override;	// 描画

	static CPlayer *Create();
	
	void SetFriction(float Infriction) { m_Friction = Infriction; };
	void SetGura(bool SetGura) { m_Gura = SetGura; };
	D3DXVECTOR3 GetPosOld() {return m_posOld; }
	D3DXVECTOR3 GetMove() { return m_move; }
	void SetMove(D3DXVECTOR3 ismove) { m_move = ismove; }
	void SetMoveDash(D3DXVECTOR3 ismove) { m_moveDash = ismove; }

	void SetLineHit(bool IsHit) { m_LineHit = IsHit; }
	CTrajectory *GetTrajectory() { return m_Trajectory; }
	
private:
	void Move();		// 移動
	void TitleMove();
	void ResetMove();
	void TutorialMove();	//動きセット

private:
	int m_Pow;
	float m_MoveSet;
	float m_MoveSpeed;
	float m_Friction;
	bool m_Gura;
	bool m_LineHit;
private:

	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_moveDash;
	D3DXVECTOR3 m_posOld;

	D3DXVECTOR3 m_rotMove;
	D3DXVECTOR3 m_rot;
	CTrajectory*m_Trajectory;

};
#endif
