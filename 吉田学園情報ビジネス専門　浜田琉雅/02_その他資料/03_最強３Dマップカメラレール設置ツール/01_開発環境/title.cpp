//**************************************************
//
// 制作 ( タイトル )
// Author : hamada ryuuga
//
//**************************************************
#include "title.h"
#include "input.h"
#include "manager.h"
 
#include "fade.h"
#include "sound.h"
 
#include "3dpolygon.h"
#include "mesh.h"
#include "player.h"
#include "stage.h"
#include "objectX.h"

CStage* CTitle::m_Stage;

//-----------------------------------------------------------------------------
// コンストラクター
//-----------------------------------------------------------------------------
CTitle::CTitle()
{
}

//-----------------------------------------------------------------------------
// デストラクト
//-----------------------------------------------------------------------------
CTitle::~CTitle()
{
}

//-----------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------
HRESULT CTitle::Init(void)
{
	D3DXVECTOR3 BGPos;
	BGPos.x = 0.0f;
	BGPos.y = 0.0f;
	BGPos.z -= 30.0f;
	D3DXVECTOR3 EnemyPos = CManager::centerPos;
	m_alpha = 1.2f;

	//m_Stage = CStage::Create();

	m_addX = 0;
	m_addY = 0;
	ModeSelect = false;
	NextMode = MODE::MODE_GAME;

	EnemyPos.z -= 30.0f;

	D3DXVECTOR3 Size(3.8f, 3.8f, 3.8f);
	D3DXVECTOR3 Rot(0.0f, 1.57f, 0.0f);

	//GonFoxのTITLE文字
	m_list[0] = CObject2d::Create(1);
	m_list[0]->SetTexture(CTexture::TEXTURE_TITLE);
	m_list[0]->SetSize(CManager::centerPos);
	m_list[0]->SetPos(CManager::centerPos);
	m_list[0]->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	m_3dpolygon[0] = C3dpolygon::Create(1);
	m_3dpolygon[0]->SetTexture(CTexture::TEXTURE_TITLE);
	m_3dpolygon[0]->SetSize(D3DXVECTOR3(10.0f,0.0f,10.0f));
	m_3dpolygon[0]->SetPos(D3DXVECTOR3(0.0f,00.0f,0.0f));
	m_3dpolygon[0]->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_3dpolygon[0]->SetRot(D3DXVECTOR3(0.0f, 0.0f, 1.50f));

	//ゲームスタートの文字
	m_list[1] = CObject2d::Create(1);
	m_list[1]->SetTexture(CTexture::TEXTURE_FOXTITLE);
	m_list[1]->SetSize(CManager::centerPos);
	m_list[1]->SetPos(D3DXVECTOR3(CManager::centerPos.x,600.0f,0.0f));
	m_list[1]->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	
	
	//モード選択時の背景黒くするやつ
	fade = CObject2d::Create(2);
	fade->SetTexture(CTexture::TEXTURE_NONE);
	fade->SetSize(CManager::centerPos);
	fade->SetPos(CManager::centerPos);
	fade->SetColar(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
	
	float y = 120.0f;
	
	//ゲームの文字
	m_object2d[0] = CObject2d::Create(2);
	m_object2d[0]->SetTexture(CTexture::TEXTURE_TITLEGAME);
	m_object2d[0]->SetSize(CManager::centerPos);
	m_object2d[0]->SetPos(D3DXVECTOR3(CManager::centerPos.x, CManager::centerPos.y - y, 0.0f));
	m_object2d[0]->SetColar(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
	
	//
	////チュートリアルの文字
	//m_object2d[1] = CObject2d::Create(2);
	//m_object2d[1]->SetTexture(CTexture::TEXTURE_TITLETUTORIAL);
	//m_object2d[1]->SetSize(CManager::centerPos);
	//m_object2d[1]->SetPos(D3DXVECTOR3(CManager::centerPos.x, CManager::centerPos.y, 0.0f));
	//m_object2d[1]->SetColar(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
	//
	////ランキングの文字
	//m_object2d[2] = CObject2d::Create(2);
	//m_object2d[2]->SetTexture(CTexture::TEXTURE_TITLERANKIN);
	//m_object2d[2]->SetSize(CManager::centerPos);
	//m_object2d[2]->SetPos(D3DXVECTOR3(CManager::centerPos.x, CManager::centerPos.y + y, 0.0f));
	//m_object2d[2]->SetColar(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
	//
	y += 120.0f;
	
	////おわりの文字
	//m_object2d[3] = CObject2d::Create(2);
	//m_object2d[3]->SetTexture(CTexture::TEXTURE_TITLEEND);
	//m_object2d[3]->SetSize(CManager::centerPos);
	//m_object2d[3]->SetPos(D3DXVECTOR3(CManager::centerPos.x, CManager::centerPos.y + y, 0.0f));
	//m_object2d[3]->SetColar(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	m_ParticleCount = 0;

	CManager::GetInstance()->GetSound()->Play(CSound::LABEL_BGM_TITLE);

	
	return S_OK;
}

//-----------------------------------------------------------------------------
// 破棄
//-----------------------------------------------------------------------------
void CTitle::Uninit(void)
{
	CManager::GetInstance()->GetSound()->Stop();

	CObject::Release();
	CObjectXManager::ReleaseAll();
}

//-----------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------
void CTitle::Update(void)
{
	//きつねをもちもちさせるやつ
	if (!ModeSelect)
	{//一回押された	
		if (m_addY <= 10)
		{
			Sizcontroller = true;
		}

		if (m_addY >= 50)
		{
			Sizcontroller = false;
		}

		float a;
		if (Sizcontroller)
		{
			m_addY++;
			m_addX--;
			a = sinf((float)m_alpha);
			m_alpha -= 1.0f / 60;
		}
		else
		{
			m_addY--;
			m_addX++;
			a = sinf((float)m_alpha);
			m_alpha += 1.0f / 60;
		}

		//きつねをもちもちさせるやつ
		D3DXVECTOR3 addPos = D3DXVECTOR3(1.0f + (float)m_addX, 1.0f + (float)m_addY, 0.0f);
		//m_Bg[1]->SetSize(CManager::centerPos *0.8f + addPos);

		//点滅させる
		m_list[1]->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, a));
	}

	CInput *CInputpInput = CInput::GetKey();
	if (CInputpInput->Trigger(CInput::KEY_DECISION))
	{
		CManager* maneger = CManager::GetInstance();
		//maneger->GetSound()->Play(CSound::LABEL_SE_ON);
		if (ModeSelect)
		{//一回押された	
			switch (NextMode)
			{
			case MODE::MODE_GAME:
				//モードの設定
				maneger->GetFade()->NextMode(CManager::MODE_GAME);
				break;
			case MODE::MODE_END:
				//ゲームの終了
				PostQuitMessage(0);
				break;
			default:
				break;
			}
		}
		else
		{
			//画面黒くする
			fade->SetColar(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.5f));

			////文字を出し
			//for (int i = 0; i < 4; i++)
			//{
			//	m_object2d[i]->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.8f));
			//}
			//
			////今使ってるやつを明るく
			m_object2d[0]->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
			ModeSelect = true;
		}
	}

	if (ModeSelect)
	{

		

#ifdef _DEBUG

		if (CInputpInput->Trigger(CInput::KEY_DEBUG))
		{
			//モードの設定

			//
		}
		if (CInputpInput->Trigger(CInput::KEY_F2))
		{
			//モードの設定
			
			//m_Stage->
		}

#endif // DEBUG
	}
}

//-----------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------
void CTitle::Draw(void)
{

}