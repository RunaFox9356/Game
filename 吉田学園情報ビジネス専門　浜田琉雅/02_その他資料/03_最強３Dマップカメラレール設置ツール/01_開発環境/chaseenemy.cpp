//=============================================================================
//
// 敵設定
// Author:hamada ryuuga
//
//=============================================================================

#include "chaseenemy.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "stage_imgui.h"
#include "reaction.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CChaseEnemy::CChaseEnemy()
{
	m_lookmin = { -200.0f,-200.0f ,-200.0f };
	m_lookmax = { 200.0f,200.0f ,200.0f };
	m_lookCount = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CChaseEnemy::~CChaseEnemy()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CChaseEnemy::Init()
{
	// 現在のモーション番号の保管
	CEnemy::Init();
	SetUp(CObject::ENEMY);
	m_hit = false;
	m_isMove = false;

	for (int i = 0; i < 12; i++)
	{
		if (Line[i] != nullptr)
		{
			Line[i] = nullptr;
		}
	}
	m_sensing = CReaction::Create();
	m_sensing->SetTexture(CTexture::TEXTURE_OFF);
	m_sensing->SetPos(GetPos());
	m_sensing->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));
	m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CChaseEnemy::Uninit()
{
	CEnemy::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CChaseEnemy::Update()
{
	CEnemy::Update();
	
	CPlayer*Player = CGame::GetStage()->GetPlayer();
	D3DXVECTOR3  pPos = Player->GetPos();
	D3DXVECTOR3  pPosOld = Player->GetPosOld();
	D3DXVECTOR3  pSize = Player->GetSize();

	m_sensing->SetPos(D3DXVECTOR3(GetPos().x,GetPos().y+ 300.0f, GetPos().z));
	if (CollisionMove(&pPos, &pPosOld, &pSize))
	{
	
		m_lookCount++;
		m_sensing->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));
		m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		if (m_lookCount >= 20)
		{	// 動作
			Move();
			m_sensing->SetTexture(CTexture::TEXTURE_ON);
		}
		else
		{
			m_sensing->SetTexture(CTexture::TEXTURE_OFF);
		}
	}
	else
	{
		m_sensing->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));
		m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		m_lookCount = 0;
	}

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CChaseEnemy::Draw()
{
	CChaseEnemy::Setlookline();
	CEnemy::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CChaseEnemy *CChaseEnemy::Create(D3DXVECTOR3 pos)
{
	CChaseEnemy *pEnemy = nullptr;

	pEnemy = new CChaseEnemy;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::CHASEENEMY);
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CChaseEnemy::Move()
{
	m_posOld = GetPos();//過去の移動量を保存
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 rot = GetRot();
	//重力
	pos.y -= 1.0f;
	//modelとプレイヤーの当たり判定
	CPlayer*Player = CGame::GetStage()->GetPlayer();


	D3DXVECTOR3 vecPlayerDist = Player->GetPos() - GetPos();
	float distPlayer = D3DXVec3Length(&vecPlayerDist);
	
	pos += vecPlayerDist / distPlayer * 4.0f;
	rot.y = atan2f(vecPlayerDist.x, vecPlayerDist.z) + D3DX_PI;
	SetPos(pos);
	SetRot(rot);
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CChaseEnemy::Hit()
{
	CStageImgui* imgui = CGame::GetStageImgui();
	imgui->ImguiEnemy();
}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CChaseEnemy::NotHit()
{
	m_hit = false;
}

//--------------------------------------------------
// こりじょん判定
//--------------------------------------------------
bool CChaseEnemy::CollisionMove(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize)
{

	bool bIsLanding = false;

	D3DXMATRIX mtxWorld = *CMotionModel3D::GetMtxWorld();

	D3DXVECTOR3 min = GetlookMin();
	D3DXVECTOR3 max = GetlookMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			bIsLanding = true;
		}
	}
	else
	{
	
	}

	return bIsLanding;
}



//--------------------------------------------------
// 当たり判定の設定
//--------------------------------------------------
void CChaseEnemy::Setlookline()
{
	D3DXCOLOR col(1.0f, 0.0f, 1.0f, 1.0f);

	for (int i = 0; i < 12; i++)
	{
		if (Line[i] == nullptr)
		{
			Line[i] = CLine::Create();
		}
	}
	//D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	Line[0]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMin().y, GetlookMax().z), D3DXVECTOR3(GetlookMax().x, GetlookMin().y, GetlookMax().z), col);
	Line[1]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMin().y, GetlookMin().z), D3DXVECTOR3(GetlookMin().x, GetlookMin().y, GetlookMax().z), col);
	Line[2]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMin().y, GetlookMin().z), D3DXVECTOR3(GetlookMax().x, GetlookMin().y, GetlookMin().z), col);
	Line[3]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMax().x, GetlookMin().y, GetlookMin().z), D3DXVECTOR3(GetlookMax().x, GetlookMin().y, GetlookMax().z), col);
	Line[4]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMax().y, GetlookMax().z), D3DXVECTOR3(GetlookMax().x, GetlookMax().y, GetlookMax().z), col);
	Line[5]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMax().y, GetlookMin().z), D3DXVECTOR3(GetlookMin().x, GetlookMax().y, GetlookMax().z), col);
	Line[6]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMax().y, GetlookMin().z), D3DXVECTOR3(GetlookMax().x, GetlookMax().y, GetlookMin().z), col);
	Line[7]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMax().x, GetlookMax().y, GetlookMin().z), D3DXVECTOR3(GetlookMax().x, GetlookMax().y, GetlookMax().z), col);
	Line[8]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMin().y, GetlookMax().z), D3DXVECTOR3(GetlookMin().x, GetlookMax().y, GetlookMax().z), col);
	Line[9]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMin().x, GetlookMin().y, GetlookMin().z), D3DXVECTOR3(GetlookMin().x, GetlookMax().y, GetlookMin().z), col);
	Line[10]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMax().x, GetlookMin().y, GetlookMin().z), D3DXVECTOR3(GetlookMax().x, GetlookMax().y, GetlookMin().z), col);
	Line[11]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetlookMax().x, GetlookMin().y, GetlookMax().z), D3DXVECTOR3(GetlookMax().x, GetlookMax().y, GetlookMax().z), col);
}