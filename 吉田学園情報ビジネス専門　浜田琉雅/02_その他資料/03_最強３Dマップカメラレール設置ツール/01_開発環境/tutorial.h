//=============================================================================
//
// TUTORIAL画面のヘッダー
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _TUTORIAL_H_		//このマクロが定義されてなかったら
#define _TUTORIAL_H_		//2重インクルード防止のマクロ定義

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "main.h"
#include "object.h"

//-----------------------------------------------------------------------------
// 前方宣言
//-----------------------------------------------------------------------------
class CObject2d;

//-----------------------------------------------------------------------------
// チュートリアルクラス
//-----------------------------------------------------------------------------
class CTutorial :public CObject
{
public:
	CTutorial();
	~CTutorial();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

private:
	CObject2d *m_object2d[4];
	//CScore*pScore;

	D3DXVECTOR3 m_posOld;
	int m_Magic;
	int m_NextTaskCount;//次のタスクまでのカウント

	//CLEARタスク一覧表
	bool m_MoveClear;
	bool m_AttackClear;
	bool m_MagicClear;
	
	int m_nInterval;
	int m_nEnableTime;
	bool m_bFire;
};

#endif
