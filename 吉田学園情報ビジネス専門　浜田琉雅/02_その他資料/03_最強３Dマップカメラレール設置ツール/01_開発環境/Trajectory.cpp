//============================
//
// メッシュ設定(まっすぐ)
// Author:hamada ryuuga
//
//============================


#include "Trajectory.h"
#include "manager.h"
#include "utility.h"
#include "input.h"
#include "movemesh.h"



CTrajectory::CTrajectory(int nPriority) : CObject(nPriority)
{

}
CTrajectory::~CTrajectory()
{

}

//=========================================
// 初期化処理
//=========================================
HRESULT CTrajectory::Init(void)
{
	m_myDraw = true;
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();

	m_pTextureEmesh = NULL;
	// 初期化処理
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	// 回転座標

	m_pVtxBuff = nullptr;	    // 頂点バッファーへのポインタ
	m_pTextureEmesh = nullptr;        //テクスチャのポインタ


	m_pos = { 0.0f,0.0f,0.0f };	// 頂点座標
	m_posOrigin = { 0.0f,0.0f,0.0f };	// 頂点座標
	m_rot = { 0.0f,0.0f,0.0f };	// 回転座標
	m_nVtx = MAXTRAJECTORY;//頂点数							// m_mtxWorld;// ワールドマトリックス
	
	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * m_nVtx,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	for (int nVtx =0; nVtx < m_nVtx; nVtx++)
	{
		pVtx[nVtx].pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	
		pVtx[nVtx].col = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);

	}
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
	return S_OK;
}

//=========================================
// 終了処理
//=========================================
void CTrajectory::Uninit(void)
{
	// 頂点バッファーの解放
	if (m_pVtxBuff != NULL)
	{
		m_pVtxBuff->Release();
		m_pVtxBuff = NULL;
	}
	if (m_pTextureEmesh != NULL)
	{
		m_pTextureEmesh->Release();
		m_pTextureEmesh = NULL;
	}


	Release();
}

//=========================================
// 更新処理
//=========================================
void CTrajectory::Update(void)
{
	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);


	for (int nVtx = MAXTRAJECTORY - 3; nVtx >= 0; nVtx--)
	{
		pVtx[nVtx + 2].pos = pVtx[nVtx].pos;
		pVtx[nVtx + 2].col = D3DXCOLOR(0.0f, 0.0f, 1.0f , 1.0f);
	}
	
	D3DXVECTOR3 Pos(-50.0f, 30.0f, 0.0f);
	D3DXVECTOR3 OffSetPos(50.0f, 30.0f, 0.0f);
	D3DXVec3TransformCoord(&pVtx[0].pos, &Pos, m_ModelWorld);
	D3DXVec3TransformCoord(&pVtx[1].pos, &OffSetPos, m_ModelWorld);
	pVtx[0].col = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);
	pVtx[1].col = D3DXCOLOR(0.0f, 0.0f, 1.0f, 1.0f);
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
}

//=========================================
// 描画処理
//=========================================
void CTrajectory::Draw(void)
{
	if (!m_myDraw)
	{
		return;
	}
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();
	D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス

	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//ライト設定falseにするとライトと食らわない
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映
	// 行列回転関数(第1引数にヨー(y)ピッチ(x)ロール(z)方向の回転行列を作成)
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールド座標行列の設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);
	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	// ポリゴンの描画
	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, m_nVtx-2);


	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}



//=============================================================================
// GetPos関数
//=============================================================================
const D3DXVECTOR3 *CTrajectory::GetPos() const
{
	return &m_posOrigin;
}

//=============================================================================
// SetPos関数
//=============================================================================
void CTrajectory::SetPos(const D3DXVECTOR3 &pos)
{
	m_pos = pos;
	m_posOrigin = pos;
}
//=============================================================================
// Create関数
//=============================================================================
CTrajectory* CTrajectory::Create()
{
	CTrajectory * pObject = nullptr;
	pObject = new CTrajectory(2);

	if (pObject != nullptr)
	{

		pObject->Init();

	}
	return pObject;
}




