////=============================================================================
////
//// 説明書
//// Author : 浜田琉雅
////
////=============================================================================
//
//
//#ifndef _SHADOW_H_			// このマクロ定義がされてなかったら
//#define _SHADOW_H_			// 二重インクルード防止のマクロ定義
//
//#include "renderer.h"
//
//
//
//class CShadow : public CObject3d
//{
//public:
//
//	//modelデータの構造体//
//	struct ModelData
//	{
//		int key;		// 時間管理
//		int nowKey;		// 今のキー
//		int loop;		// ループするかどうか[0:ループしない / 1 : ループする]
//		int num_key;  	// キー数
//						/*MyKeySet KeySet[MAX_KEY];*/
//	};
//
//
//	CShadow();
//	~CShadow() override;
//	HRESULT Init() override;
//	void Uninit() override;
//	void Update() override;
//	void Draw() override;
//	static CShadow* Create(D3DXVECTOR3 pos);
//private:
//	D3DXVECTOR3 m_Testrot;
//
//	void move();
//};
//#endif
//
