//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================
#ifndef _STAGE_H_			// このマクロ定義がされてなかったら
#define _STAGE_H_			// 二重インクルード防止のマクロ定義

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "renderer.h"
#include "object.h"

//-----------------------------------------------------------------------------
// 前方宣言
//-----------------------------------------------------------------------------
class CMesh;
class CPlayer;
class CBuilding;
class CEnemy;
//-----------------------------------------------------------------------------
// ステージクラス
//-----------------------------------------------------------------------------
class CStage : public CObject
{
public:
	static const int MAX_MODEL = 256;	// テクスチャの最大数

	const std::string REL_PATH = "data/TEXTURE/";
	const std::string ABS_PATH = "data\\TEXTURE\\";

	enum MESHTYPE
	{
		MESH = 0,		// 通常
		MESHMOVE,		// 動くゆか
		FLOORINGMOVE,	//滑る床
		RUNMESH,		//乗ったら動く床
		MAXMESH			//あんただれや？
	};


	enum ENEMYTYPE
	{
		ENEMY = 0,		// 通常
		CHASEENEMY,		// Playerを追ってくるやつ
		BOSS,		// BOSS
		MAXENEMY			//あんただれや？
	};

	enum MODELTYPE
	{
		NORMAL = 0,		// 通常
		PENDULUM,		// 振り子
		WOOD,			// 木
		BALL,			// ボール
		RAIL,			// 乗ったら動く床
		CHANGE,			// 乗ったら動く床
		GOAL,			// ゴール
		DASHITEM,			// dashitem
		BANE,			// bame
		BELL,			// 得点アイテム
		CAMERA,			// CAMERA
		MAXMODEL				//あんただれや？
	};

	void SetObject();

	void Savefile(const char * pFileName);
	void Loadfile(const char * pFileName);

	void SavefileMesh(const char * pFileName);
	void LoadfileMesh(const char * pFileName);

	void SavefileEnemy(const char * pFileName);
	void LoadfileEnemy(const char * pFileName);

	CStage();
	~CStage() override;
	HRESULT Init()override;
	void Uninit() override;
	void Update() override;
	void Draw()override {} ;

	
	static CStage* Create();

	void CreateModel(D3DXVECTOR3 IsPos);
	void CreateMesh(D3DXVECTOR3 IsPos);
	void CreateEnemy(D3DXVECTOR3 IsPos);
	
	void SetPath(std::string str);		// パスの設定
	void SetPathTex(std::string str);	// パスの設定
	void SetPathEnemy(std::string str);	// パスの設定
	void SetPopItem(char* Name) { PopItemName = Name; }

	CPlayer * GetPlayer() { return m_Player; }
	char* GetPopItem() { return PopItemName; }
	std::string GetPath(int index, bool path = true);		// パスの取得
	std::string GetPathTex(int index, bool path = true);	// パスの取得
	std::string GetPathEnemy(int index, bool path = true);	// パスの取得
	static CMesh*GetMesh(int IsNumber);
	
	CMesh*GetPlayerHitMesh(int IsNumber);

	int GetNumAll();				// 総数の取得
	int GetNumAllTex();				// 総数の取得
	int GetNumAllBuilding() { return m_numAllBuilding; };	// 総数の取得
	int GetNumAllEnemy() { return m_numAllEnemy; };	// 敵の総数の取得
	int GetNumAllEnemyPass() { return m_numAllEnemyPass; };	// 敵の読み込んだpass総数の取得
private:
	int m_numAll;
	int m_numAllMesh;
	int m_numAllTex;
	int m_numAllEnemy;
	int m_numAllEnemyPass;
	int m_numAllBuilding;

	CPlayer * m_Player;
	char *PopItemName;


	std::string m_fileName[MAX_MODEL];		// ファイルパス
	std::string m_fileNameTex[MAX_MODEL];	// ファイルパス
	std::string m_fileNameEnemy[MAX_MODEL];	// ファイルパス
};
#endif
