//=============================================================================
//
// dashitem
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _ENEMY_H_
#define	_ENEMY_H_

#include "motion_model3D.h"
#include "stage.h"

class CLine;
//**************************************************
// クラス
//**************************************************
class CEnemy : public CMotionModel3D
{
public:
	CEnemy();
	~CEnemy() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CEnemy *Create(D3DXVECTOR3 pos);
	void Move();//動き
	void quat() {}//クオータにおん
	virtual void Hit();//当たってるとき
	virtual void NotHit();//当たってない時

	void SetPass(std::string pass);//なんのModelを読み込むかどうか
	std::string GetPass(){ return Myfilepass; }

	bool CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize);//当たり判定
	void Setline();//当たり判定の可視化


	//Model　sizeの設定
	void SetMin(const D3DXVECTOR3 ismin) { m_min = ismin; }
	void SetMax(const D3DXVECTOR3 ismax) { m_max = ismax; }

	D3DXVECTOR3 GetMin() { return m_min; }
	D3DXVECTOR3 GetMax() { return m_max; }

	void SetPopPos(const D3DXVECTOR3 isPopPos) { m_PopPos = isPopPos; SetPos(isPopPos); }
	D3DXVECTOR3 GetPopPos() { return m_PopPos; }


	void SetPopRot(const D3DXVECTOR3 isPopRot) { m_PopRot = isPopRot; SetRot(isPopRot); }
	D3DXVECTOR3 GetPopRot() { return m_PopRot; }

	void SetEnemyType(CStage::ENEMYTYPE IsType) { m_EnemyType = IsType; };
	CStage::ENEMYTYPE GetEnemyType() { return m_EnemyType; };

private:
	bool m_hit;//当たってるか
	bool m_isMove;//うごくかどうか
	D3DXVECTOR3 m_PopPos;
	D3DXVECTOR3 m_PopRot;
	D3DXVECTOR3 m_min;
	D3DXVECTOR3 m_max;
	CLine*Line[12];

	CStage::ENEMYTYPE m_EnemyType;
	std::string Myfilepass;
};

#endif // !_PENBULUM_H_

#pragma once
