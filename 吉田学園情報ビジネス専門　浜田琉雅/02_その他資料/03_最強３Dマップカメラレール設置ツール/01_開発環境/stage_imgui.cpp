//**************************************************
//
// Hackathon ( imgui_property.cpp )
// Author  : katsuki mizuki
// Author  : Tanaka Kouta
// Author  : Hamada Ryuga
// Author  : Yuda Kaito
//
//**************************************************

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "stage_imgui.h"
//-----------------------------------------------------------------------------
// imgui
//-----------------------------------------------------------------------------
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui_internal.h"
#include <implot.h>

//-----------------------------------------------------------------------------
// json
//-----------------------------------------------------------------------------
#include <fstream>

//-----------------------------------------------------------------------------
// stage
//-----------------------------------------------------------------------------
#include "main.h"
#include "game.h"
#include "stage.h"
#include "manager.h"
#include "texture.h"
#include "mesh.h"
#include "title.h"
#include "stage.h"
#include "building.h"
#include "object.h"
#include "ball.h"
#include "pendulum.h"
#include "wood_spawn.h"
#include "main.h"
#include "player.h"
#include "game.h"
#include "movemesh.h"
#include "camera.h"
#include "camera_animation.h"
#include "camera_player.h"
#include "enemy.h"
#include "boss.h"
#include "player.h"
//-----------------------------------------------------------------------------
// コンストラクタ
//-----------------------------------------------------------------------------
CStageImgui::CStageImgui() :
	m_sliderIntmodel(0),
	m_sliderIntmesh(0)
{
}

//-----------------------------------------------------------------------------
// デストラクタ
//-----------------------------------------------------------------------------
CStageImgui::~CStageImgui()
{
}

//-----------------------------------------------------------------------------
// 初期化
//-----------------------------------------------------------------------------
HWND CStageImgui::Init(HWND hWnd, LPDIRECT3DDEVICE9 pDevice)
{
	// 初期化
	HWND outWnd = CImguiProperty::Init(hWnd, pDevice);

	// 鉄球データを初期化
	m_Pendulum.type = 0;
	m_Pendulum.coefficient = 0.0f;
	m_Pendulum.destRot.x = 0.0f;
	m_Pendulum.destRot.y = 0.0f;
	m_Pendulum.destRot.z = 0.0f;

	// ボールデータを初期化
	m_Ball.rotType = 0;
	m_Ball.radius = 0.0f;
	m_Ball.rotSpeed = 0.0f;

	// 丸太データを初期化
	m_Wood.coolTime = 0;
	m_Wood.rot = 0.0f;
	m_Wood.rotSpeed = 0.0f;
	m_Wood.move = 0.0f;
	for (int i = 0; i < D3DXyz; i++)
	{
		m_sliderRot3[i] = 0.0f;
		m_sliderNowRot3[i] = 0.0f;
		m_PosVSet[i] = 0.0f;
		m_PosRSet[i] = 0.0f;
		m_posRDest[i] = 10.0f;
		m_posVDest[i] = 0.0f;
		m_fDistance[i] = 1.0f;
	}
	m_posVDest[1] = D3DX_PI;

	m_BossAnimation = 0;
	m_KeySet = 0;
	m_indexModel = 0;
	m_indexNouEnemy = 0;
	return outWnd;
}

//-----------------------------------------------------------------------------
// 終了
//-----------------------------------------------------------------------------
void CStageImgui::Uninit(HWND hWnd, WNDCLASSEX wcex)
{
	CImguiProperty::Uninit(hWnd, wcex);
}

//-----------------------------------------------------------------------------
// 更新
//-----------------------------------------------------------------------------
bool CStageImgui::Update()
{


	CImguiProperty::Update();
	ImguiCreate();
	// テキスト表示
	ImGui::Text("FPS  : %.2f", ImGui::GetIO().Framerate);

	D3DXVECTOR3 Vec = CGame::GetStage()->GetPlayer()->GetPos();
	ImGui::Text("POS  : %.2f,%.2f,%.2f", Vec.x, Vec.y, Vec.z);
	ImGui::Separator();

	TaskBarUpdate();	// タスクバーの更新
	EditModel();		// モデルの編集
	EditMesh();			// メッシュの編集
	EditCamera();
	EditEnemy();
	ImguiPlarer();
	Imguifilecord();
	//ImGui::SliderFloat3(u8"ロット", &sliderRot.x, -3.14f, 3.14f);
	//ImGui::Separator();

	static bool checkBox = true;
	if (ImGui::Button(u8"Player初期値"))
	{// ボタンが押された
		CGame::GetStage()->GetPlayer()->Init();
	}
	if (ImGui::Button(u8"ラインモード"))
	{// ボタンが押された
		checkBox = !checkBox;

		
		CManager::GetInstance()->GetRenderer()->SetModeline(checkBox);
	}
	if (ImGui::Button(u8"重力ON！"))
	{// ボタンが押された
		m_Gura = !m_Gura;
		CPlayer* Data = CGame::GetStage()->GetPlayer();
		//Data->SetPos(D3DXVECTOR3(Data->GetPos().x, 0.0f, Data->GetPos().z));
		Data->SetGura(m_Gura);
	}
	if (ImGui::Button(u8"クオータニオン初期化"))
	{// ボタンが押された
		D3DXQUATERNION quat;
		D3DXQuaternionIdentity(&quat);
		D3DXVECTOR3 vecY = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		D3DXQuaternionRotationAxis(&quat, &vecY, D3DX_PI);
		CGame::GetStage()->GetPlayer()->SetQuat(quat);
	}
	

	ImGui::End();
	return false;

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CStageImgui::Draw()
{
	CImguiProperty::Draw();
}

//--------------------------------------------------
// EditModel
//--------------------------------------------------
void CStageImgui::EditModel(void)
{
	static char text[MAX_TEXT] = "";
	static char textModel[MAX_TEXT] = "";
	static char textAnimationPass[MAX_TEXT] = "";
	static char NoutextAnimationPass[MAX_TEXT] = "";
	//モデルを編集するか否か
	if (!ImGui::CollapsingHeader(u8"モデル"))
	{ // モデルを編集しない場合
		return;
	}

	/* ↓モデルを編集する場合↓ */

	// 整数のスライド
	ImGui::SliderInt(u8"発生モデルのタイプ", &m_sliderIntmodel, 0, CStage::MAXMODEL - 1);

	// 整数のスライド
	ImGui::DragFloat3(u8"発生モデルのロット", &m_sliderRot3[0],0.01f, -3.14f, 3.14f) ;
	// ギミックごとの特殊設定の編集
	switch (m_sliderIntmodel)
	{
	case CStage::NORMAL:
		ImGui::Text(u8"当たり判定もない背景用");
		break;
	case CStage::PENDULUM:
		ImGui::Text(u8"鉄球");
		if (ImGui::CollapsingHeader(u8"鉄球設定"))
		{
			// 整数のスライド
			ImGui::SliderInt(u8"鉄球のタイプ", &m_Pendulum.type, 0, 1);
			ImGui::Separator();

			ImGui::SliderFloat(u8"加速と減速", &m_Pendulum.coefficient, 0.0f, 1.0f);
			ImGui::Separator();

			ImGui::SliderFloat3("rot", &m_Pendulum.destRot.x, -D3DX_PI, D3DX_PI);
			ImGui::Separator();
		}
		break;
	case CStage::WOOD:
		ImGui::Text(u8"丸太スポナー");
		if (ImGui::CollapsingHeader(u8"丸太設定"))
		{
			// 整数のスライド
			ImGui::SliderInt(u8"丸太の発生時間", &m_Wood.coolTime, 1, 10000);
			ImGui::Separator();

			ImGui::SliderFloat(u8"rot", &m_Wood.rot, 0.0f, D3DX_PI);

			ImGui::SliderFloat(u8"回転速度", &m_Wood.rotSpeed, 0.0f, 100.0f);

			ImGui::SliderFloat(u8"丸太の速さ", &m_Wood.move, 0.0f, 100.0f);
			ImGui::Separator();

		}
		break;
	case CStage::BALL:
		ImGui::Text(u8"回る玉設定");
		if (ImGui::CollapsingHeader(u8"回る玉設定"))
		{
			// 整数のスライド
			ImGui::SliderInt(u8"回り方", &m_Ball.rotType, 0, 1);
			ImGui::Separator();

			ImGui::SliderFloat(u8"ランド", &m_Ball.radius, 0.0f, 1000.0f);
			ImGui::Separator();

			ImGui::SliderFloat(u8"rotはやさ", &m_Ball.rotSpeed, 0.0f, 10.0f);
			ImGui::Separator();
		}
		break;
	case CStage::RAIL:
		ImGui::Text(u8"レール");
		break;
	case CStage::CHANGE:
		ImGui::Text(u8"チェックポイント");
		break;
	case CStage::GOAL:
		ImGui::Text(u8"ゴール");
		break;
	case CStage::DASHITEM:
		ImGui::Text(u8"加速アイテム");
		break;
	case CStage::BANE:
		ImGui::Text(u8"跳ねる");
		break;
	case CStage::BELL:
		ImGui::Text(u8"スコア加算");
		break;
	case CStage::CAMERA:
		ImGui::Text(u8"カメラ再生してくれる");
		if (ImGui::CollapsingHeader(u8"カメラプレイヤーの設定"))
		{
			ImGui::InputText(u8"読み込むなまえ", &textAnimationPass[0], sizeof(textAnimationPass));
			m_CameraAnimationPass = textAnimationPass;
		}
		break;

	case CStage::MAXMODEL:
		break;
	default:
		break;
	}

	//もし最大数超えてたら戻す
	if (CGame::GetStage()->GetNumAllBuilding() <= m_indexBuilding)
	{
		m_indexBuilding = CGame::GetStage()->GetNumAllBuilding();
	}

	//--------------------
	//配置してたモデル設定
	//--------------------
	if (ImGui::BeginCombo(u8"設置したモデル", m_NowModelName.c_str(), 0))
	{//特殊設定
		for (int i = 0; i < CGame::GetStage()->GetNumAllBuilding(); i++)
		{
			if (CObject::SelectModelObject(m_indexBuilding, CObject::BUILDING) != nullptr)
			{
				std::string ModelName;
				const bool is_selected = (m_indexBuilding == i);
				CBuilding* Obj = dynamic_cast<CBuilding*>(CObject::SelectModelObject(i, CObject::BUILDING));  // ダウンキャスト 

				ModelName = Obj->GetFileName();
				ModelName += std::to_string(i);


				if (ImGui::Selectable(ModelName.c_str(), is_selected))
				{// 選ばれた選択肢に変更
					m_indexBuilding = i;
					m_NowModelName = ModelName;
				}

				if (is_selected)
				{// 選択肢の項目を開く
					ImGui::SetItemDefaultFocus();
				}

				ModelName = Obj->GetFileName();
				ModelName += std::to_string(i);
			}
		}
		ImGui::EndCombo();
	}

	if (CGame::GetStage()->GetNumAllBuilding() <= m_indexBuilding)
	{
		m_indexBuilding = CGame::GetStage()->GetNumAllBuilding();
	}

	if (CObject::SelectModelObject(m_indexBuilding, CObject::BUILDING) != nullptr)
	{
		CBuilding* Obj = dynamic_cast<CBuilding*>(CObject::SelectModelObject(m_indexBuilding, CObject::BUILDING));  // ダウンキャスト 

		// ギミックごとの特殊設定の編集
		switch (Obj->GetModelType())
		{
		case CStage::NORMAL:
			break;
		case CStage::PENDULUM:
			if (ImGui::CollapsingHeader(u8"選択した鉄球設定"))
			{

				CPendulum* objBall = dynamic_cast<CPendulum*>(Obj);  // ダウンキャスト

				ImGui::Checkbox(u8"動かさない", &m_MoveIron);

				if (m_MoveIron)
				{
					objBall->GetRotNon();
				}

				static Pendulum ModelPendulum;

				//--------------------
				//ほしい情報の取得設定
				//--------------------
				ModelPendulum.type = objBall->GetType();
				ModelPendulum.coefficient = objBall->GetCoefficient();
				ModelPendulum.destRot.x = objBall->GetDestRot().x;
				ModelPendulum.destRot.y = objBall->GetDestRot().y;
				ModelPendulum.destRot.z = objBall->GetDestRot().z;

				// 整数のスライド
				ImGui::SliderInt(u8"鉄球のタイプ", &ModelPendulum.type, 0, 1);
				ImGui::Separator();

				ImGui::SliderFloat(u8"加速と減速", &ModelPendulum.coefficient, 0.0f, 1.0f);
				ImGui::Separator();

				ImGui::SliderFloat3("rot", &ModelPendulum.destRot.x, -3.14f, 3.14f);
				ImGui::Separator();

				objBall->SetDestRot(ModelPendulum.destRot);
				objBall->SetCoefficient(ModelPendulum.coefficient);
				objBall->SetType(ModelPendulum.type);
			}
			break;
		case CStage::WOOD:
			if (ImGui::CollapsingHeader(u8"選択した丸太設定"))
			{
				static Wood ModelWood;
				CWood_Spawn* objWood = dynamic_cast<CWood_Spawn*>(Obj);	// ダウンキャスト

				//--------------------
				// ほしい情報の取得設定
				//--------------------
				ModelWood.coolTime = objWood->GetCoolTime();
				ModelWood.rot = objWood->GetRot();
				ModelWood.rotSpeed = objWood->GetRotSpeed();
				ModelWood.move = objWood->GetMoveSpeed();

				ImGui::SliderInt(u8"丸太の発生時間", &ModelWood.coolTime, 1, 10000);

				ImGui::DragFloat3(u8"rot", &ModelWood.rot, 0.0f, 3.14f, 0.5f);

				ImGui::SliderFloat(u8"回転速度", &ModelWood.rotSpeed, 0.0f, 100.0f);

				ImGui::SliderFloat(u8"丸太の速さ", &ModelWood.move, 0.0f, 100.0f);
				ImGui::Separator();

				objWood->SetRot(ModelWood.rot);
				objWood->SetMoveSpeed(ModelWood.move);
				objWood->SetRotSpeed(ModelWood.rotSpeed);
				objWood->SetCoolTime(ModelWood.coolTime);
			}
			break;
		case CStage::BALL:
			if (ImGui::CollapsingHeader(u8"選択した回る玉設定"))
			{
				static Ball ModelBall;
				CBall* objBallModel = dynamic_cast<CBall*>(Obj);	// ダウンキャスト

				// 情報の取得
				ModelBall.rotType = objBallModel->GetRotType();
				ModelBall.radius = objBallModel->GetRadius();
				ModelBall.rotSpeed = objBallModel->GetRotSpeed();

				// 整数のスライド
				ImGui::DragFloat3(u8"選択されてる座標だよ", &m_sliderPos3[0], 0.5f);

				ImGui::SliderInt(u8"回り方", &ModelBall.rotType, 0, 1);
				ImGui::SliderFloat(u8"ランド", &ModelBall.radius, 0.0f, 1000.0f);
				ImGui::SliderFloat(u8"rotはやさ", &ModelBall.rotSpeed, 0.0f, 10.0f);

				ImGui::Separator();

				objBallModel->SetRadius(ModelBall.radius);
				objBallModel->SetRotSpeed(ModelBall.rotSpeed);
				objBallModel->SetRotType(ModelBall.rotType);
				objBallModel->SetCenterPos(D3DXVECTOR3(m_sliderPos3[0], m_sliderPos3[1], m_sliderPos3[2]));
			}
			break;
		case CStage::RAIL:
			break;
		case CStage::CHANGE:
			break;
		case CStage::GOAL:
			break;
		case CStage::CAMERA:
			if (ImGui::CollapsingHeader(u8"今選んでるカメラプレイヤーの設定"))
			{
				CCamera_Player* objCamera＿Player = dynamic_cast<CCamera_Player*>(Obj);	// ダウンキャスト
				
				ImGui::InputText(u8"読み込む選択してるやつのなまえだよ", &NoutextAnimationPass[0], sizeof(NoutextAnimationPass));
				std::string CameraPass = NoutextAnimationPass;
				if (ImGui::Button(u8"設定適応"))
				{// ボタンが押された
					objCamera＿Player->SetPass(CameraPass);
				}
				ImGui::SameLine();
				if (ImGui::Button(u8"今読み込んでるの適応"))
				{// ボタンが押された
					memcpy(NoutextAnimationPass, objCamera＿Player->GetPass().c_str(), sizeof(NoutextAnimationPass));
				}
			}
			break;
		case CStage::MAXMODEL:
			break;
		default:
			break;
		}

		m_sliderPos3[0] = Obj->GetPos().x;
		m_sliderPos3[1] = Obj->GetPos().y;
		m_sliderPos3[2] = Obj->GetPos().z;
		ImGui::DragFloat3(u8"選択されてる座標だよ", &m_sliderPos3[0], 0.5f);
		ImGui::DragFloat3(u8"選択されてるモデルのロットだよ", &m_sliderNowRot3[0], 0.01f, -3.14f, 3.14f);
		Obj->SetRot(D3DXVECTOR3(m_sliderNowRot3[0], m_sliderNowRot3[1], m_sliderNowRot3[2]));
		Obj->SetPos(D3DXVECTOR3(m_sliderPos3[0], m_sliderPos3[1], m_sliderPos3[2]));
	}
	
	if (m_indexModel >= CGame::GetStage()->GetNumAll())
	{
		m_indexModel = CGame::GetStage()->GetNumAll();
	}
	if (ImGui::BeginCombo(u8"今まで読み込みんだモデル", CGame::GetStage()->GetPath(m_indexModel, true).c_str(), 0))
	{// コンボボタン
		for (int i = 0; i < CGame::GetStage()->GetNumAll(); i++)
		{
			const bool is_selected = ((int)m_indexModel == i);

			if (ImGui::Selectable(CGame::GetStage()->GetPath(i, true).c_str(), is_selected))
			{// 選ばれた選択肢に変更
				m_indexModel = i;
				memcpy(text, CGame::GetStage()->GetPath(i, true).c_str(), CGame::GetStage()->GetPath(i, true).size() + 1);
			}

			if (is_selected)
			{// 選択肢の項目を開く
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}

	// テキスト入力
	ImGui::InputText(u8"読み込み", text, sizeof(text));
	ImGui::Separator();

	if (ImGui::Button(u8"読み込みボタン"))
	{// ボタンが押された
		CGame::GetStage()->SetPopItem(text);
		CGame::GetStage()->SetPath(text);
	}
	ImGui::SameLine();
	if (ImGui::Button("data\\MODEL"))
	{// ボタンが押された
		memcpy(text, "data\\MODEL", 10 + 1);
	}
	ImGui::SameLine();
	if (ImGui::Button(u8"Model生成"))
	{// ボタンが押された
		const D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
		CGame::GetStage()->CreateModel(centerPos);
	}

	ImGui::Separator();
}

//--------------------------------------------------
// タスクバーの更新
//--------------------------------------------------
void CStageImgui::TaskBarUpdate(void)
{
	if (!ImGui::BeginMenuBar())
	{//タスクバー
		return;
	}

	auto SaveLoad = [this](const char* name,const char* path)
	{
		if (ImGui::BeginMenu(name))
		{
			HWND Wnd = GetWnd();
			if (ImGui::MenuItem("Save"))
			{
				MessageBox(Wnd, "セーブしますか？", "今消したら保存されてないよ！", MB_OK);
				funcFileSave(Wnd, MESH);
			}
			else if (ImGui::MenuItem("Load"))
			{
				MessageBox(Wnd, "ロードしますか？", "今消したら保存されてないよ！", MB_OK);
				funcFileLoad(Wnd, MESH);
				//CGame::GetStage()->LoadfileMesh(path);
			}

			ImGui::EndMenu();
		}
	};

	auto SaveLoadModel = [this](const char* name, const char* path)
	{
		if (ImGui::BeginMenu(name))
		{
			HWND Wnd = GetWnd();
			if (ImGui::MenuItem("Save"))
			{
				MessageBox(Wnd, "セーブしますか？", "今消したら保存されてないよ！", MB_OK);
				funcFileSave(Wnd, MODEL);
			}
			else if (ImGui::MenuItem("Load"))
			{
				MessageBox(Wnd, "ロードしますか？", "今消したら保存されてないよ！", MB_OK);
				funcFileLoad(Wnd, MODEL);
				//CGame::GetStage()->Loadfile(path);
			}

			ImGui::EndMenu();
		}
	};
	auto SaveLoadEnemy = [this](const char* name, const char* path)
	{
		if (ImGui::BeginMenu(name))
		{
			HWND Wnd = GetWnd();
			if (ImGui::MenuItem("Save"))
			{
				MessageBox(Wnd, "セーブしますか？", "今消したら保存されてないよ！", MB_OK);
				funcFileSave(Wnd, ENEMY);
			}
			else if (ImGui::MenuItem("Load"))
			{
				MessageBox(Wnd, "ロードしますか？", "今消したら保存されてないよ！", MB_OK);
				funcFileLoad(Wnd, ENEMY);
				//CGame::GetStage()->LoadfileEnemy(path);
			}

			ImGui::EndMenu();
		}
	};
	SaveLoad(u8"メッシュ", "data\\MESH\\boss.json");
	SaveLoadModel(u8"モデル", "data\\testBil.json");
	SaveLoadEnemy(u8"エネミー", "data\\testEnemy.json");
	ImGui::EndMenuBar();
}

//--------------------------------------------------
// EditMesh
//--------------------------------------------------
void CStageImgui::EditMesh(void)
{
	static int sliderInt = 10;
	static int AnimationSpeed = 10;
	static float meshSizeFloat[2];
	static float meshMoveFloat;
	static D3DXVECTOR3 meshMovePoptimeFloat;
	static char textmesh[MAX_TEXT] = "";
	static bool Reverse = false;

	int Meshint = 0;

	if (ImGui::CollapsingHeader(u8"メッシュ"))
	{
		for (int Mesh = 0; Mesh < MeshMax; Mesh++)
	{//当たってるmeshチェック

		CMesh* hitPlayerMesh = CGame::GetStage()->GetPlayerHitMesh(Mesh);

		if (hitPlayerMesh != nullptr)
		{
			ImGui::Text(u8"乗ってるメッシュ: %d(Type%d)",hitPlayerMesh->GetNumber(),hitPlayerMesh->GetMeshType());
			Meshint++;
		}
	}
		if (ImGui::CollapsingHeader(u8"制作設定"))
		{
			ImGui::SliderInt(u8"メッシュタイプ", &m_sliderIntmesh, 0, CStage::MAXMESH - 1);
			if (m_sliderIntmesh == CStage::MESH)
			{
				ImGui::Text(u8"普通の");
			}
			else if (m_sliderIntmesh == CStage::MESHMOVE)
			{
				ImGui::Text(u8"線作れる");
			}
			else if (m_sliderIntmesh == CStage::FLOORINGMOVE)
			{
				ImGui::Text(u8"スベール");
			}
			else if (m_sliderIntmesh == CStage::RUNMESH)
			{
				ImGui::Text(u8"横に動く");
			}

			ImGui::Separator();
		}
		//--------------------
		//どのmeshさわってんの設定
		//--------------------
		if (ImGui::BeginCombo(u8"編集したいmesh", m_NowMeshName.c_str(), 0))
		{// コンボボタン
			for (int i = 0; i < MeshMax; i++)
			{
	
				CMesh* CMesh = CGame::GetStage()->GetMesh(i);
				if (CMesh == nullptr)
				{
					continue;
				}
			
				const bool is_selected = ((int)m_indexmeshNumber == i);
				std::string MoveAnimeData = std::to_string(i);

				if (ImGui::Selectable(MoveAnimeData.c_str(), is_selected))
				{
					//
					m_indexmeshNumber = CMesh->GetNumber();
					m_NowMeshName = std::to_string(m_indexmeshNumber);
					m_indexmeshAnimation = 0;
					m_NowMeshNumberName = std::to_string(m_indexmeshAnimation);
				}
				if (is_selected)
				{// 選択肢の項目を開く
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}
		//--------------------
		//アニメーション設定
		//--------------------
		if (ImGui::CollapsingHeader(u8"ラインを描く一覧"))
		{
			if (ImGui::Button(u8"アニメーション保存ボタン"))
			{// ボタンが押された
				for (int Mesh = 0; Mesh < MeshMax; Mesh++)
				{//当たってるmeshチェック
					if (CGame::GetStage()->GetPlayerHitMesh(Mesh) == nullptr)
					{
						continue;
					}
					CMesh* CMesh = CGame::GetStage()->GetPlayerHitMesh(Mesh);

					if (CMesh->GetMeshType() == CStage::MESHMOVE)
					{
						CMoveMesh* objMoveMesh = dynamic_cast<CMoveMesh*>(CMesh);  // ダウンキャスト		

						objMoveMesh->AddPoptime(CGame::GetStage()->GetPlayer()->GetPos());
						objMoveMesh->SetAnimationSpeed(AnimationSpeed);
					}
				}
			}
		}
		//--------------------
		//アニメーション設定
		//--------------------
		if (ImGui::CollapsingHeader(u8"アニメーション座標一覧"))
		{

			ImGui::Checkbox(u8"アニメーションするかしないか", &m_Animation);
			ImGui::SliderInt(u8"アニメーション速さ", &AnimationSpeed, 10, 100);
			ImGui::Separator();
			//--------------------
			//アニメーション保存
			//--------------------
			if (ImGui::Button(u8"アニメーション保存ボタン"))
			{// ボタンが押された
				for (int Mesh = 0; Mesh < MeshMax; Mesh++)
				{//当たってるmeshチェック
					if (CGame::GetStage()->GetPlayerHitMesh(Mesh) == nullptr)
					{
						continue;
					}
					CMesh* CMesh = CGame::GetStage()->GetPlayerHitMesh(Mesh);

					if (CMesh->GetMeshType() == CStage::MESHMOVE)
					{
						CMoveMesh* objMoveMesh = dynamic_cast<CMoveMesh*>(CMesh);  // ダウンキャスト		

						objMoveMesh->AddPoptime(CGame::GetStage()->GetPlayer()->GetPos());
						objMoveMesh->SetAnimationSpeed(AnimationSpeed);
					}
				}
			}
			ImGui::Separator();

			CMesh* CMesh = CGame::GetStage()->GetMesh(m_indexmeshNumber);
			CMoveMesh* objMoveMesh = dynamic_cast<CMoveMesh*>(CMesh);  // ダウンキャスト		
			if (objMoveMesh != nullptr)
			{
				
				if (ImGui::BeginCombo(u8"アニメーション設定", m_NowMeshNumberName.c_str(), 0))
				{// コンボボタン
					for (int i = 0; i < objMoveMesh->GetPoptimeSize(); i++)
					{
						
						const bool is_selected = ((int)m_indexmeshAnimation == i);
						std::string MoveMeshAnimeData = std::to_string(i);

						if (ImGui::Selectable(MoveMeshAnimeData.c_str(), is_selected))
						{
							// 選ばれた番号に変更
							m_indexmeshAnimation = i;
							// 選ばれた選択肢に変更
							m_NowMeshNumberName = std::to_string(m_indexmeshAnimation);
						}
						if (is_selected)
						{// 選択肢の項目を開く
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}
			}

			if (CMesh != nullptr && objMoveMesh != nullptr&&objMoveMesh->GetPoptimeSize() != 0)
			{
				meshMovePoptimeFloat = objMoveMesh->GetPoptime(m_indexmeshAnimation);
				Reverse = objMoveMesh->GetReverse(m_indexmeshAnimation);

				ImGui::DragFloat3(u8"meshのアニメーション設定座標設定", &meshMovePoptimeFloat.x, 5.0f, 100.0f, 0.5f);
				ImGui::Checkbox(u8"法線反転", &Reverse);

				objMoveMesh->SetPoptime(m_indexmeshAnimation, meshMovePoptimeFloat, Reverse);
			}
		}

		//// 整数のスライド
		ImGui::SliderInt(u8"メッシュサイズ", &sliderInt, 1, 100);
		ImGui::Separator();

		ImGui::DragFloat2(u8"meshの縦横サイズ変更", &meshSizeFloat[0], 0.5f, 0.0f, 100.0f );

		ImGui::DragFloat(u8"Meshのmove", &meshMoveFloat, 0.5f, 0.0f, 100.0f);

		if (ImGui::BeginCombo(u8"今まで読み込みんだテクスチャ", CGame::GetStage()->GetPathTex(m_indexTex, true).c_str(), 0))
		{// コンボボタン
			for (int i = 0; i < CGame::GetStage()->GetNumAllTex(); i++)
			{
				const bool is_selected = ((int)m_indexTex == i);

				if (ImGui::Selectable(CGame::GetStage()->GetPathTex(i, true).c_str(), is_selected))
				{// 選ばれた選択肢に変更
					m_indexTex = i;
					memcpy(textmesh, CGame::GetStage()->GetPathTex(i, true).c_str(), CGame::GetStage()->GetPathTex(i, true).size() + 1);
				}

				if (is_selected)
				{// 選択肢の項目を開く
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		ImGui::InputText(u8"テクスチャ読み込み", textmesh, sizeof(textmesh));
		ImGui::Separator();

		if (ImGui::Button("data\\TEXTURE"))
		{// ボタンが押された
			memcpy(textmesh, "data\\TEXTURE", 12 + 1);
		}
		ImGui::SameLine();

		//--------------------
		//meshのシステム設定
		//--------------------
		if (ImGui::Button(u8"メッシュ変更"))
		{// ボタンが押された
			for (int meshCnt = 0; meshCnt < 100; meshCnt++)
			{
				CMesh* getPlayerHitMesh = CGame::GetStage()->GetPlayerHitMesh(meshCnt);

				if (getPlayerHitMesh != nullptr && !getPlayerHitMesh->GetDeath())
				{
					if (getPlayerHitMesh->GetLanding())
					{
						CMesh* getMesh = CGame::GetStage()->GetMesh(meshCnt);
						getMesh->SetMesh(sliderInt);
						getMesh->SetMove(meshMoveFloat);
						getMesh->SetOneMeshSize(D3DXVECTOR3(meshSizeFloat[0], 0.0f, meshSizeFloat[1]));
						getMesh->SetTexture(textmesh);
						CGame::GetStage()->SetPath(textmesh);
					}

				}
			}
		}
		ImGui::SameLine();
		if (ImGui::Button(u8"Mesh生成"))
		{// ボタンが押された
			const D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
			CGame::GetStage()->CreateMesh(centerPos);
		}
		if (ImGui::CollapsingHeader(u8"現在出てるメッシュの設定"))
		{
			//--------------------
			//meshの座標を設定
			//--------------------
			float DataBox[100][3];
			for (int meshCnt = 0; meshCnt < 100; meshCnt++)
			{
				CMesh* getMesh = CGame::GetStage()->GetMesh(meshCnt);
				if (getMesh != nullptr && !getMesh->GetDeath())
				{
					std::string MeshName = "Mesh";

					MeshName += std::to_string(getMesh->GetNumber());

					DataBox[meshCnt][0] = getMesh->GetPos()->x;
					DataBox[meshCnt][1] = getMesh->GetPos()->y;
					DataBox[meshCnt][2] = getMesh->GetPos()->z;
					ImGui::DragFloat3(MeshName.c_str(), &DataBox[meshCnt][0], 10.0f, 10000.0f, 0.5f);
					getMesh->SetPos(D3DXVECTOR3(DataBox[meshCnt][0], DataBox[meshCnt][1], DataBox[meshCnt][2]));

				}
			}
		}
	}
}

//--------------------------------------------------
// EditMesh
//--------------------------------------------------
void CStageImgui::EditCamera(void)
{
	static char textSeve[MAX_TEXT] = "CameraAnimation";
	static char textLoad[MAX_TEXT] = "CameraAnimation";
	if (ImGui::CollapsingHeader(u8"カメラ"))
	{
		if (CRenderer::GetCamera()->GetAnimationSize() != 0)
		{
			if (ImGui::BeginCombo(u8"設定したいアニメーション選択", m_NowCameraNumberName.c_str(), 0))
			{// コンボボタン
				for (int i = 0; i < CRenderer::GetCamera()->GetAnimationSize(); i++)
				{
					const bool is_selected = ((int)m_indexCamera == i);
					std::string ComboData = std::to_string(i);
					if (ImGui::Selectable(ComboData.c_str(), is_selected))
					{// 選ばれた選択肢に変更
						m_indexCamera = i;
						m_NowCameraNumberName = std::to_string(m_indexCamera);
					}

					if (is_selected)
					{// 選択肢の項目を開く
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}

			if (CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetkeySetPoptimeSize() != 0)
			{
				if (ImGui::BeginCombo(u8"編集したいアニメーションの座標選択", m_NowCameraAnimationNumberName.c_str(), 0))
				{// コンボボタン
					for (int i = 0; i < CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetkeySetPoptimeSize(); i++)
					{
						const bool is_selected = ((int)m_indexCameraAnimation == i);
						std::string ComboData = std::to_string(i);
						if (ImGui::Selectable(ComboData.c_str(), is_selected))
						{// 選ばれた選択肢に変更
							m_indexCameraAnimation = i;
							m_NowCameraAnimationNumberName = std::to_string(m_indexCameraAnimation);
						}

						if (is_selected)
						{// 選択肢の項目を開く
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}

				float AnimationBoxPosV[3];
				float AnimationBoxPosR[3];
				int AnimationKeySet;
				bool Izing;

				if (m_indexCameraAnimation >= CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetkeySetPoptimeSize())
				{
					m_indexCameraAnimation = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetkeySetPoptimeSize();
				}
				
				AnimationBoxPosV[0] = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetPosVPoptime(m_indexCameraAnimation).x;
				AnimationBoxPosV[1] = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetPosVPoptime(m_indexCameraAnimation).y;
				AnimationBoxPosV[2] = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetPosVPoptime(m_indexCameraAnimation).z;

				AnimationBoxPosR[0] = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetPosRPoptime(m_indexCameraAnimation).x;
				AnimationBoxPosR[1] = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetPosRPoptime(m_indexCameraAnimation).y;
				AnimationBoxPosR[2] = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetPosRPoptime(m_indexCameraAnimation).z;

				AnimationKeySet = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetkeySetPoptime(m_indexCameraAnimation);

				Izing = CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetIzing(m_indexCameraAnimation);

				ImGui::DragFloat3("PosV", &AnimationBoxPosV[0], 10.0f, 10000.0f, 0.5f);
				ImGui::DragFloat3("PosR", &AnimationBoxPosR[0], 10.0f, 10000.0f, 0.5f);
				ImGui::SliderInt(u8"キーフレーム数", &AnimationKeySet, 1, 1000);
				ImGui::Checkbox(u8"イージング", &Izing);

				CRenderer::GetCamera()->GetAnimation(m_indexCamera)->SetPosVPoptime(m_indexCameraAnimation, D3DXVECTOR3(AnimationBoxPosV[0], AnimationBoxPosV[1], AnimationBoxPosV[2]));
				CRenderer::GetCamera()->GetAnimation(m_indexCamera)->SetPosRPoptime(m_indexCameraAnimation, D3DXVECTOR3(AnimationBoxPosR[0], AnimationBoxPosR[1], AnimationBoxPosR[2]));
				CRenderer::GetCamera()->GetAnimation(m_indexCamera)->SetkeySetPoptime(m_indexCameraAnimation, AnimationKeySet);
				CRenderer::GetCamera()->GetAnimation(m_indexCamera)->SetIzing(m_indexCameraAnimation, Izing);

				CRenderer::GetCamera()->GetAnimation(m_indexCamera)->Change();


			}
		}
		if (ImGui::Button(u8"アニメーション作成"))
		{// ボタンが押された
			CRenderer::GetCamera()->AddAnimation();
			m_indexCameraAnimation = 0;

		}
		if (ImGui::Button(u8"自由カメラモード切り替え"))
		{// ボタンが押された
			CRenderer::GetCamera()->Setisfree();

		}
		if (ImGui::Button(u8"アニメーション再生"))
		{// ボタンが押された
			CRenderer::GetCamera()->PlayAnimation(m_indexCamera,false);
		}
		if (ImGui::Button(u8"カメラの初期化"))
		{// ボタンが押された
			CRenderer::GetCamera()->GameInit();
			/*CRenderer::GetCamera()->GetAnimation(m_indexCamera)->LastDelete();
			if (m_indexCameraAnimation == CRenderer::GetCamera()->GetAnimation(m_indexCamera)->GetkeySetPoptimeSize())
			{
				m_indexCameraAnimation--;
				m_NowCameraAnimationNumberName = std::to_string(m_indexCameraAnimation);
			}*/
		}
		
		if (ImGui::Button(u8"アニメーション座標保存"))
		{// ボタンが押された
			if (CRenderer::GetCamera()->GetAnimationSize() == 0)
			{
				CRenderer::GetCamera()->AddAnimation();
				m_indexCamera = 0;
			}

			CRenderer::GetCamera()->GetAnimation(m_indexCamera)->AddPosVPoptime(D3DXVECTOR3(m_PosVSet[0], m_PosVSet[1], m_PosVSet[2]));
			CRenderer::GetCamera()->GetAnimation(m_indexCamera)->AddPosRPoptime(D3DXVECTOR3(m_PosRSet[0], m_PosRSet[1], m_PosRSet[2]));
			CRenderer::GetCamera()->GetAnimation(m_indexCamera)->AddkeySetPoptime(m_KeySet);
			CRenderer::GetCamera()->GetAnimation(m_indexCamera)->AddIzing(m_Izing);
		}
		if (!CRenderer::GetCamera()->Getisfree())
		{
			m_PosVSet[0]=CRenderer::GetCamera()->GetPos()->x;
			m_PosVSet[1]=CRenderer::GetCamera()->GetPos()->y;
			m_PosVSet[2]=CRenderer::GetCamera()->GetPos()->z;

			m_PosRSet[0] = CRenderer::GetCamera()->GetPosR()->x;
			m_PosRSet[1] = CRenderer::GetCamera()->GetPosR()->y;
			m_PosRSet[2] = CRenderer::GetCamera()->GetPosR()->z;
		}
		ImGui::DragFloat3(u8"今のカメラを動かすPosV", &m_PosVSet[0], 10.0f, 10000.0f, 0.5f);
		ImGui::DragFloat3(u8"今のカメラを動かすPosR", &m_PosRSet[0], 10.0f, 10000.0f, 0.5f);

		ImGui::DragFloat3(u8"今のカメラを動かすposRDest", &m_posRDest[0], 0.1f, -10000.0f, 10000.0f);
		ImGui::DragFloat3(u8"今のカメラを動かすposVDest", &m_posVDest[0], 0.1f, -10000.0f, 10000.0f);
		ImGui::DragFloat3(u8"今のカメラを動かすfDistance", &m_fDistance[0], 0.1f, -10000.0f, 10000.0f);


		ImGui::SliderInt(u8"今のキーフレーム数", &m_KeySet, 1, 1000);

		ImGui::InputText(u8"保存したい名まえ", textSeve, sizeof(textSeve));
		if (ImGui::Button(u8"セーブ"))
		{// ボタンが押された

			CRenderer::GetCamera()->GetAnimation(m_indexCamera)->Save(textSeve);
		}
		ImGui::InputText(u8"読み込みたい名まえ", textLoad, sizeof(textLoad));
		if (ImGui::Button(u8"ロード"))
		{// ボタンが押された
			CRenderer::GetCamera()->LandAnimation(textLoad);
		}
	}

}
//--------------------------------------------------
// エネミーの設定
//--------------------------------------------------
void CStageImgui::EditEnemy()
{
	static char textEnemy[MAX_TEXT] = "";
	if (ImGui::CollapsingHeader(u8"エネミー設定"))
	{

		ImGui::SliderInt(u8"ENEMYのタイプ", &m_sliderIntEnemy, 0, CStage::MAXENEMY - 1);

		if (ImGui::BeginCombo(u8"今まで読み込みんだモデル", CGame::GetStage()->GetPathEnemy(m_indexEnemy, true).c_str(), 0))
		{// コンボボタン
			for (int i = 0; i < CGame::GetStage()->GetNumAllEnemyPass(); i++)
			{
				const bool is_selected = ((int)m_indexEnemy == i);

				if (ImGui::Selectable(CGame::GetStage()->GetPathEnemy(i, true).c_str(), is_selected))
				{// 選ばれた選択肢に変更
					m_indexEnemy = i;
					memcpy(textEnemy, CGame::GetStage()->GetPathEnemy(i, true).c_str(), CGame::GetStage()->GetPathEnemy(i, true).size() + 1);
				}

				if (is_selected)
				{// 選択肢の項目を開く
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		
		CEnemy* Enemy = dynamic_cast<CEnemy*>(CObject::SelectModelObject(m_indexNouEnemy, CObject::ENEMY));  // ダウンキャスト 
		if (Enemy != nullptr)
		{
			if (ImGui::BeginCombo(u8"設置したエネミー", m_NowEnemyName.c_str(), 0))
			{//特殊設定
				for (int i = 0; i < CGame::GetStage()->GetNumAllEnemy(); i++)
				{
					if (CObject::SelectModelObject(m_indexNouEnemy, CObject::ENEMY) != nullptr)
					{
						std::string ModelName;
						const bool is_selected = (m_indexNouEnemy == i);
					

						ModelName += std::to_string(i);

						if (ImGui::Selectable(ModelName.c_str(), is_selected))
						{// 選ばれた選択肢に変更
							m_indexNouEnemy = i;
							m_NowEnemyName = ModelName;
						}

						if (is_selected)
						{// 選択肢の項目を開く
							ImGui::SetItemDefaultFocus();
						}

						ModelName += std::to_string(i);
					}
				}
				ImGui::EndCombo();
			}

			CEnemy* ChangeEnemy = dynamic_cast<CEnemy*>(CObject::SelectModelObject(m_indexNouEnemy, CObject::ENEMY));  // ダウンキャスト 
			
			float EnemyPos[3];
			float EnemyRot[3];
			float EnemyMin[3];
			float EnemyMax[3];

			EnemyPos[0] = ChangeEnemy->GetPos().x;
			EnemyPos[1] = ChangeEnemy->GetPos().y;
			EnemyPos[2] = ChangeEnemy->GetPos().z;

			EnemyRot[0] = ChangeEnemy->GetRot().x;
			EnemyRot[1] = ChangeEnemy->GetRot().y;
			EnemyRot[2] = ChangeEnemy->GetRot().z;

			EnemyMin[0] = ChangeEnemy->GetMin().x;
			EnemyMin[1] = ChangeEnemy->GetMin().y;
			EnemyMin[2] = ChangeEnemy->GetMin().z;

			EnemyMax[0] = ChangeEnemy->GetMax().x;
			EnemyMax[1] = ChangeEnemy->GetMax().y;
			EnemyMax[2] = ChangeEnemy->GetMax().z;

			//スクロール
			ImGui::DragFloat3(u8"エネミーの座標", &EnemyPos[0], 10.0f, -10000.0f, 10000.0f);
			ImGui::DragFloat3(u8"エネミーの回転軸", &EnemyRot[0], 10.0f, 10000.0f, 0.5f);
			ImGui::DragFloat3(u8"当たり判定のさいず（Min）", &EnemyMin[0], 10.0f, 10000.0f, 0.5f);
			ImGui::DragFloat3(u8"当たり判定のさいず（Max）", &EnemyMax[0], 10.0f, 10000.0f, 0.5f);


			switch (ChangeEnemy->GetEnemyType())
			{
			case CStage::BOSS:
			{
				CBoss* BossEnemy = dynamic_cast<CBoss*>(Enemy);  // ダウンキャスト
				EditBoss(BossEnemy);
			}
			default:
				break;
			}
		
			ChangeEnemy->SetPopPos(D3DXVECTOR3(EnemyPos[0], EnemyPos[1], EnemyPos[2]));
			ChangeEnemy->SetPopRot(D3DXVECTOR3(EnemyRot[0], EnemyRot[1], EnemyRot[2]));
			ChangeEnemy->SetMin(D3DXVECTOR3(EnemyMin[0], EnemyMin[1], EnemyMin[2]));
			ChangeEnemy->SetMax(D3DXVECTOR3(EnemyMax[0], EnemyMax[1], EnemyMax[2]));
		}
	}
}

//--------------------------------------------------
// BOSS
//--------------------------------------------------
void CStageImgui::EditBoss(CBoss* BossEnemy)
{
	if (ImGui::CollapsingHeader(u8"BOSS設定"))
	{
		if (BossEnemy->GeteventListSize() != 0)
		{
			if (ImGui::BeginCombo(u8"アニメーション", m_BossAnime.c_str(), 0))
			{// コンボボタン
				for (int i = 0; i < BossEnemy->GeteventListSize(); i++)
				{
					const bool is_selected = ((int)m_indexEnemyBoss == i);

					std::string ModelName;
					ModelName += std::to_string(i);
					if (ImGui::Selectable(ModelName.c_str(), is_selected))
					{// 選ばれた選択肢に変更
						m_indexEnemyBoss = i;
						m_BossAnime = ModelName;
					}

					if (is_selected)
					{// 選択肢の項目を開く
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}

			int BossMove;
			int BossMoveType;
			BossMoveType = BossEnemy->GetBossMoveType(m_indexEnemyBoss);
			BossMove = BossEnemy->GeteventList(m_indexEnemyBoss);

			ImGui::SliderInt(u8"選択してるのタイプ", &BossMoveType, 0, CBoss::MAXBOSS - 1);

			ImGui::SliderInt(u8"選択してるキーフレーム数", &BossMove, 1, 1000);
			BossEnemy->SetBossMoveType(m_indexEnemyBoss, (CBoss::BOSSMOVETYPE)BossMoveType);
			BossEnemy->SeteventList(m_indexEnemyBoss, BossMove);
		}
		// 整数のスライド
		ImGui::SliderInt(u8"発生モデルのタイプ", &m_BossAnimationType, 0, CBoss::MAXBOSS - 1);

		ImGui::SliderInt(u8"今のキーフレーム数", &m_BossKeySet, 1, 1000);

		if (ImGui::Button(u8"追加キー"))
		{// ボタンが押された
			BossEnemy->AddeventList(m_BossKeySet);
			BossEnemy->AddeBossMoveType((CBoss::BOSSMOVETYPE)m_BossAnimationType);
		}
	}
}

//--------------------------------------------------
// Playerの動き
//--------------------------------------------------
void CStageImgui::EditPlayer()
{
	if (ImGui::CollapsingHeader(u8"プレイヤーのいろいろ"))
	{
		m_sliderPlayerPos[0] = CGame::GetStage()->GetPlayer()->GetPos().x;
		m_sliderPlayerPos[1] = CGame::GetStage()->GetPlayer()->GetPos().y;
		m_sliderPlayerPos[2] = CGame::GetStage()->GetPlayer()->GetPos().z;

		ImGui::DragFloat3(u8"Playerの設定", &m_sliderPlayerPos[0], 10.0f, 10000.0f, 0.5f);

	}
}

//--------------------------------------------------
// ウインドウ出力処理
//--------------------------------------------------
void CStageImgui::Imguigold(void)
{
	// ウインドウの命名
	ImGui::Begin("gold", nullptr, ImGuiWindowFlags_MenuBar);
	ImGui::End();
}

//--------------------------------------------------
// ウインドウ出力処理
//--------------------------------------------------
void CStageImgui::Imguifilecord(void)
{
	// ウインドウの命名
	static char textSeve[MAX_TEXT] = "STAGE0";
	ImGui::Begin("gold", nullptr, ImGuiWindowFlags_MenuBar);
	ImGui::InputText(u8"保存したい名まえ", textSeve, sizeof(textSeve));

	m_SavefileNeme = textSeve;
	
	ImGui::End();
}

//--------------------------------------------------
// ウインドウ出力処理
//--------------------------------------------------
void CStageImgui::ImguiEnemy(void)
{
	// ウインドウの命名
	ImGui::Begin(u8"Enemy", nullptr, ImGuiWindowFlags_MenuBar);
	ImGui::End();
}

//--------------------------------------------------
// 選んでるモテル
//--------------------------------------------------
bool CStageImgui::GetAnimati()
{
	return m_Animation;
}

//--------------------------------------------------
// 選んでるモテル
//--------------------------------------------------
int CStageImgui::selectModel()
{
	return m_indexBuilding;
}

//--------------------------------------------------
// ウインドウ出力処理
//--------------------------------------------------
void CStageImgui::ImguiCreate(void)
{
	// ウインドウの命名
	ImGui::Begin(u8"生成ボタン", nullptr, ImGuiWindowFlags_MenuBar);

	const D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
	if (ImGui::Button(u8"Mesh生成"))
	{// ボタンが押された
		CGame::GetStage()->CreateMesh(centerPos);
	}
	if (ImGui::Button(u8"Model生成"))
	{// ボタンが押された
		CGame::GetStage()->CreateModel(centerPos);
	}
	if (ImGui::Button(u8"Enemy生成"))
	{// ボタンが押された
		CGame::GetStage()->CreateEnemy(centerPos);
	}
	ImGui::End();
}

//--------------------------------------------------
// ウインドウ出力処理
//--------------------------------------------------
void CStageImgui::ImguiPlarer(void)
{
	// ウインドウの命名
	ImGui::Begin(u8"プレイヤー操作", nullptr, ImGuiWindowFlags_MenuBar);
	float PlayerPos[3];

	PlayerPos[0] = m_PlayerPos.x;
	PlayerPos[1] = m_PlayerPos.y;
	PlayerPos[2] = m_PlayerPos.z;

	ImGui::DragFloat3(u8"プレイヤーの座標", &PlayerPos[0], 10.0f, -10000.0f, 10000.0f);

	m_PlayerPos = D3DXVECTOR3(PlayerPos[0], PlayerPos[1], PlayerPos[2]);
	if (ImGui::Button(u8"読み込み"))
	{// ボタンが押された
		m_PlayerPos = CGame::GetStage()->GetPlayer()->GetPos();
	}
	if (ImGui::Button(u8"適応"))
	{// ボタンが押された
		CGame::GetStage()->GetPlayer()->SetPos(m_PlayerPos);
	}
	ImGui::End();
}

//========================
//ウィンドウだしてやるやつ
//========================
void CStageImgui::ImguiChangenumber(std::string number, int Number)
{
	ImGui::Begin(u8"敵のnumberや！", nullptr, ImGuiWindowFlags_MenuBar);

	std::string Nownumber = number;
	Nownumber += std::to_string(Number);
	ImGui::Text(Nownumber.c_str());

	ImGui::End();
}

//========================
//ウィンドウだしてやるやつ
//========================
void CStageImgui::funcFileSave(HWND hWnd, CStageImgui::DATATYPE Mode)
{
	static OPENFILENAME     ofn;
	static TCHAR            szPath[MAX_PATH];
	static TCHAR            szFile[MAX_PATH];

	if (szPath[0] == TEXT('\0')) {
		GetCurrentDirectory(MAX_PATH, szPath);
	}
	if (ofn.lStructSize == 0) {
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrInitialDir = szPath;	// 初期フォルダ位置
		ofn.lpstrFile = szFile;			// 選択ファイル格納
		ofn.nMaxFile = MAX_PATH;
		ofn.lpstrDefExt = TEXT(".json\0.x");
		ofn.lpstrFilter = TEXT("jsonファイル(*.json)\0*.json\0Xファイル(*.x)\0*.x\0");
		ofn.lpstrTitle = TEXT("テキストファイルを保存します。");
		ofn.Flags = OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
	}
	if (GetSaveFileName(&ofn)) {
		MessageBox(hWnd, szFile, TEXT("ファイル名を付けて保存"), MB_OK);
	}

	if (szFile[0] != '\0')
	{
		if (Mode == CStageImgui::ENEMY)
		{
			CGame::GetStage()->SavefileEnemy(szFile); 
		}
		if (Mode == CStageImgui::MESH)
		{
			CGame::GetStage()->SavefileMesh(szFile);
		}
		if (Mode == CStageImgui::MODEL)
		{
			CGame::GetStage()->Savefile(szFile);
		}
	}
	SetCurrentDirectory(szPath);
}

//========================
//ウィンドウだしてやるやつ
//========================
void CStageImgui::funcFileLoad(HWND hWnd, CStageImgui::DATATYPE Mode)
{
	static OPENFILENAME     ofn;
	static TCHAR            szPath[MAX_PATH];
	static TCHAR            szFile[MAX_PATH];

	if (szPath[0] == TEXT('\0')) {
		GetCurrentDirectory(MAX_PATH, szPath);
	}
	if (ofn.lStructSize == 0) {
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrInitialDir = szPath;	// 初期フォルダ位置
		ofn.lpstrFile = szFile;			// 選択ファイル格納
		ofn.nMaxFile = MAX_PATH;
		ofn.lpstrDefExt = TEXT(".json");
		ofn.lpstrFilter = TEXT("jsonファイル(*.json)\0*.json\0");
		ofn.lpstrTitle = TEXT("テキストファイル読み込む。");
		ofn.Flags = OFN_FILEMUSTEXIST;

	}
	if (GetOpenFileName(&ofn)) {
		//MessageBox(hWnd, szFile, TEXT("これを読み込むぞぉ"), MB_OK);
	}

	if (szFile[0] != '\0')
	{
		SetCurrentDirectory(szPath);
		if (Mode == CStageImgui::ENEMY)
		{
			CGame::GetStage()->LoadfileEnemy(szFile);
		}
		if (Mode == CStageImgui::MESH)
		{
			CGame::GetStage()->LoadfileMesh(szFile);
		}
		if (Mode == CStageImgui::MODEL)
		{
			CGame::GetStage()->Loadfile(szFile);
		}
	}
	SetCurrentDirectory(szPath);
}

