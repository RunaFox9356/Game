//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _MOVEMESH_H_			// このマクロ定義がされてなかったら
#define _MOVEMESH_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "mesh.h"

class Spline;
class CLineMesh;
class CMoveMesh : public CMesh
{
public:

	CMoveMesh();
	~CMoveMesh() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CMoveMesh* Create();

	void AddPoptime(const D3DXVECTOR3 IsPos);
	void SetPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos, const bool IsReverse);
	D3DXVECTOR3 GetPoptime(const int IsPoptime) { return m_Poptime.at(IsPoptime); }

	int GetPoptimeSize() { return m_Poptime.size(); }

	void SetAnimationSpeed(const int IsSpeed) { m_AnimationSpeed = IsSpeed; }

	bool GetReverse(const int IsPoptime) { return m_Reverse.at(IsPoptime); }



	int GetAnimationSpeed() { return m_AnimationSpeed; }
private:
	void move();
	void OnHit() override;

	bool CollisionWall(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld,int line);
	D3DXVECTOR3 m_Testrot;
	D3DXVECTOR3 m_movemesh;
	std::vector <D3DXVECTOR3> m_Poptime;
	std::vector <bool> m_Reverse;
	int m_NowAnimation;
	int m_AnimationSpeed;
	int m_SpeedCount;
	bool m_FastHit;
	CLineMesh* LineMesh;
};
#endif

