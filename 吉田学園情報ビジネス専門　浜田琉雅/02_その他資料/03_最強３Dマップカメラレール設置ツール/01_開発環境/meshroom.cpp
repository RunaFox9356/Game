//============================
//
// 動くmesh設定
// Author:hamada ryuuga
//
//============================

#include "meshroom.h"
#include "utility.h"
#include "stage.h"
#include "player.h"
#include "title.h"
#include "manager.h"
#include "game.h"
//------------------------------------
// コンストラクタ
//------------------------------------
CMeshRoom::CMeshRoom()
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CMeshRoom::~CMeshRoom()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CMeshRoom::Init()
{
	CMesh::Init();

	m_Testrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	return S_OK;
}

//------------------------------------
// 終了
//------------------------------------
void CMeshRoom::Uninit()
{
	CMesh::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CMeshRoom::Update()
{
	CMesh::Update();
	//動き
	CMeshRoom::move();
}

//------------------------------------
// 描画
//------------------------------------
void CMeshRoom::Draw()
{
	CMesh::Draw();
}

//------------------------------------
// create
//------------------------------------
CMeshRoom *CMeshRoom::Create()
{
	CMeshRoom * pObject = new CMeshRoom;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}


//------------------------------------
// 動き系統
//------------------------------------
void CMeshRoom::move()
{
	
}

//------------------------------------
// Playerが当たった時の判定
//------------------------------------
void CMeshRoom::OnHit()
{
	/*m_meshmove = CGame::GetStage::GetStage()->GetPlayer()->GetMove();

	m_meshmove.z *= 1.2f;
	m_meshmove.x *= 1.2f;

	CGame::GetStage::GetStage()->GetPlayer()->SetMove(m_meshmove);*/

	CGame::GetStage()->GetPlayer()->SetFriction(-0.48f);
}