//=============================================================================
//
// BOSS
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _BOSS_H_
#define	_BOSS_H_

#include"enemy.h"

namespace nl = nlohmann;
//**************************************************
// クラス
//**************************************************
class  CBoss : public CEnemy
{
public:

	

	enum BOSSMOVETYPE
	{
		NONE = 0,		// 通常
		METEO,		// 攻撃
		MOVE,		//動く
		MAXBOSS			//あんただれや？
	};


	CBoss();
	~CBoss() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CBoss *Create(D3DXVECTOR3 pos);
	void Move();
	void quat() {}
	void Hit()override;
	void NotHit()override;

	//	Bossのイベントまでの時間追加
	void AddeventList(const int IseventList) { m_eventList.push_back(IseventList); }
	//	Bossのイベントまでの時間セット
	void SeteventList(const int IsPoptime, const int IseventList) { m_eventList.at(IsPoptime) = IseventList; }
	//	Bossのイベントまでの時間ゲット
	int GeteventList(const int IsPoptime) { return m_eventList.at(IsPoptime); }
	//	Bossのイベントまでの時間の総数取得
	int GeteventListSize() { return m_eventList.size(); }

	//	Bossのイベントタイプを追加
	void AddeBossMoveType(const BOSSMOVETYPE IsBossMoveType) { m_BossMoveType.push_back(IsBossMoveType); }
	//	Bossのイベントタイプをセット
	void SetBossMoveType(const int IsPoptime, const BOSSMOVETYPE IsBossMoveType) { m_BossMoveType.at(IsPoptime) = IsBossMoveType; }
	//	Bossのイベントタイプをゲット
	int GetBossMoveType(const int IsPoptime) { return m_BossMoveType.at(IsPoptime); }
	//	Bossのイベントタイプの総数の取得
	int GetBossMoveTypetSize() { return m_BossMoveType.size(); }


	void BossEventSeve();
	void BossEventLood();
private:
	bool m_hit;
	bool m_isMove;
	CLine*Line[12];
	D3DXVECTOR3 m_move;
	int m_frameCount;
	int m_nouevent;

	nl::json JBoss;//リストの生成
	std::vector <int>m_eventList;

	std::vector <BOSSMOVETYPE>m_BossMoveType;

	D3DXVECTOR3 m_posOld;


};

#endif // !_PENBULUM_H_


