//=============================================================================
//
// 建物
// Author:hamada ryuuga
//
//=============================================================================
//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "building.h"
#include "hamada.h"
#include "manager.h"
#include "stage.h"
#include "title.h"
#include "player.h"
#include "stage_imgui.h"
#include "game.h"
#include "utility.h"

//-----------------------------------------------------------------------------
// 定数
//-----------------------------------------------------------------------------
int CBuilding::m_NowNumber = 0;

//-----------------------------------------------------------------------------
// コンストラクタ
//-----------------------------------------------------------------------------
CBuilding::CBuilding(int nPriority) : CObjectX(nPriority)
{
}

//-----------------------------------------------------------------------------
// デストラクタ
//-----------------------------------------------------------------------------
CBuilding::~CBuilding()
{
}

//-----------------------------------------------------------------------------
// 初期化
//-----------------------------------------------------------------------------
HRESULT CBuilding::Init()
{
	m_Gold = false;
	m_ChangePoptime = false;
	CObjectX::Init();
	SetModelNumber(m_NowNumber);
	m_NowNumber++;
	return E_NOTIMPL;
}

//-----------------------------------------------------------------------------
// 終了
//-----------------------------------------------------------------------------
void CBuilding::Uninit()
{
	CObjectX::Uninit();
}

//-----------------------------------------------------------------------------
// 更新
//-----------------------------------------------------------------------------
void CBuilding::Update()
{
	GetFileName();
	//動き
	CBuilding::move();

	CObjectX::Update();

}

//-----------------------------------------------------------------------------
// 描画
//-----------------------------------------------------------------------------
void CBuilding::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();
	//アルファブレンディングを加算合成に設定
	//pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	//pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	//Ｚ軸で回転しますちなみにm_rotつかうとグルグル回ります
	//m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	D3DXMATRIX mtxWorld = GetWorldMtx();

	mtxWorld = *hmd::giftmtx(&mtxWorld, GetPos(), GetRot());

	SetWorldMtx(mtxWorld);

	CObjectX::Draw();

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//-----------------------------------------------------------------------------
// create
//-----------------------------------------------------------------------------
CBuilding *CBuilding::Create(const char * pFileName,D3DXVECTOR3 *pPos)
{
	CBuilding * pObject = nullptr;
	pObject = new CBuilding();

	CStageImgui* imgui = CGame::GetStageImgui();

	if (pObject != nullptr)
	{
		pObject->Init();
		pObject->SetModel(pFileName);
		pObject->SetUp(BUILDING);
		pObject->SetPos(*pPos);
		pObject->SetMove(D3DXVECTOR3(0.0f,0.0f,0.0f));
		pObject->SetRot(imgui->GetRot());
		pObject->SetFileName(pFileName);
		
	}
	return pObject;
}

//-----------------------------------------------------------------------------
// 動き系統
//-----------------------------------------------------------------------------
void CBuilding::move()
{
	const D3DXVECTOR3* centerPos = &CGame::GetStage()->GetPlayer()->GetPos();

	CStageImgui* imgui = CGame::GetStageImgui();

	if (m_Gold)
	{
		float Size = 100.0f;
		if ((GetPos().z - Size) <= (centerPos->z) &&
			(GetPos().z) + Size >= (centerPos->z) &&
			(GetPos().x - Size) <= (centerPos->x) &&
			(GetPos().x + Size) >= (centerPos->x))
		{
			//gold
			imgui->Imguigold();
		}
	}
	if (m_ChangePoptime)
	{
		float Size = 100.0f;
		if ((GetPos().z - Size) <= (centerPos->z) &&
			(GetPos().z) + Size >= (centerPos->z) &&
			(GetPos().x - Size) <= (centerPos->x) &&
			(GetPos().x + Size) >= (centerPos->x))
		{
			//gold
			imgui->Imguigold();
			
		}
	}
	D3DXVECTOR3 Move = GetMove();
	MovePos(Move);
}
//--------------------------------------------------
// 当たり判定　線分
//--------------------------------------------------
bool CBuilding::CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize)
{
	D3DXMATRIX mtxWorld = CObjectX::GetWorldMtx();

	bool bIsLanding = false;

	D3DXVECTOR3 min = CObjectX::GetVtxMin();
	D3DXVECTOR3 max = CObjectX::GetVtxMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			Hit();
			bIsLanding = true;
		}
	}
	else
	{
		NotHit();
	}

	return bIsLanding;
}