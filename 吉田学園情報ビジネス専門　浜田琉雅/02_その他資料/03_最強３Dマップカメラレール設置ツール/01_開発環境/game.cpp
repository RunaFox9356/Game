//============================
//
// ゲーム画面
// Author:hamada ryuuga
//
//============================

//-----------------------------------------------------------------------------
// インクルード
//-----------------------------------------------------------------------------
#include "game.h"
#include "input.h"
#include "manager.h"
#include "light.h"
#include "objectX.h"
#include "fade.h"
#include "multiply.h"
#include "particle_manager.h"
#include "score.h"
#include "sound.h"
#include "pause.h"
#include "stage.h"
#include "stage_imgui.h"

//-----------------------------------------------------------------------------
// 静的メンバー変数の宣言
//-----------------------------------------------------------------------------
CPause *CGame::m_Pause = nullptr;
CStage* CGame::m_Stage = nullptr;
CStageImgui*  CGame::m_imgui = nullptr;
int CGame::m_GameScore = 0;

//=============================================================================
// コンストラクター
//=============================================================================
CGame::CGame()
{

}

//=============================================================================
// デストラクト
//=============================================================================
CGame::~CGame()
{
}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CGame::Init(void)
{
	m_GameCount = 0;
	m_SpeedUp = 300;
	m_nCntSpawn = 0;
	m_Pattern = PATTERN_0;

	m_imgui = new CStageImgui;
	m_imgui->Init(GetWnd(), CManager::GetInstance()->GetRenderer()->GetDevice());

	m_GameScore = 0;

	m_Pause = new CPause;
	m_Pause->Init();
	m_Pause->SetUp(CObject::PAUSE);

	m_Stage = CStage::Create();

	
	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CGame::Uninit(void)
{
	CManager::GetInstance()->GetSound()->Stop();

	if (m_Pause != nullptr)
	{
		m_Pause->Uninit();
		m_Pause = nullptr;
	}

	CObject::Release();
	CObjectXManager::ReleaseAll();
}

//=============================================================================
// 更新
//=============================================================================
void CGame::Update(void)
{
	m_imgui->Update();

	CInput *CInputpInput = CInput::GetKey();
//	pScore->Add(1+ m_GameScore);
	
	if (CInputpInput->Trigger(CInput::KEY_DEBUG))
	{
		//モードの設定
		CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_RESULT);
		return;
	}
	if (CInputpInput->Trigger(CInput::KEY_F2))
	{
		CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_NAMESET);
		return;
	}
}

//=============================================================================
// 描画
//=============================================================================
void CGame::Draw(void)
{
}
