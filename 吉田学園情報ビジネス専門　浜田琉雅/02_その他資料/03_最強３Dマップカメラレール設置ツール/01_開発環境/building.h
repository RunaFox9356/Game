//=============================================================================
//
// 建物
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _BUILDING_H_			// このマクロ定義がされてなかったら
#define _BUILDING_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "objectX.h"
#include "texture.h"
#include "game.h"
#include "stage_imgui.h"


class CBuilding : public CObjectX
{
public:

	virtual void Hit() { CGame::GetStageImgui()->ImguiChangenumber(m_pFileName, m_Number); };
	virtual void NotHit() {};
	static CBuilding *CBuilding::Create(const char * pFileName, D3DXVECTOR3 *pPos);

	explicit CBuilding(int nPriority = PRIORITY_OBJECT);
	~CBuilding() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	void SetGold(bool IsGo) { m_Gold = IsGo; }
	void SetModelNumber(int IsNumber) { m_Number = IsNumber; }
	void SetChangePoptime(bool ChangePoptime) { m_ChangePoptime = ChangePoptime; }

	void SetFileName(const char * pFileName) { m_pFileName = pFileName; }

	std::string GetFileName() { return m_pFileName; }
	bool GetGold() { return m_Gold; }
	bool GetChangePoptime() { return m_ChangePoptime; }
	int GetModelNumber() {return m_Number; }
	bool CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize);
	void quat(){};

private:
	void move();
	std::string m_pFileName;
	D3DXVECTOR3 m_Move;
	bool m_Gold;
	int m_Number;
	static int m_NowNumber;
	bool m_ChangePoptime;


};

#endif

