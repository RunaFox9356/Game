//==================================================
// objectX->h
// Author: Buriya Kota
//==================================================
#ifndef _OBJECTX_H_
#define _OBJECTX_H_

//**************************************************
// インクルード
//**************************************************
#include "main.h"
#include "texture.h"
#include "object.h"
#include "stage.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CModelX;

//**************************************************
// クラス
//**************************************************
class CObjectX : public CObject
{
public:
	explicit CObjectX(int nPriority = 0);
	virtual ~CObjectX() override;

	virtual HRESULT Init() override;
	virtual void Uninit() override;
	virtual void Update() override;
	virtual void Draw() override;

	virtual void quat() = 0;

	void NotDraw();
	// モデル
	void SetModel(const char *filename);
	void SetModelType(CStage::MODELTYPE IsModel) { ModelType = IsModel; }
	const CStage::MODELTYPE GetModelType()const { return ModelType; }

	// ワールドマトリックス
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtxWorld = mtx; }
	const D3DXMATRIX& GetWorldMtx() const { return m_mtxWorld; }

	// 位置
	void SetPos(const D3DXVECTOR3& pos) { m_posOrigin = pos; }
	const D3DXVECTOR3& GetPos() const { return m_posOrigin; }
	void MovePos(const D3DXVECTOR3& move) { m_posOrigin += move; }	// 移動量代入

	void SetScale(const D3DXVECTOR3& IsScale) { m_scale = IsScale; }
	const D3DXVECTOR3& GetScale() const { return m_scale; }

	// 大きさ
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	const D3DXVECTOR3& GetSize() const { return m_size; }
	D3DXVECTOR3 GetVtxMax() { return m_vtxMaxModel; }
	D3DXVECTOR3 GetVtxMin() { return m_vtxMinModel; }

	void SetVtxMax(D3DXVECTOR3 max) { m_vtxMaxModel = max; }
	void SetVtxMin(D3DXVECTOR3 min) { m_vtxMinModel = min; }
	// 移動量
	void SetMove(const D3DXVECTOR3& move) { m_move = move; }
	D3DXVECTOR3 GetMove() { return m_move; }

	// 向き
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);	// 正規化

													// 目的の向き
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);	// 正規化

	void SetLife(const int &life) { m_nLife = life; }

	void SetQuat(bool Isquat) { m_quat = Isquat; }

	//D3DXVECTOR3 *GetPosPointer() { return &m_posOrigin; }

	CModelX* GetXData() { return m_modelXData; };
	void DrawMaterial();

private:
	CModelX* m_modelXData;

private:
	bool m_quat;
	// size
	D3DXVECTOR3 m_scale;
	// 位置
	D3DXVECTOR3 m_posOrigin;
	// 移動
	D3DXVECTOR3 m_move;
	// 向き
	D3DXVECTOR3 m_rot;
	// 向き
	D3DXVECTOR3 m_rotDest;
	// 最小値
	D3DXVECTOR3 m_vtxMinModel;
	// 最大値
	D3DXVECTOR3 m_vtxMaxModel;
	// ワールドマトリックス
	D3DXMATRIX m_mtxWorld;
	// テクスチャの列挙型

	// 頂点バッファへのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;

	// 大きさ
	D3DXVECTOR3 m_size;
	// 体力
	int m_nLife;

	CStage::MODELTYPE ModelType;

	std::unordered_map<unsigned int, D3DXCOLOR> m_materialDiffuse;	// マテリアルのDiffuse
																	//=========================================
																	//ハンドル一覧
																	//=========================================
	IDirect3DTexture9	*pTex0 = NULL;				// テクスチャ保存用
	D3DXHANDLE			m_hmWVP;					// ワールド〜射影行列
	D3DXHANDLE			m_hmWIT;					// ローカル - ワールド変換行列
	D3DXHANDLE			m_hvLightDir;				// ライトの方向
	D3DXHANDLE			m_hvCol;					// 頂点色
	D3DXHANDLE			m_hvEyePos;					// 視点の位置
	D3DXHANDLE			m_hTechnique;				// テクニック
	D3DXHANDLE			m_hTexture;					// テクスチャ
};

//=============================================================================
// モデルマネージャー　
// Author : 浜田琉雅
// 概要 : モデルのデータを保存しておく
//=============================================================================
class CObjectXManager
{
public: /* 定義 */
	static const int MODEL_MAX = 100;

private:
	static CObjectXManager * ms_ObjectXManager;

public:
	CObjectXManager();		// デフォルトコンストラクタ
	~CObjectXManager();	// デストラクタ

public: /* メンバ関数 */
	static CObjectXManager *GetManager();
	CModelX* LoadXfile(const char *pXFileName);	// 指定の読み込み
	static void ReleaseAll();

private: /* メンバ変数 */
	CModelX* m_modelXList[MODEL_MAX];
};

//=============================================================================
// モデルクラスX
// Author : 浜田琉雅
// 概要 : メッシュなどの設定
//=============================================================================
class CModelX
{
public:
	static const int MAX_TEXTURE = 16;
	CModelX()
	{
		for (int i = 0; i < MAX_TEXTURE; i++)
		{
			m_texture[i] = nullptr;
		}
	}		// デフォルトコンストラクタ
	~CModelX() {}	// デストラクタ

	void Release();

	D3DXVECTOR3 GetVtxMax() { return m_vtxMaxModel; }
	D3DXVECTOR3 GetVtxMin() { return m_vtxMinModel; }

private:
	// メッシュ(頂点の集まり)情報へのポインタ
	LPD3DXMESH m_mesh;
	// マテリアル情報へのポインタ	←　1のXファイルに複数のマテリアルが入っている
	LPD3DXBUFFER m_buffMat;
	// マテリアル情報の数
	DWORD m_numMat;
	LPDIRECT3DTEXTURE9 m_texture[MAX_TEXTURE];

	// 最小値
	D3DXVECTOR3 m_vtxMinModel;
	// 最大値
	D3DXVECTOR3 m_vtxMaxModel;
	int m_nType;
	char  m_pXFileName[256];

	friend class CObjectXManager;
	friend class CObjectX;
};

#endif	// _OBJECTX_H_
