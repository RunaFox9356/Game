//============================
//
// ゲーム画面のヘッダー
// Author:hamada ryuuga
//
//============================
#ifndef _GAME_H_		//このマクロが定義されてなかったら
#define _GAME_H_		//2重インクルード防止のマクロ定義

#include "object.h"

 
 
class CPause; 
class CStage;
class CStageImgui;

class CGame :public CObject
{
public:
	enum PATTERN
	{//出現パターン
		PATTERN_0 = 0,
		PATTERN_1,
		PATTERN_2,
		PATTERN_3,
		PATTERN_MAX
	};

	CGame();
	~CGame();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CPause * GetPause() { return m_Pause; };

//	static CScore*GetScore() { return pScore; };

	static CStage* GetStage() { return m_Stage; }
	static int GetGameScore() { return m_GameScore; };
	static CStageImgui* GetStageImgui() { return m_imgui; }

private:
	static CStage* m_Stage;
	static CPause *m_Pause;
	static CStageImgui* m_imgui;

	static int m_GameScore;
	PATTERN m_Pattern;		//パターン

	int m_GameCount;
	int m_SpeedUp;
	int m_nCntSpawn;
};
#endif