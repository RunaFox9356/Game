//=============================================================================
//
// dashitem
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _METEO_H_
#define	_METEO_H_

#include"enemy.h"

class CReaction;

//**************************************************
// クラス
//**************************************************
class  CMeteo : public CEnemy
{
public:
	CMeteo();
	~CMeteo() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CMeteo *Create(D3DXVECTOR3 pos);
	void Move();
	void quat() {}
	void Hit()override;
	void NotHit()override;


	//Model　sizeの設定
	void SetlookMin(const D3DXVECTOR3 ismin) { m_lookmin = ismin; }
	void SetlookMax(const D3DXVECTOR3 ismax) { m_lookmax = ismax; }

	D3DXVECTOR3 GetlookMin() { return m_lookmin; }
	D3DXVECTOR3 GetlookMax() { return m_lookmax; }

	void Setlookline();//当たり判定の可視化
private:
	bool m_hit;
	bool m_isMove;
	CLine*Line[12];
	D3DXVECTOR3 m_move;

	CReaction *m_sensing;

	int m_lookCount;
	//感知範囲
	D3DXVECTOR3 m_lookmin;
	D3DXVECTOR3 m_lookmax;

	D3DXVECTOR3 m_posOld;
	bool CollisionMove(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize);//うごくかどうかの当たり判定

};

#endif // !_PENBULUM_H_


