//=============================================================================
//
// カメラ設定
// Author:hamada ryuuga
//
//=============================================================================
//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "camera.h"
#include "input.h"

#include "manager.h"
#include "player.h"
#include "stage.h"
#include "title.h"
#include "game.h"
#include "utility.h"
#include "camera_animation.h"
#include "stage_imgui.h"

//-----------------------------------------------------------------------------
// 定数
//-----------------------------------------------------------------------------
const D3DXVECTOR3 CCamera::INIT_POSV(0.0f, 0.0f, -750.0f);	// 位置の初期位置
const float CCamera::MOVE_ATTENUATION = 0.1f;	// 減衰係数
const float CCamera::FIELD_OF_VIEW = 45.0f;	// 視野角
const float CCamera::NEAR_VALUE = 10.0f;	// ニア
const float CCamera::FUR_VALUE = 10000.0f;	// ファー

//-----------------------------------------------------------------------------
// コンストラクト
//-----------------------------------------------------------------------------
CCamera::CCamera()
{
	
}

//-----------------------------------------------------------------------------
// デストラクト
//-----------------------------------------------------------------------------
CCamera::~CCamera()
{
}

//-----------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------
void CCamera::Init(void)
{
	m_IsFree = false;
	m_IsAnimation = false;
	m_IsLoop = false;
	m_NouAnimation = 0;
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	//視点　注視点　上方向　設定
	m_posV = D3DXVECTOR3(0.0f, 0.0f, -750.0f);
	m_posR = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_directionR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_directionV = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_fDistance = sqrtf((m_posR.x - m_posV.x) *
		(m_posR.x - m_posV.x) +
		(m_posR.z - m_posV.z) *
		(m_posR.z - m_posV.z));

	m_fDistance = sqrtf((m_posR.y - m_posV.y)*
		(m_posR.y - m_posV.y) +
		(m_fDistance*m_fDistance));

	m_rot.x = atan2f((m_posR.z - m_posV.z),
		(m_posR.y - m_posV.y));
}

//-----------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------
void CCamera::Uninit(void)
{

	for (int AnimationSize = 0; AnimationSize < (int)m_Animation.size(); AnimationSize++)
	{
		delete m_Animation.at(AnimationSize);
		m_Animation.at(AnimationSize) = nullptr;
	}
	m_Animation.clear();
	

}

//-----------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------
void CCamera::Update(void)
{
	if (CGame::GetStage() != nullptr)
	{
		if (m_IsAnimation)
		{
			if (m_NouAnimation >= GetAnimationSize())
			{//Animationのさいず超えてたら再生しない
				m_IsAnimation = false;
				return;
			}
			GetAnimation(m_NouAnimation)->Animation(&m_posV,&m_posR,&m_IsAnimation, m_IsLoop);

			//if(保管の時間を調整できるようにする)
		}
		else if (m_IsFree)
		{
			CStageImgui* imgui = CGame::GetStageImgui();
			m_posV = imgui->GetPosV();
			m_posR = imgui->GetPosR();
		}
		else
		{
			
			CStageImgui* imgui = CGame::GetStageImgui();


			const D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
			const D3DXVECTOR3 Rot = CGame::GetStage()->GetPlayer()->GetRot();


			m_posRDest.x = centerPos.x + sinf(Rot.y - D3DX_PI) * imgui->GetposRDest().x;		//目的の値
			m_posRDest.y = centerPos.y + cosf(Rot.y - D3DX_PI) * imgui->GetposRDest().y;		//目的の値
			m_posRDest.z = centerPos.z + cosf(Rot.y - D3DX_PI) * imgui->GetposRDest().z;

			m_posVDest.x = centerPos.x - sinf(m_rot.y - imgui->GetposVDest().x)* m_fDistance* imgui->GetDistance().x;			//目的の値
			m_posVDest.y = centerPos.y - sinf(m_rot.x - imgui->GetposVDest().y)* m_fDistance* imgui->GetDistance().y;			//目的の値
			m_posVDest.z = centerPos.z - cosf(m_rot.y - imgui->GetposVDest().z)* m_fDistance* imgui->GetDistance().z;

			m_posR.x += (m_posRDest.x - m_posR.x) * MOVE_ATTENUATION;
			m_posR.y += (m_posRDest.y - m_posR.y) * MOVE_ATTENUATION;
			m_posR.z += (m_posRDest.z - m_posR.z) * MOVE_ATTENUATION;

			m_posV.x += (m_posVDest.x - m_posV.x) * MOVE_ATTENUATION;
			m_posV.y += (m_posVDest.y - m_posV.y) * MOVE_ATTENUATION;
			m_posV.z += (m_posVDest.z - m_posV.z) * MOVE_ATTENUATION;

			//正規化
			NormalizeAngle(&m_rot.x);
			NormalizeAngle(&m_rot.y);
		}
	}
}

//-----------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------
void CCamera::Set(int Type)
{
	m_Type = Type;
	LPDIRECT3DDEVICE9  pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();//デバイスのポインタ

	//ビューマトリックスを初期化
	D3DXMatrixIdentity(&m_MtxView);

	//ビューマトリックスの作成
	D3DXMatrixLookAtLH(&m_MtxView,
		&m_posV,
		&m_posR,
		&m_vecU);

	//適用
	pDevice->SetTransform(D3DTS_VIEW, &m_MtxView);

	//プロジェクションマトリックスを初期化
	D3DXMatrixIdentity(&m_MtxProje);

	// プロジェクションマトリックスの作成(透視投影)
	D3DXMatrixPerspectiveFovLH(&m_MtxProje,				// プロジェクションマトリックス
		D3DXToRadian(FIELD_OF_VIEW),					// 視野角
		(float)SCREEN_WIDTH / (float)SCREEN_HEIGHT,		// アスペクト比
		NEAR_VALUE,										// ニア
		FUR_VALUE);										// ファー

//else
//{
//	// プロジェクションマトリックスの作成(平行投影)
//	D3DXMatrixOrthoLH(&m_MtxProje,					// プロジェクションマトリックス
//		(float)SCREEN_WIDTH,								// 幅
//		(float)SCREEN_HEIGHT,								// 高さ
//		-100.0f,											// ニア
//		2000.0f);											// ファー
//}
//適用
	pDevice->SetTransform(D3DTS_PROJECTION, &m_MtxProje);
}


//-----------------------------------------------------------------------------
// データがあるときの読み込み処理
//-----------------------------------------------------------------------------
void CCamera::LandAnimation(std::string filepass)
{
	CCameraAnimation* setAnimation =CCameraAnimation::create();
	setAnimation->Load(filepass);

	m_Animation.push_back(setAnimation);
}
//-----------------------------------------------------------------------------
// 空のデータの作成処理
//-----------------------------------------------------------------------------
void CCamera::AddAnimation()
{
	m_Animation.push_back(CCameraAnimation::create());
}

//-----------------------------------------------------------------------------
// データの取得処理
//-----------------------------------------------------------------------------
CCameraAnimation * CCamera::GetAnimation(const int IsPoptime)
{
	return m_Animation.at(IsPoptime);
}

//-----------------------------------------------------------------------------
// データの取得処理
//-----------------------------------------------------------------------------
int CCamera::GetAnimationSize()
{
	return m_Animation.size();
}

//-----------------------------------------------------------------------------
//	Animationの再生処理
//-----------------------------------------------------------------------------
void CCamera::PlayAnimation(int Animation, bool Isloop)
{
	
		m_NouAnimation = Animation;
		m_IsAnimation = true;
		m_IsLoop = Isloop;
	
}

//-----------------------------------------------------------------------------
//	Animationの再生処理(オーバーロード)
//-----------------------------------------------------------------------------
void CCamera::PlayAnimation(std::string AnimationName,bool Isloop)
{
	if (AnimationName != "")
	{
		for (int Animation = 0; Animation < (int)m_Animation.size(); Animation++)
		{
			if (m_Animation.at(Animation)->GetName().c_str() == AnimationName)
			{
				m_NouAnimation = Animation;
				m_IsAnimation = true;
				m_IsLoop = Isloop;
				break;
			}

		}
	}

}
//-----------------------------------------------------------------------------
// gameのときの初期化
//-----------------------------------------------------------------------------
void CCamera::GameInit()
{
	m_IsAnimation = false;
	m_IsLoop = false;
	m_NouAnimation = 0;
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	//視点　注視点　上方向　設定
	m_posV = D3DXVECTOR3(0.0f, 300.0f, -750.0f);
	m_posR = D3DXVECTOR3(0.0f, 250.0f, 0.0f);
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_directionR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_directionV = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_fDistance = sqrtf((m_posR.x - m_posV.x) *
		(m_posR.x - m_posV.x) +
		(m_posR.z - m_posV.z) *
		(m_posR.z - m_posV.z));

	m_fDistance = sqrtf((m_posR.y - m_posV.y)*
		(m_posR.y - m_posV.y) +
		(m_fDistance*m_fDistance));

	m_rot.x = atan2f((m_posR.z - m_posV.z),
		(m_posR.y - m_posV.y));
	for (int Animation = 0; Animation < GetAnimationSize(); Animation++)
	{
		GetAnimation(Animation)->AnimationReset();
	}

}