//============================
//
// カメラAnimation設定ヘッター
// Author:hamada ryuuga
//
//============================

#ifndef _CAMERA_ANIMATION_H_
#define _CAMERA_ANIMATION_H_

#include "renderer.h"
#include "line.h"

namespace nl = nlohmann;
class CCameraAnimation
{
public:
	CCameraAnimation();
	~CCameraAnimation();

	void Animation(D3DXVECTOR3 *posV, D3DXVECTOR3 *posR, bool*IsAnimation,bool IsLoop);

	void Change();

	void Save(std::string);	// 画面設定
	void Load(std::string);	// 画面設定

	static CCameraAnimation*create();

	//	posVポイントを追加
	void AddPosVPoptime(const D3DXVECTOR3 IsPos) { m_posVAnimationPos.push_back(IsPos); }
	//	posVポイントをセット
	void SetPosVPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_posVAnimationPos.at(IsPoptime) = IsPos; }
	//	posVポイントをゲット
	D3DXVECTOR3 GetPosVPoptime(const int IsPoptime) { return m_posVAnimationPos.at(IsPoptime); }
	//	posVポイントのさいずの取得
	int GetPosVPoptimeSize() { return m_posVAnimationPos.size(); }


	//	PosRポイントを追加
	void AddPosRPoptime(const D3DXVECTOR3 IsPos) { m_posRAnimationPos.push_back(IsPos); }
	//	PosRポイントをセット
	void SetPosRPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_posRAnimationPos.at(IsPoptime) = IsPos; }
	//	PosRポイントをゲット
	D3DXVECTOR3 GetPosRPoptime(const int IsPoptime) { return m_posRAnimationPos.at(IsPoptime); }
	//	PosRポイントのさいずの取得
	int GetPosRPoptimeSize() { return m_posRAnimationPos.size(); }


	//	keySetポイントを追加
	void AddkeySetPoptime(const int IsPos) { keySet.push_back(IsPos); }
	//	keySetポイントをセット
	void SetkeySetPoptime(const int IsPoptime, const int IsPos) { keySet.at(IsPoptime) = IsPos; }
	//	keySetポイントをゲット
	int GetkeySetPoptime(const int IsPoptime) { return keySet.at(IsPoptime); }
	//	keySetポイントのさいずの取得
	int GetkeySetPoptimeSize() { return keySet.size(); }


	//	AnimationRポイントを追加
	void AddAnimationRPoptime(const D3DXVECTOR3 IsPos) { m_AnimationPoptimeR.push_back(IsPos); }
	//	AnimationRポイントをセット
	void SetAnimationRPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_AnimationPoptimeR.at(IsPoptime) = IsPos; }
	//	AnimationRポイントをゲット
	D3DXVECTOR3 GetAnimationRPoptime(const int IsPoptime) { return m_AnimationPoptimeR.at(IsPoptime); }
	//	AnimationRポイントのさいずの取得
	int GetAnimationRPoptimeSize() { return m_AnimationPoptimeR.size(); }

	//	AnimationRポイントを追加
	void AddAnimationVPoptime(const D3DXVECTOR3 IsPos) { m_AnimationPoptimeV.push_back(IsPos); }
	//	AnimationRポイントをセット
	void SetAnimationVPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_AnimationPoptimeV.at(IsPoptime) = IsPos; }
	//	AnimationRポイントをゲット
	D3DXVECTOR3 GetAnimationVPoptime(const int IsPoptime) { return m_AnimationPoptimeV.at(IsPoptime); }
	//	AnimationRポイントのさいずの取得
	int GetAnimationVPoptimeSize() { return m_AnimationPoptimeV.size(); }


	//	Izingポイントを追加
	void AddIzing(const bool IsIzing) { m_Izing.push_back(IsIzing); }
	//	Izingポイントをセット
	void SetIzing(const int IsPoptime, const bool IsIzing) { m_Izing.at(IsPoptime) = IsIzing; }
	//	Izingポイントをゲット
	bool GetIzing(const int IsPoptime) { return m_Izing.at(IsPoptime); }
	//	Izingポイントのさいずの取得
	int GetIzingSize() { return m_Izing.size(); }


	//	末端の削除
	void LastDelete();


	void AnimationReset();

	std::string GetName() { return m_Name; }
private:

	float m_movespline;
	int m_Cntspline;

	int m_nCntFrame;
	int m_moveCount;

	int m_nCntFrameV;
	int m_moveCountV;

	int m_nCntFrameR;
	int m_moveCountR;
	std::vector <int>keySet;
	std::vector <D3DXVECTOR3> m_posVAnimationPos;
	std::vector <D3DXVECTOR3> m_posRAnimationPos;

	std::vector <D3DXVECTOR3> m_AnimationPoptimeV;
	std::vector <D3DXVECTOR3> m_AnimationPoptimeR;

	std::vector <bool> m_Izing;
	std::vector <CLine*> m_LineV;
	std::vector <CLine*> m_LineR;
	D3DXVECTOR3 m_posVDest;
	D3DXVECTOR3 m_posRDest;
	D3DXVECTOR3 m_IzingbaseV;
	D3DXVECTOR3 m_IzingbaseR;
	std::string m_Name;
	nl::json m_Camera;//リストの生成


};


#endif