//============================
//
// ２Dpolygon設定
// Author:hamada ryuuga
//
//============================

#include "stage.h"
#include "object.h"
#include "utility.h"

#include "letter.h"
#include "manager.h"

#include "building.h"
#include "mesh.h"
#include "movemesh.h"
#include "meshroom.h"
#include "runmesh.h"
#include "player.h"
#include "imgui_property.h"

#include "input.h"
#include "objectX.h"

#include "ball.h"
#include"wood_spawn.h"
#include "pendulum.h"

#include "stage.h"
#include "game.h"

#include "manager.h"
#include "renderer.h"
#include "stage_imgui.h"
#include "rail.h"
#include "dashitem.h"
#include "sprint.h"
#include "bell.h"
#include "camera_player.h"
#include "enemy.h"
#include "chaseenemy.h"
#include "boss.h"

namespace nl = nlohmann;

nl::json JBuilding;//リストの生成
nl::json JlistMesh;//リストの生成
nl::json JlistEnemy;//リストの生成

//------------------------------------
// コンストラクタ
//------------------------------------
CStage::CStage()
{
	m_numAll = 0;
	m_numAllMesh = 0;
	m_numAllTex = 0;
	m_numAllBuilding = 0;
	m_numAllEnemyPass = 0;
	m_numAllEnemy = 0;
}

//------------------------------------
// デストラクタ
//------------------------------------
CStage::~CStage()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT  CStage::Init()
{
	
	m_Player = CPlayer::Create();
	m_Player->SetMotion("data/SYSTEM/Gon/Fox.txt");

	//m_mesh[0] = CMesh::Create();

	SetPopItem("data\\MODEL\\Bell\\suzu.x");
	SetPath("data\\MODEL\\Bell\\suzu.x");
	SetPath("data\\MODEL\\sign.x");
	SetPath("data\\MODEL\\pole.x");
	SetPath("data\\MODEL\\FlowerBlue.x");
	SetPath("data\\MODEL\\FlowerPnk.x");
	SetPath("data\\MODEL\\FlowerRed.x");
	SetPath("data\\MODEL\\FlowerYellow.x");
	SetPath("data\\MODEL\\tree.x");
	SetPath("data\\MODEL\\Bigkabe.x");
	SetPath("data\\MODEL\\hasiraMini.x");
	SetPath("data\\MODEL\\hasisi.x");
	SetPathTex("data\\TEXTURE\\BG.png");
	SetPathEnemy("data\\system\\enemy\\snake.txt");
	SetPathEnemy("data\\system\\enemy\\kuma.txt");
	SetPathEnemy("data\\system\\enemy\\Kumakai.txt");
	SetPathEnemy("data\\system\\enemy\\neko.txt");
	SetPathEnemy("data\\system\\enemy\\Raccoon.txt");
	SetPathEnemy("data/SYSTEM/Gon/Fox.txt");

	return S_OK;
}


//------------------------------------
//オブジェクトの生成
//------------------------------------
void CStage::SetObject()
{

	
}

//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::Loadfile(const char * pFileName)
{
	std::ifstream ifs(pFileName);

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JBuilding;
		nIndex = JBuilding["INDEX"];
		m_numAllBuilding = JBuilding["INDEX"];
		D3DXVECTOR3 pos;
		D3DXVECTOR3 size;
		D3DXVECTOR3 rot;
		std::string Type;
		bool gold;
		bool Check;
		int ModelType = 0;
		
		for (int nCntBuilding = 0; nCntBuilding < nIndex; nCntBuilding++)
		{
			CBuilding* Buil = nullptr;
			std::string name = "BUILDING";
			std::string Number = std::to_string(nCntBuilding);
			name += Number;

			if (JBuilding.count(name) == 0)
			{
				//int a = 0;
			}
			pos = D3DXVECTOR3(JBuilding[name]["POS"]["X"], JBuilding[name]["POS"]["Y"], JBuilding[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JBuilding[name]["ROT"]["X"], JBuilding[name]["ROT"]["Y"], JBuilding[name]["ROT"]["Z"]);
			Type = JBuilding[name]["TYPE"];
			ModelType = JBuilding[name]["MODELTYPE"];
			gold = JBuilding[name]["GOLD"];
			Check = JBuilding[name]["CHECK"];
			switch ((CStage::MODELTYPE)ModelType)
			{
			case CStage::NORMAL:
			{
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetGold(gold);
				break;
			}
			case CStage::BALL:
			{
				float radius;
				float Rotspeed;
				int Rottype;
				D3DXVECTOR3 centerpos;

				name += "BALL";
				centerpos = D3DXVECTOR3(JBuilding[name]["CENTERPOS"]["X"], JBuilding[name]["CENTERPOS"]["Y"], JBuilding[name]["CENTERPOS"]["Z"]);
				radius = JBuilding[name]["RADIUS"];
				Rotspeed = JBuilding[name]["ROTSPEED"];
				Rottype = JBuilding[name]["ROTTYPE"];

				Buil = CBall::Create(centerpos, radius, Rotspeed, Rottype);

				break;
			}
			case CStage::PENDULUM:
			{
				D3DXVECTOR3 Ballrot;
				float Coefficient;
				int Movetype;

				std::string BallType;


				name += "PENDULUM";

				Ballrot = D3DXVECTOR3(JBuilding[name]["DESTROT"]["X"], JBuilding[name]["DESTROT"]["Y"], JBuilding[name]["DESTROT"]["Z"]);
				Coefficient = JBuilding[name]["COEFFICIENT"];
				Movetype = JBuilding[name]["MOVETYPE"];
				Buil = CPendulum::Create(pos, Ballrot, Coefficient, Movetype);

				break;
			}
			case CStage::WOOD:
			{
				D3DXVECTOR3 Woodpos;
				float Poprot;
				int Poptime;
				float PoprotSpeed;
				float PopMove;

				std::string WoodType;

				name += "WOOD";

				Woodpos = D3DXVECTOR3(JBuilding[name]["POPPOS"]["X"], JBuilding[name]["POPPOS"]["Y"], JBuilding[name]["POPPOS"]["Z"]);
				Poprot = JBuilding[name]["POPROT"];
				Poptime = JBuilding[name]["POPTIME"];
				PoprotSpeed = JBuilding[name]["ROTSPEED"];
				PopMove = JBuilding[name]["MOVE"];


				Buil = CWood_Spawn::Create(Woodpos, Poprot, PopMove, PoprotSpeed, Poptime);
				break;
			}
			case CStage::RAIL:
				Buil = CRail::Create(pos, rot);
				break;
			case CStage::CHANGE:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetChangePoptime(Check);
				break;
			case CStage::GOAL:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetGold(gold);
				break;
			case CStage::DASHITEM:
				Buil = CDashitem::Create(pos, rot);
				break;
			case CStage::BANE:
				Buil = CSprint::Create(pos, rot);
				break;
			case CStage::BELL:
				Buil = CBell::Create(pos, rot);
				break;
			case CStage::CAMERA:
			{
				name += "CAMERA";
				std::string PopPass = JBuilding[name]["PLAYERPASS"];
				Buil = CCamera_Player::Create(pos, rot, PopPass);
				
			}
				break;
			default:
				break;
			}

			Buil->SetUp(BUILDING);
			Buil->SetModelType((CStage::MODELTYPE)ModelType);
		
		}
	}
}

//------------------------------------
//今あるオブジェクトの保存
//------------------------------------
void CStage::Savefile(const char * pFileName)
{
	int nIndex = 0;
	for (int Isobject = 0; Isobject < PRIORITY_MAX; Isobject++)
	{	
		CObject* obj = GetTop(Isobject);

		if (obj != nullptr)
		{
			while (obj)
			{
				if (obj->GetType() == BUILDING)
				{
					CBuilding* objBui = dynamic_cast<CBuilding*>(obj);  // ダウンキャスト

					if (objBui->GetFileName().c_str() == nullptr)
					{
						objBui->SetFileName("Null");
					}

					std::string name = "BUILDING";
					std::string Number = std::to_string(nIndex);
					name += Number;

					JBuilding[name] = {
						{ "POS",{
							{ "X", objBui->GetPos().x } ,
							{ "Y", objBui->GetPos().y } ,
							{ "Z", objBui->GetPos().z } } },
						{ "ROT",{
							{ "X", objBui->GetRot().x } ,
							{ "Y", objBui->GetRot().y } ,
							{ "Z", objBui->GetRot().z } } } ,
						{ "MOVE",{
							{"X", objBui->GetMove().x } ,
							{"Y", objBui->GetMove().y } ,
							{"Z", objBui->GetMove().z } } } ,
						{"TYPE", objBui->GetFileName().c_str() } ,
						{"MODELTYPE",objBui->GetModelType() },
						{"GOLD",objBui->GetGold() },
						{"CHECK",objBui->GetChangePoptime()}, };

					switch (objBui->GetModelType())
					{
					case CStage::NORMAL:
						break;
					case CStage::PENDULUM:
					{//別の情報保存
						CPendulum* objBall = dynamic_cast<CPendulum*>(obj);  // ダウンキャスト
						name += "PENDULUM";
						JBuilding[name] = { { "DESTROT",{
							{ "X", objBall->GetDestRot().x } ,
							{ "Y", objBall->GetDestRot().y } ,
							{ "Z", objBall->GetDestRot().z } } },
							{ "COEFFICIENT", objBall->GetCoefficient() } ,
							{ "MOVETYPE", objBall->GetType() } };

						break;
					}
					case CStage::BALL:
					{//別の情報保存
						CBall* objBallModel = dynamic_cast<CBall*>(obj);  // ダウンキャスト
						name += "BALL";
						JBuilding[name] = { { "CENTERPOS",{
							{ "X", objBallModel->GetCenterPos().x } ,
							{ "Y", objBallModel->GetCenterPos().y } ,
							{ "Z", objBallModel->GetCenterPos().z }} },
							{ "RADIUS", objBallModel->GetRadius() } ,
							{ "ROTSPEED", objBallModel->GetRotSpeed() } ,
							{ "ROTTYPE", objBallModel->GetRotType() } };

						break;
					}
					case CStage::WOOD:
					{//別の情報保存
						CWood_Spawn* objWood = dynamic_cast<CWood_Spawn*>(obj);  // ダウンキャスト
						name += "WOOD";
						JBuilding[name] = { { "POPPOS",{
							{ "X", objWood->GetSpawnPos().x } ,
							{ "Y", objWood->GetSpawnPos().y } ,
							{ "Z", objWood->GetSpawnPos().z } } },
							{ "POPROT", objWood->GetRot() } ,
							{ "POPTIME", objWood->GetCoolTime() } ,
							{ "ROTSPEED", objWood->GetRotSpeed() } ,
							{ "MOVE", objWood->GetMoveSpeed() } };

						break;
					}
					case CStage::RAIL:
						break;
					case CStage::CHANGE:
						break;
					case CStage::GOAL:
						break;
					case CStage::DASHITEM:
						break;
					case CStage::BANE:
						break;
					case CStage::BELL:
						break;
					case CStage::CAMERA:
						
					{
						CCamera_Player* Camera_Player = dynamic_cast<CCamera_Player*>(obj);  // ダウンキャスト
						name += "CAMERA";
						JBuilding[name] = {
							{ "PLAYERPASS", Camera_Player->GetPass().c_str() } };

						break;
					}
					default:
					 break;

					}

					nIndex++;

					if (obj->GetNext() == nullptr)
					{
						obj = nullptr;
						break;
					}
					else
					{
						CObject *objnext = obj->GetNext();

						obj = objnext;
					}

				}
				else
				{
					if (obj->GetNext() == nullptr)
					{
						break;
					}
					else
					{
						CObject *objnext = obj->GetNext();
						obj = objnext;
					}
				}
			}

		}
	}
	JBuilding["INDEX"] = nIndex;

	auto jobj = JBuilding.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = pFileName;
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();
}

//------------------------------------
//今あるオブジェクトの保存
//------------------------------------
void CStage::SavefileMesh(const char * pFileName)
{
	int nIndex = 0;
	//string filepass = "";
	std::string filepass = ("data\\MESH\\");

	std::string createfilepass = CGame::GetStageImgui()->Getfile();

	JlistMesh["PASS"] = createfilepass;

	filepass += (createfilepass + ("\\"));

	//フォルダ生成
	if (CreateDirectoryA(createfilepass.c_str(), NULL))
	{
		printf("フォルダ作成に成功しました。\n");
	}
	else 
	{
		printf("フォルダ作成に失敗しました。\n");
	}


	SetCurrentDirectory(CManager::GetdefaultPath());
	for (int meshCount= 0; meshCount < m_numAllMesh; meshCount++)
	{
		CObject* Mesh = CObject::SelectModelObject(meshCount, CObject::MESH);
		if (Mesh != nullptr)
		{

			CMesh* CastMesh = dynamic_cast<CMesh*>(Mesh);  // ダウンキャスト
				
				std::string name = "Mesh";
				std::string Number = std::to_string(nIndex);
				name += Number;
				std::string Pass = (filepass + (name + ".json"));
				JlistMesh[name] = {
				{ "FILEPASS", Pass.c_str() } ,
				{ "TYPE", CastMesh->GetMeshType() } };

				nIndex++;
	
				CastMesh->Savefile(Pass.c_str());
		}
	

	}
	JlistMesh["INDEX"] = nIndex;
	auto jobj = JlistMesh.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = pFileName;
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();
}


//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::LoadfileMesh(const char * pFileName)
{

	std::ifstream ifs(pFileName);

	if (ifs)
	{
		ifs >> JlistMesh;

		int nIndex = 0;

		std::string defaultfilePass;
		nIndex = JlistMesh["INDEX"];

		m_numAllMesh = nIndex;
		int Type;

		for (int meshCount = 0; meshCount < nIndex; meshCount++)
		{
			CMesh*mesh = nullptr;
			std::string name = "Mesh";
			std::string Number = std::to_string(meshCount);
			name += Number;
			std::string filepass;
			defaultfilePass;
			filepass = JlistMesh[name]["FILEPASS"];
			Type = JlistMesh[name]["TYPE"];

			switch ((MESHTYPE)Type)
			{
			case MESH:
				mesh = CMesh::Create();
				break;
			case MESHMOVE:
				mesh = CMoveMesh::Create();
				break;
			case FLOORINGMOVE:
				mesh = CMeshRoom::Create();
				break;
			case RUNMESH:
				mesh = CRunMesh::Create();
				break;
			default:
				mesh = CMesh::Create();
				break;
			}

			mesh->Loadfile(filepass.c_str());
			mesh->SetType(Type);
			mesh->SetNumber(meshCount);
		}
	}
}
//------------------------------------
// 終了
//------------------------------------
void CStage::Uninit()
{
	CObject::Release();
}

//------------------------------------
// 更新
//------------------------------------
void CStage::Update()
{
	CInput *CInputpInput = CInput::GetKey();
	if (CInputpInput->Trigger(CInput::KEY_DELETE)&& CInputpInput->Press(CInput::KEY_SHIFT))
	{
		if (CObject::SelectModelObject(CGame::GetStageImgui()->selectModel(), CObject::BUILDING) != nullptr)
		{		
			if (m_numAllBuilding >= 0)
			{
				CObject::SelectModelObject(CGame::GetStageImgui()->selectModel(), CObject::BUILDING)->Uninit();
				m_numAllBuilding--;
			}
		}

	}
}


//------------------------------------
// create
//------------------------------------
CStage *CStage::Create()
{
	CStage * pObject = new CStage;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}

//------------------------------------
// 当たってるメッシュをとる関数
//------------------------------------
CMesh*CStage::GetPlayerHitMesh(int IsNumber)
{
	CObject* Mesh = CObject::SelectModelObject(IsNumber, CObject::MESH);
	if (Mesh != nullptr)
	{

		CMesh* CastMesh = dynamic_cast<CMesh*>(Mesh);  // ダウンキャスト
		//m_posOrigin;
	
		D3DXVECTOR3 Size(CastMesh->GetMeshSize());
		D3DXVECTOR3 oneboxSize(CastMesh->GetOneMeshSize());
		if (((CastMesh->GetPos()->z - Size.z) <= (m_Player->GetPos().z)) &&
			((CastMesh->GetPos()->z) + oneboxSize.z >= (m_Player->GetPos().z)) &&
			((CastMesh->GetPos()->x - oneboxSize.x) <= (m_Player->GetPos().x)) &&
			((CastMesh->GetPos()->x + Size.x) >= (m_Player->GetPos().x)))
		{
			return	CastMesh;
		}
		
	}
	return	nullptr;
}

//------------------------------------
// Meshの制作
//------------------------------------
void CStage::CreateMesh(D3DXVECTOR3 IsPos)
{
	CMesh* CastMesh = nullptr;

	switch ((MESHTYPE)CGame::GetStageImgui()->GetMesh())
	{
	case MESH:
		CastMesh = CMesh::Create();
		break;
	case MESHMOVE:
		CastMesh = CMoveMesh::Create();
		break;
	case FLOORINGMOVE:
		CastMesh = CMeshRoom::Create();
		break;
	case RUNMESH:
		CastMesh = CRunMesh::Create();
		break;
	default:
		CastMesh = CMesh::Create();
		break;
	}
	CastMesh->SetNumber(m_numAllMesh);
	CastMesh->SetPos(IsPos);
	CastMesh->SetMesh(10);
	CastMesh->SetType(CGame::GetStageImgui()->GetMesh());
	m_numAllMesh++;



}

//------------------------------------
// モデルの制作
//------------------------------------
void CStage::CreateModel(D3DXVECTOR3 IsPos)
{
	CBuilding *Building;

	CStageImgui* imgui = CGame::GetStageImgui();
	switch ((CStage::MODELTYPE)imgui->GetModel())
	{
	case CStage::NORMAL:
		Building = CBuilding::Create(CGame::GetStage()->GetPopItem(), &IsPos);
		break;
	case CStage::PENDULUM:
		Building = CPendulum::Create(IsPos, imgui->GetPendulum().destRot, imgui->GetPendulum().coefficient, imgui->GetPendulum().type);
		break;
	case CStage::WOOD:
		Building = CWood_Spawn::Create(IsPos, imgui->GetWood().rot, imgui->GetWood().move, imgui->GetWood().rotSpeed, imgui->GetWood().coolTime);
		break;
	case CStage::BALL:
		Building = CBall::Create(IsPos, imgui->GetBall().radius, imgui->GetBall().rotSpeed, imgui->GetBall().rotType);
		break;
	case CStage::RAIL:
		Building = CRail::Create(IsPos, imgui->GetRot());
		break;
	case CStage::CHANGE:
		Building = CBuilding::Create(CGame::GetStage()->GetPopItem(), &IsPos);
		Building->SetChangePoptime(true);
		break;
	case CStage::GOAL:
		Building = CBuilding::Create(CGame::GetStage()->GetPopItem(), &IsPos);
		Building->SetGold(true);
		break;
	case CStage::DASHITEM:
		Building = CDashitem::Create(IsPos, imgui->GetRot());
		break;
	case CStage::BANE:
		Building = CSprint::Create(IsPos, imgui->GetRot());
		break;
	case CStage::CAMERA:
		Building = CCamera_Player::Create(IsPos, imgui->GetRot(), imgui->GetPass());
		break;
	default:
		Building = CBuilding::Create(CGame::GetStage()->GetPopItem(), &IsPos);
		break;
	}
	Building->SetUp(BUILDING);
	Building->SetModelType((CStage::MODELTYPE)imgui->GetModel());
	m_numAllBuilding++;
}


//------------------------------------
// モデルの制作
//------------------------------------
void CStage::CreateEnemy(D3DXVECTOR3 IsPos)
{
	CStageImgui* imgui = CGame::GetStageImgui();
	CEnemy*Enemy;
	int Data = imgui->GetEnemy();
	switch ((CStage::MODELTYPE)Data)
	{
	case CStage::ENEMY:
		Enemy = CEnemy::Create(IsPos);
		break;
	case CStage::CHASEENEMY:
		Enemy = CChaseEnemy::Create(IsPos);
		break;
	case CStage::BOSS:
		Enemy = CBoss::Create(IsPos);
		break;
	default:
		Enemy = CEnemy::Create(IsPos);
		break;
	}
	Enemy->SetPass(CGame::GetStage()->GetPathEnemy(imgui->GetModelEnemy(), true).c_str());
	Enemy->SetUp(CObject::ENEMY);
	m_numAllEnemy++;
}

//--------------------------------------------------
// パスの設定
//--------------------------------------------------
void CStage::SetPath(std::string str)	
{
	if (str == "")
	{
		return;
	}
	for (int i = 0; i < m_numAll; i++)
	{
		if (m_fileName[i] == str)
		{
			return;
		}

	}
	m_fileName[m_numAll] = str;
	m_numAll++;
}

//--------------------------------------------------
// パスの取得
//--------------------------------------------------
std::string CStage::GetPath(int index, bool path)
{
	
	if (path)
	{// パスあり
		return m_fileName[index];
	}

	std::string str;

	for (int i = 0; i < index; i++)
	{
		str += m_fileName[i];
	}

	return str;
}

//--------------------------------------------------
// 総数の取得
//--------------------------------------------------
int CStage::GetNumAll()
{
	return m_numAll;
}

//--------------------------------------------------
// パスの設定
//--------------------------------------------------
void CStage::SetPathTex(std::string str)
{
	for (int i = 0; i < m_numAllTex; i++)
	{
		if (m_fileNameTex[i] == str)
		{
			return;
		}
	}
	m_fileNameTex[m_numAllTex] = str;
	m_numAllTex++;
}

//--------------------------------------------------
// パスの取得
//--------------------------------------------------
std::string CStage::GetPathTex(int index, bool path)
{

	if (path)
	{// パスあり
		return m_fileNameTex[index];
	}

	std::string str;

	for (int i = 0; i < index; i++)
	{
		str += m_fileNameTex[i];
	}

	return str;
}


//--------------------------------------------------
// パスの設定
//--------------------------------------------------
void CStage::SetPathEnemy(std::string str)
{
	for (int i = 0; i < m_numAllEnemyPass; i++)
	{
		if (m_fileNameEnemy[i] == str)
		{
			return;
		}
	}
	m_fileNameEnemy[m_numAllEnemyPass] = str;
	m_numAllEnemyPass++;
}

//--------------------------------------------------
// パスの取得
//--------------------------------------------------
std::string CStage::GetPathEnemy(int index, bool path)
{

	if (path)
	{// パスあり
		return m_fileNameEnemy[index];
	}

	std::string str;

	for (int i = 0; i < index; i++)
	{
		str += m_fileNameEnemy[i];
	}

	return str;
}

//--------------------------------------------------
// GetMesh
//--------------------------------------------------
CMesh * CStage::GetMesh(int IsNumber)
{
	CObject* Mesh = CObject::SelectModelObject(IsNumber, CObject::MESH);
	CMesh* CastMesh = dynamic_cast<CMesh*>(Mesh);  // ダウンキャスト

	return CastMesh;
}

//--------------------------------------------------
// 総数の取得
//--------------------------------------------------
int CStage::GetNumAllTex()				
{
	return m_numAllTex;
}


//------------------------------------
//今あるEnemyの保存
//------------------------------------
void CStage::SavefileEnemy(const char * pFileName)
{
	int nIndex = 0;
	for (int EnemyCount = 0; EnemyCount < m_numAllEnemy; EnemyCount++)
	{
		
		CObject*Object = CObject::SelectModelObject(EnemyCount, CObject::ENEMY);

		CEnemy* Enemy = dynamic_cast<CEnemy*>(Object);  // ダウンキャスト
		if (Enemy != nullptr)
		{
			std::string name = "ENEMY";
			std::string Number = std::to_string(nIndex);
			name += Number;
			JlistEnemy[name] = {
				{ "POS",{
					{ "X", Enemy->GetPopPos().x } ,
					{ "Y", Enemy->GetPopPos().y } ,
					{ "Z", Enemy->GetPopPos().z } } },
				{ "ROT",{
					{ "X", Enemy->GetPopRot().x } ,
					{ "Y", Enemy->GetPopRot().y } ,
					{ "Z", Enemy->GetPopRot().z } } } ,
				{ "MIN",{
					{ "X", Enemy->GetMin().x } ,
					{ "Y", Enemy->GetMin().y } ,
					{ "Z", Enemy->GetMin().z } } } ,
				{ "MAX",{
					{ "X", Enemy->GetMax().x } ,
					{ "Y", Enemy->GetMax().y } ,
					{ "Z", Enemy->GetMax().z } } } ,
				{ "TYPE", Enemy->GetEnemyType() } ,
				{ "PASS", Enemy->GetPass() } , };
			
			nIndex++;
			switch (Enemy->GetEnemyType())
			{
			case CStage::BOSS:
			{
				CBoss* BossEnemy = dynamic_cast<CBoss*>(Enemy);  // ダウンキャスト
				BossEnemy->BossEventSeve();
			}
			default:
		
				break;
			}
		}
	}
		JlistEnemy["INDEX"] = nIndex;


		auto jobj = JlistEnemy.dump();
		std::ofstream writing_file;
		const std::string pathToJSON = pFileName;
		writing_file.open(pathToJSON, std::ios::out);
		writing_file << jobj << std::endl;
		writing_file.close();
	
}

//------------------------------------
//今あるEnemyのロード
//------------------------------------
void CStage::LoadfileEnemy(const char * pFileName)
{
	std::ifstream ifs(pFileName);

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JlistEnemy;
		nIndex = JlistEnemy["INDEX"];
		m_numAllEnemy = JlistEnemy["INDEX"];
		D3DXVECTOR3 pos;
		D3DXVECTOR3 Min;
		D3DXVECTOR3 Max;
		D3DXVECTOR3 rot;
		std::string Type;
		int ModelType = 0;

		for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
		{
			CEnemy* Enemy = nullptr;
			std::string name = "ENEMY";
			std::string Number = std::to_string(nCntEnemy);
			name += Number;

			pos = D3DXVECTOR3(JlistEnemy[name]["POS"]["X"], JlistEnemy[name]["POS"]["Y"], JlistEnemy[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JlistEnemy[name]["ROT"]["X"], JlistEnemy[name]["ROT"]["Y"], JlistEnemy[name]["ROT"]["Z"]);
			Min = D3DXVECTOR3(JlistEnemy[name]["MIN"]["X"], JlistEnemy[name]["MIN"]["Y"], JlistEnemy[name]["MIN"]["Z"]);
			Max = D3DXVECTOR3(JlistEnemy[name]["MAX"]["X"], JlistEnemy[name]["MAX"]["Y"], JlistEnemy[name]["MAX"]["Z"]);

			Type = JlistEnemy[name]["PASS"];
			ModelType = JlistEnemy[name]["TYPE"];

			switch ((CStage::MODELTYPE)ModelType)
			{
			case CStage::ENEMY:
				Enemy = CEnemy::Create(pos);
				break;
			case CStage::CHASEENEMY:
				Enemy = CChaseEnemy::Create(pos);
				break;
			case CStage::BOSS:
			{
				Enemy = CBoss::Create(pos);
				CBoss* BossEnemy = dynamic_cast<CBoss*>(Enemy);  // ダウンキャスト
				BossEnemy->BossEventLood();
				break;
			}
			default:
				Enemy = CEnemy::Create(pos);
				break;
			}
			Enemy->SetMin(Min);
			Enemy->SetMax(Max);
			Enemy->SetRot(rot);
			Enemy->SetPass(Type);
			Enemy->SetUp(CObject::ENEMY);
		}
	}
}


