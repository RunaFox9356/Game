//=============================================================================
//
// 敵設定
// Author:hamada ryuuga
//
//=============================================================================

#include "enemy.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "stage_imgui.h"


//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemy::CEnemy()
{
	m_min = {-50.0f,-50.0f ,-50.0f };
	m_max = { 50.0f,50.0f ,50.0f };
	m_PopPos = { 0.0f,0.0f ,0.0f };
	SetPopRot({ 0.0f,0.0f ,0.0f });
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemy::~CEnemy()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemy::Init()
{
	for (int i = 0; i < 12; i++)
	{
		if (Line[i] != nullptr)
		{
			Line[i] = nullptr;
		}
	}
	// 現在のモーション番号の保管
	CMotionModel3D::Init();
	SetUp(CObject::ENEMY);
	m_hit = false;
	m_isMove = false;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEnemy::Uninit()
{
	CMotionModel3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemy::Update()
{
	CMotionModel3D::Update();


	
	// 動作
	Move();

	CPlayer*Player = CGame::GetStage()->GetPlayer();
	D3DXVECTOR3  pPos = Player->GetPos();
	D3DXVECTOR3  pPosOld = Player->GetPosOld();
	D3DXVECTOR3  pSize = Player->GetSize();
	CollisionModel(&pPos,&pPosOld, &pSize);

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CEnemy::Draw()
{
	CMotionModel3D::Draw();
	CEnemy::Setline();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CEnemy *CEnemy::Create(D3DXVECTOR3 pos)
{
	CEnemy *pEnemy = nullptr;

	pEnemy = new CEnemy;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::ENEMY);
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CEnemy::Move()
{
	
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CEnemy::Hit()
{
	CStageImgui* imgui = CGame::GetStageImgui();
	imgui->ImguiEnemy();
}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CEnemy::NotHit()
{
	m_hit = false;
}

//--------------------------------------------------
// Passの設定
//--------------------------------------------------
void CEnemy::SetPass(std::string pass)
{
	SetMotion(pass.c_str());
	Myfilepass = pass;
}

//--------------------------------------------------
// 当たり判定の設定
//--------------------------------------------------
bool CEnemy::CollisionModel(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize)
{
	bool bIsLanding = false;

	D3DXMATRIX mtxWorld = *CMotionModel3D::GetMtxWorld();

	D3DXVECTOR3 min = GetMin();
	D3DXVECTOR3 max = GetMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			Hit();
			bIsLanding = true;
		}
	}
	else
	{
		NotHit();
	}

	return bIsLanding;
}


//--------------------------------------------------
// 当たり判定の設定
//--------------------------------------------------
void CEnemy::Setline()
{
	D3DXCOLOR col(1.0f, 1.0f, 1.0f, 1.0f);

	for (int i = 0; i < 12; i++)
	{
		if (Line[i] == nullptr)
		{
			Line[i] = CLine::Create();
		}
	}
	//D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	Line[0]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMin().y, GetMax().z), D3DXVECTOR3(GetMax().x, GetMin().y, GetMax().z), col);
	Line[1]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMin().y, GetMin().z), D3DXVECTOR3(GetMin().x, GetMin().y, GetMax().z), col);
	Line[2]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMin().y, GetMin().z), D3DXVECTOR3(GetMax().x, GetMin().y, GetMin().z), col);
	Line[3]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMax().x, GetMin().y, GetMin().z), D3DXVECTOR3(GetMax().x, GetMin().y, GetMax().z), col);
	Line[4]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMax().y, GetMax().z), D3DXVECTOR3(GetMax().x, GetMax().y, GetMax().z), col);
	Line[5]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMax().y, GetMin().z), D3DXVECTOR3(GetMin().x, GetMax().y, GetMax().z), col);
	Line[6]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMax().y, GetMin().z), D3DXVECTOR3(GetMax().x, GetMax().y, GetMin().z), col);
	Line[7]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMax().x, GetMax().y, GetMin().z), D3DXVECTOR3(GetMax().x, GetMax().y, GetMax().z), col);
	Line[8]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMin().y, GetMax().z), D3DXVECTOR3(GetMin().x, GetMax().y, GetMax().z), col);
	Line[9]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMin().x, GetMin().y, GetMin().z), D3DXVECTOR3(GetMin().x, GetMax().y, GetMin().z), col);
	Line[10]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMax().x, GetMin().y, GetMin().z), D3DXVECTOR3(GetMax().x, GetMax().y, GetMin().z), col);
	Line[11]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetMax().x, GetMin().y, GetMax().z), D3DXVECTOR3(GetMax().x, GetMax().y, GetMax().z), col);
}
