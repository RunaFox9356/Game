//============================
//
// 動くmesh設定
// Author:hamada ryuuga
//
//============================

#include "movemesh.h"
#include "utility.h"
#include "stage.h"
#include "player.h"
#include "game.h"
#include "object.h"
#include "utility.h"
#include "stage_imgui.h"
#include "spline.h"
#include "linemesh.h"
//------------------------------------
// コンストラクタ
//------------------------------------
CMoveMesh::CMoveMesh()
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CMoveMesh::~CMoveMesh()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CMoveMesh::Init()
{
	CMesh::Init();

	m_NowAnimation = 0;
	m_AnimationSpeed = 0;
	m_SpeedCount = 0;
	LineMesh = CLineMesh::Create();
	m_Testrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	return S_OK;
}

//------------------------------------
// 終了
//------------------------------------
void CMoveMesh::Uninit()
{
	CMesh::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CMoveMesh::Update()
{
	CMesh::Update();
	//動き
	CMoveMesh::move();
}

//------------------------------------
// 描画
//------------------------------------
void CMoveMesh::Draw()
{
	CMesh::Draw();

	//double*x = new double[GetPoptimeSize()];
	//double*y = new double[GetPoptimeSize()];
	//double*z = new double[GetPoptimeSize()];

	//if (m_Poptime.size() != 0)
	//{
	//	for (int i = 0; i < GetPoptimeSize(); i++)
	//	{
	//		x[i] = GetPoptime(i).x;
	//		y[i] = GetPoptime(i).y;
	//		z[i] = GetPoptime(i).z;
	//	}
	//	drowSpline(x, y, z, GetPoptimeSize(), LineMesh);
	//}
	//
	//delete[] x;
	//delete[] y;
	//delete[] z;
}	

//------------------------------------
// create
//------------------------------------
CMoveMesh *CMoveMesh::Create()
{
	CMoveMesh * pObject = new CMoveMesh;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}

//------------------------------------
// Animationポイント加算
//------------------------------------
void CMoveMesh::AddPoptime(const D3DXVECTOR3 IsPos)
{
	m_Poptime.push_back(IsPos);
	m_Reverse.push_back(false);
	LineMesh->AddPoptime(IsPos);
	LineMesh->AddReverse(false);
}

//------------------------------------
// AnimationポイントSet
//------------------------------------
void CMoveMesh::SetPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos, const bool IsReverse)
{
	m_Poptime.at(IsPoptime) = IsPos;
	LineMesh->SetPoptime(IsPoptime, IsPos);
	m_Reverse.at(IsPoptime) = IsReverse;
	LineMesh->SetReverse(IsPoptime, IsReverse);
}


//------------------------------------
// 動き系統
//------------------------------------
void CMoveMesh::move()
{

}

//------------------------------------
// Playerが当たった時の判定
//------------------------------------
void CMoveMesh::OnHit()
{

	CStageImgui* imgui = CGame::GetStageImgui();

	if (m_Poptime.size() != 0 && imgui->GetAnimati())
	{
		D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
		D3DXVECTOR3 centerPosOld = CGame::GetStage()->GetPlayer()->GetPosOld();
		for (int ListVec = 0; ListVec < GetPoptimeSize() - 1; ListVec++)
		{
			
			if (CMoveMesh::CollisionWall(&centerPos, &centerPosOld, ListVec))
			{
				m_NowAnimation = ListVec+1;
				m_FastHit = true;
			}
		}
		SwitchCollision(false);

		if (m_FastHit)
		{
			Homing(&centerPos, centerPos, GetPoptime(m_NowAnimation), (float)m_AnimationSpeed);

			CGame::GetStage()->GetPlayer()->SetPos(centerPos);

			if (centerPos == GetPoptime(m_NowAnimation))
			{
				m_NowAnimation++;
				if (m_NowAnimation >= (int)m_Poptime.size())
				{
					m_NowAnimation = m_Poptime.size() - 1;
				}
			}
		}
	}
	else
	{
		m_FastHit = false;
		m_SpeedCount = 0;
		m_NowAnimation = 0;
	}

}

//----------------------------
//Wall動き
//----------------------------
bool CMoveMesh::CollisionWall(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, int ListVec)
{
	bool bIsLanding = false;

	//V1 move
	D3DXVECTOR3 vecMove = *pPos - *pPosOld;
	D3DXVECTOR3 vecWall;
	D3DXVECTOR3 vecPos;


	//ベクトルS2 V2
	vecWall = GetPoptime(ListVec + 1) - GetPoptime(ListVec) + (vecMove);


	//ベクトル現在のPOSと始点までの距離
	vecPos = *pPos - D3DXVECTOR3(GetPoptime(ListVec));

	//外積計算
	//float vecLine = Vec2Cross(&vecPos, &vecWall);
	D3DXVECTOR3 vecOld;

	//ベクトルV
	vecOld = GetPoptime(ListVec) - *pPosOld;

	//計算用の箱
	float vecCalculation[2];

	//T1求めるやつ
	vecCalculation[0] = Vec2Cross(&vecOld, &vecWall);
	vecCalculation[1] = Vec2Cross(&vecMove, &vecWall);

	//posOldから交点までの距離
	float t1 = vecCalculation[0] / vecCalculation[1];

	//T2求めるやつ
	vecCalculation[0] = Vec2Cross(&vecOld, &vecMove);
	vecCalculation[1] = Vec2Cross(&vecMove, &vecWall);

	//壁の視点から交点までの距離
	float t2 = vecCalculation[0] / vecCalculation[1];

	//差
	float eps = 0.00001f;

	if (t1 + eps < 0 || t1 - eps > 1 || t2 + eps < 0 || t2 - eps > 1)
	{//交差してないときの判

	}
	else
	{//交差してるとき

		bIsLanding = true;
	}

	return bIsLanding;
}



