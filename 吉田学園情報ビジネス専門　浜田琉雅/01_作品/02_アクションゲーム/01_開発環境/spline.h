
//**************************************************
//
// spline.h
// Author  : hamada hamada
//
//**************************************************
#ifndef _SPLINE_H_	//このマクロ定義がされてなかったら
#define _SPLINE_H_	//２重インクルード防止のマクロ定義

#include "manager.h"
#include "line.h"


class CLineMesh;

#define MaxSplineSize 100

void drowSpline(double *x, double *y, double *z, int num, CLineMesh*m_lineMesh);
void SetSpline(int size, std::vector <D3DXVECTOR3> &posPos, std::vector <D3DXVECTOR3> &OutPos);
class Spline
{
	
	int m_num;
	double a[MaxSplineSize + 1], b[MaxSplineSize + 1], c[MaxSplineSize + 1], d[MaxSplineSize + 1];
public:
	Spline() { m_num = 0; }
	void init(double *sp, int num);
	double culc(double t);
	int GetNum(void) { return m_num; };

};





#endif // !_S_H_