//=============================================================================
//
// BGオブジェクト
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _SHADOW_H_			// このマクロ定義がされてなかったら
#define _SHADOW_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "3dpolygon.h"
#include "texture.h"

class CShadow : public C3dpolygon
{
public:


	static CShadow *CShadow::Create(D3DXVECTOR3 Pos);

	CShadow(const int list);
	~CShadow() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	const D3DXVECTOR3 *GetPos() const override;
	void SetPos(const D3DXVECTOR3 &pos) override;
	void SetQuat(D3DXQUATERNION quat) { m_quat = quat; }

private:
	
	void move();
	D3DXVECTOR3 m_move;
	D3DXMATRIX m_mtxRot;
	D3DXQUATERNION m_quat;
};

#endif

