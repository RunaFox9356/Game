//=============================================================================
//
// 敵設定
// Author:hamada ryuuga
//
//=============================================================================

#include "enemy.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "score.h"
#include "manager.h"
#include "camera.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemy::CEnemy()
{
	m_min = {-50.0f,-50.0f ,-50.0f };
	m_max = { 50.0f,50.0f ,50.0f };
	m_popPos = { 0.0f,0.0f ,0.0f };
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemy::~CEnemy()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemy::Init()
{

	// 現在のモーション番号の保管
	CMotionModel3D::Init();
	SetUp(CObject::ENEMY);
	m_hit = false;
	m_isMove = false;
	m_get = false;
	m_throw = false;
	m_nouHave = 0;

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEnemy::Uninit()
{
	CMotionModel3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemy::Update()
{
	CMotionModel3D::Update();

	if (m_get)
	{
		// 動作
		Move();

	}

	
	if (*CManager::GetInstance()->GetMode() == CManager::MODE_GAME || *CManager::GetInstance()->GetMode() == CManager::MODE_BOSS)
	{
		CPlayer*Player = CGame::GetStage()->GetPlayer();
		D3DXVECTOR3  pPos = Player->GetPos();
		D3DXVECTOR3  pPosOld = Player->GetPosOld();
		D3DXVECTOR3  pSize = Player->GetSize();


		CollisionModel(&pPos, &pPosOld, &pSize, Player);
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CEnemy::Draw()
{

	CMotionModel3D::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CEnemy *CEnemy::Create(D3DXVECTOR3 pos)
{
	CEnemy *pEnemy = nullptr;

	pEnemy = new CEnemy;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::ENEMY);
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CEnemy::Move()
{
	if (m_throw)
	{//投げられた時
		D3DXVECTOR3 *posOld = &GetPos();
		D3DXVECTOR3 *pos = &GetPos();
		*pos += m_move;
		SetPos(*pos);
		SearchModelObject(0, ENEMY, [this, pos, posOld](CObject*Enemy)
		{
			if (this != Enemy)
			{
				CEnemy* pEnemy = dynamic_cast<CEnemy*>(Enemy);
				D3DXVECTOR3  Size = pEnemy->GetMax() - pEnemy->GetMin();
				pEnemy->CollisionModel(pos, posOld, &Size,this);
			}
		});
	}
	else
	{//つかまってる時
		D3DXVECTOR3 pos = GetPos();
		D3DXVECTOR3 rot = GetRot();

		//modelとプレイヤーの当たり判定
		CPlayer*Player = CGame::GetStage()->GetPlayer();

		pos = Player->GetposLog(9 * m_nouHave);

		rot = Player->GetRot();

		NormalizeAngle(&rot.y);
		SetPos(pos);
		SetRot(rot);
	}
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CEnemy::Hit(CObject *object)
{
	CPlayer*Player = CGame::GetStage()->GetPlayer();

	if (Player->GetHave() && (Player->GetNouEnemy() <= MAXHAVE - 1) && !m_get)
	{//捕まえたとき
		m_get = true;
		Player->SetHaveData(true);
		Player->SetHaveDefEnemy(this);
		m_nouHave = Player->GetNouEnemy();

	}
	if (!m_get&&!Player->GetHave()&& !m_throw)
	{
		if (Player->GetAttack())
		{//攻撃で倒したとき
			D3DXVECTOR3 Move = Player->GetMove();
			CGame::GetStage()->GetScore()->Add(100);
			Player->Setlength(10000.0f);

			Player->SetMove({ Move.x,20.0f,Move.z });
			Player->SetLoolHit(false);
			Player->SetAttack(false);
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_BOM);
			Uninit();
		}
		else
		{
			if (!Player->GetcheckHit())
			{//当たったとき
				CRenderer::GetCamera()->ShakeCamera(30, 10.0f);

				D3DXVECTOR3 Move = Player->GetMove();
				Player->SetcheckHit(true);
				CGame::GetStage()->GetScore()->Add(-100);
				Player->SetMove({ Move.x,20.0f,Move.z });
				CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_DAMAGE);
			}
		}
	}
}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CEnemy::NotHit()
{
	m_hit = false;
}

//--------------------------------------------------
// Passの設定
//--------------------------------------------------
void CEnemy::SetPass(std::string pass)
{
	SetMotion(pass.c_str());
	m_myfilepass = pass;
}

//--------------------------------------------------
// 当たり判定の設定
//--------------------------------------------------
bool CEnemy::CollisionModel(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize,CObject *object)
{
	bool bIsLanding = false;

	D3DXMATRIX mtxWorld = *CMotionModel3D::GetMtxWorld();

	D3DXVECTOR3 min = GetMin();
	D3DXVECTOR3 max = GetMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			Hit(object);
			bIsLanding = true;
		}
	}
	else
	{
		NotHit();
	}

	return bIsLanding;
}


