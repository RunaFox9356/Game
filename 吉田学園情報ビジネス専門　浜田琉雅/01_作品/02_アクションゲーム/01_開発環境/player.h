//=============================================================================
//
// プレイヤー設定ヘッター
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _PLAYER_H_
#define _PLAYER_H_

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "motion_model3D.h"
#include "Trajectory.h"
//-----------------------------------------------------------------------------
// 前方宣言
//-----------------------------------------------------------------------------
class CTrajectory;
class CReaction;
class CChaseEnemy;
class CEnemy;
class CShadow;
#define MAXLOG (30)
#define MAXHAVE (3)
//-----------------------------------------------------------------------------
// プレイヤー操作キャラクタークラス
//-----------------------------------------------------------------------------
class CPlayer : public CMotionModel3D
{
public:

	//--------------------------------------------------------------------
	// プレイヤーのアクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_TYPE
	{
		TYPE_NEUTRAL = 0,		// ニュートラル
		TYPE_MOVE,			// 移動
		TYPE_ATTACK,			// 攻撃
		TYPE_MAX,				// 最大数
	};

	//modelデータの構造体//
	struct ModelData
	{
		int key;		// 時間管理
		int nowKey;		// 今のキー
		int loop;		// ループするかどうか[0:ループしない / 1 : ループする]
		int num_key;  	// キー数
		/*MyKeySet KeySet[MAX_KEY];*/
	};

public: // 定数
	static const int MAXLIFE;				// 最大体力
	static const float MOVE_ATTENUATION;	// 減衰係数
	static const float MOVE_ROTATTENUATION;	// 回転移動減衰係数
	static const float SPEED;				// スピード
	static const float WIDTH;				// モデルの半径
	static const int MAX_PRAYER;			// 最大数
	static const int MAX_MOVE;				// アニメーションの最大数
	static const int INVINCIBLE;			// 無敵時間
	static const int GRAVITYTIME;			// 無敵時間
	static const int MAX_MODELPARTS;		// モデルの最大数

public:
	CPlayer();
	~CPlayer();

	HRESULT Init()override;	// 初期化
	void Uninit()override;	// 破棄
	void Update()override;	// 更新
	void Draw()override;	// 描画

	static CPlayer *Create();

	void SetLineHit(bool IsHit) { m_lineHit = IsHit; m_lineHitCount = 5; }
	bool GetLineHit() { return m_lineHit; }
	void SetFriction(float Infriction) { m_friction = Infriction; }

	D3DXVECTOR3 GetPosOld() { return m_posOld; }
	D3DXVECTOR3 GetMove() { return m_move; }

	void SetMove(D3DXVECTOR3 ismove) { m_move = ismove; }
	void SetMoveDash(D3DXVECTOR3 ismove) { m_moveDash = ismove; }
	D3DXVECTOR3 GetMoveDash(void) { return m_moveDash; }

	void AddMove(D3DXVECTOR3 ismove) { m_move += ismove; }

	void SetIsGravity(bool IsGravity) { m_isGravity = IsGravity; }
	bool GettIsGravity() { return m_isGravity; }
	//CTrajectory *GetTrajectory() { return m_Trajectory; }

	D3DXVECTOR3 GetlookMin() { return m_lookmin; }
	D3DXVECTOR3 GetlookMax() { return m_lookmax; }

	bool Collision(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pSize, CObject*Enemy);


	//攻撃するかどうか
	bool GetAttack() { return m_attack; }
	void SetAttack(bool IsAttack) { m_attack = IsAttack; }

	//エネミーをつかむかどうか
	bool GetHave() { return m_have; }

	//エネミーデータを持ってるかどうか
	bool GetHaveData() { return m_haveData; }
	void SetHaveData(bool Ishave) { m_haveData = Ishave; }

	//エネミーを入れる
	void SetHaveEnemy(CChaseEnemy *Ishave) { m_haveEnemy[m_nouEnemy] = Ishave; m_nouEnemy++; }
	//エネミーを入れる
	void SetHaveDefEnemy(CEnemy *Ishave) { m_haveDefEnemy[m_nouEnemy] = Ishave; m_nouEnemy++; }

	int GetNouEnemy() { return m_nouEnemy; }


	//ロックオン	
	void SetLoolHit(bool IsLoolHit) { m_loolHit= IsLoolHit; }
	
	
	void Setlength(float length) { m_length = length; };
	float Getlength() { return m_length; }

	void SetcheckHit(bool length);
	bool GetcheckHit() { return m_hit; }

	void SetMoveRock(bool length) { m_moveLock = length; m_lockTime = INVINCIBLE; }
	bool GetMoveRock() { return m_moveLock; }

	void SetMeshHit(bool length) { m_meshHit = length; }
	bool GetMeshHit() { return m_meshHit; }

	void SetReSpoonPos(D3DXVECTOR3 IsSpoon) { m_reSpoonPos = IsSpoon; }
	D3DXVECTOR3 GetposLog(int number) { return m_posLog[number]; }

	void SetIsMove(bool length) { m_movePlayer = length; }

	void SetGravity(bool length) { m_isGravity = length; m_gravityTime = GRAVITYTIME; }
	bool GetGravity() { return m_isGravity; }

	void Setjump(bool length) { m_jump = length; }

	CShadow * GetShadow() { return m_shadow; };
	void SetIsDeath(bool length) { m_setIsDeath = length; }
private:
	void Move();		// 移動
	void TitleMove();
	void ResetMove();
	void TutorialMove();	//動きセット

private:
	
	int m_lockTime;
	int m_damage;
	int m_nouEnemy;
	int m_haveTime;//持ってるかどうか
	int m_gravityTime;//重力時間
	int m_lineHitCount;
	float m_moveSpeed;
	float m_friction;//地面摩擦係数
	float m_gravity;//重力
	float m_length;

	float m_moveSetZ;
	float m_moveSetX;

	bool m_setIsDeath;
	bool m_meshHit;
	bool m_lineHit;	//lineに当たってるかどうか
	bool m_isGravity;//重力使うかどうか
	bool m_hit;//Damageくらったかどうか
	bool m_moveLock;//Damageくらったかどうか
	bool m_have;//持ってるかどうか
	bool m_haveData;
	bool m_change;
	bool m_movePlayer;
private:
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_posLog[MAXLOG];
	D3DXVECTOR3 m_rotMove;
	

	D3DXVECTOR3 m_rot;

	D3DXVECTOR3 m_reSpoonPos;

	D3DXVECTOR3 m_moveDash;

	//感知範囲
	D3DXVECTOR3 m_lookmin;
	D3DXVECTOR3 m_lookmax;
	D3DXVECTOR3 m_goPos;
	//CTrajectory*m_Trajectory;
	ACTION_TYPE m_action;
	CReaction *m_lockon;
	CObject* m_enemy;

	CEnemy* m_haveDefEnemy[MAXHAVE];
	CChaseEnemy* m_haveEnemy[MAXHAVE];
	CShadow * m_shadow;
	bool m_loolHit;
	bool m_attack;
	bool m_jump;
	bool m_isDaesh;
};
#endif
