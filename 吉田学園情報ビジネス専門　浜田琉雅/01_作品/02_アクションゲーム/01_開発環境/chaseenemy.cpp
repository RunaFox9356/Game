//=============================================================================
//
// 敵設定
// Author:hamada ryuuga
//
//=============================================================================

#include "chaseenemy.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "reaction.h"
#include "boss.h"
#include "score.h"
#include "manager.h"
#include "renderer.h"
#include "sound.h"
#include "renderer.h"
#include "camera.h"
#include "shadow.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CChaseEnemy::CChaseEnemy()
{
	m_lookmin = { -500.0f,-500.0f ,-500.0f };
	m_lookmax = { 500.0f,500.0f ,500.0f };
	m_lookCount = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CChaseEnemy::~CChaseEnemy()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CChaseEnemy::Init()
{
	// 現在のモーション番号の保管
	CEnemy::Init();
	SetUp(CObject::ENEMY);
	m_hit = false;
	m_isMove = false;
	m_isPlayerHit = false;
	m_aiming = true;
	m_aimingCount = 0;
	m_des = false;
	m_get = false;
	m_sensing = CReaction::Create();
	m_sensing->SetTexture(CTexture::TEXTURE_OFF);
	m_sensing->SetPos(GetPos());
	m_sensing->SetPopEvent(false);
	m_sensing->SetSize(D3DXVECTOR3(50.0f, 50.0f, 0.0f));
	m_sensing->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	m_move = { 0.0f,0.0f,0.0f };
	m_throw = false;
	m_nouHave = 0;
	m_alpha = 0.3f;
	m_isChange = true;
	m_shadow = CShadow::Create(GetPos());
	m_shadow->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.7f));
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CChaseEnemy::Uninit()
{
	CEnemy::Uninit();
	m_shadow->Release();
	m_sensing->Release();
	m_des = true;

}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CChaseEnemy::Update()
{

	CEnemy::Update();
	if (m_get)
	{
		Move();
		m_sensing->SetPos(D3DXVECTOR3{100000.0f,1000000.0f,1000.0f});
		m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
	else
	{
		CPlayer*Player = CGame::GetStage()->GetPlayer();
		D3DXVECTOR3  pPos = Player->GetPos();
		D3DXVECTOR3  pPosOld = Player->GetPosOld();
		if (CollisionMove(&pPos, &pPosOld, &D3DXVECTOR3(100.0f, 100.0f, 100.0f)))
		{
			D3DXVECTOR3 pos = GetPos();
			m_sensing->SetPos(D3DXVECTOR3(pos.x, pos.y+100.0f, pos.z));
			m_sensing->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

			if (m_isChange)
			{
				m_alpha += 0.01f;
			}
			else
			{
				m_alpha -= 0.01f;
			}
			if (m_alpha <= 0.3f)
			{
				m_isChange = true;
			}
			if (m_alpha >= 1.0f)
			{
				m_isChange = false;
			}
			m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_alpha));
		}
		else
		{
			m_sensing->SetPos(GetPos());
			m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		}

		if (m_aiming)
		{//ロケット発射
			D3DXVECTOR3 pos = GetPos();
			pos.y += 10.0f;
			SetPos(pos);

			m_aimingCount++;
			if (m_aimingCount >= 120)
			{
			
				SetPos(D3DXVECTOR3(pPos.x, 1000, pPos.z));
				m_aiming = false;
			}
		}
		else
		{//プレイヤー
			
			//重力
			D3DXVECTOR3 pos = GetPos();
			pos.y -= 10.0f;
			SetPos(pos);
		}
		if (GetPos().y <= -100.0f)
		{
			Uninit();
		}
	}
	D3DXVECTOR3 pos = GetPos();
	if (CGame::GetStage()->GetHitMesh(pos))
	{
		m_shadow->SetPos(D3DXVECTOR3 (pos.x,3.0f, pos.z));
		m_shadow->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.7f));
	
	}
	else
	{
		m_shadow->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}


}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CChaseEnemy::Draw()
{
	CEnemy::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CChaseEnemy *CChaseEnemy::Create(D3DXVECTOR3 pos)
{
	CChaseEnemy *pEnemy = nullptr;

	pEnemy = new CChaseEnemy;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::CHASEENEMY);
		pEnemy->SetMotion("data\\system\\enemy\\snake.txt");
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CChaseEnemy::Move()
{

	if (m_throw)
	{//投げられた時
		D3DXVECTOR3 *posOld = &GetPos();
		D3DXVECTOR3 *pos = &GetPos();
		*pos += m_move;
		SetPos(*pos);
		SearchModelObject(0, BOSS, [this, pos, posOld](CObject*Enemy)
		{
			CEnemy* pEnemy = dynamic_cast<CEnemy*>(Enemy);
			D3DXVECTOR3  Size = pEnemy->GetMax() - pEnemy->GetMin();
			pEnemy->CollisionModel(pos, posOld, &Size, this);
		});
	}
	else
	{//つかまってる時
		m_posOld = GetPos();//過去の移動量を保存
		D3DXVECTOR3 pos = GetPos();
		D3DXVECTOR3 rot = GetRot();

		//modelとプレイヤーの当たり判定
		CPlayer*Player = CGame::GetStage()->GetPlayer();

		pos = Player->GetposLog(9* m_nouHave);
	
		rot = Player->GetRot();

		NormalizeAngle(&rot.y);
		SetPos(pos);
		SetRot(rot);
	}


}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CChaseEnemy::Hit(CObject *object)
{
	CPlayer*Player = CGame::GetStage()->GetPlayer();
	if (Player->GetHave() && (Player->GetNouEnemy() <= MAXHAVE - 1) && !m_get)
	{//捕まえる
		m_get = true;
		Player->SetHaveData(true);
		Player->SetHaveEnemy(this);
		m_nouHave = Player->GetNouEnemy();
		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_CATCH);

	}
	if (!m_get&&!Player->GetHave())
	{
		if (Player->GetAttack())
		{//攻撃する
			D3DXVECTOR3 Move = Player->GetMove();
			CGame::GetStage()->GetScore()->Add(100);
			Player->Setlength(10000.0f);

			Player->SetMove({ Move.x,20.0f,Move.z });
			Player->SetLoolHit(false);
			Player->SetAttack(false);
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_BOM);
			Uninit();
		}
		else
		{
			if (!Player->GetcheckHit())
			{//ダメージ

				CRenderer::GetCamera()->ShakeCamera(30, 10.0f);

				D3DXVECTOR3 Move = Player->GetMove();
				Player->SetcheckHit(true);
				CGame::GetStage()->GetScore()->Add(-100);
				Player->SetMove({ Move.x,20.0f,Move.z });
				CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_DAMAGE);
			}
		}
	}

}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CChaseEnemy::NotHit()
{
	m_hit = false;
}

//--------------------------------------------------
// こりじょん判定
//--------------------------------------------------
bool CChaseEnemy::CollisionMove(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize)
{

	bool bIsLanding = false;

	D3DXMATRIX mtxWorld = *CMotionModel3D::GetMtxWorld();

	D3DXVECTOR3 min = GetlookMin();
	D3DXVECTOR3 max = GetlookMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		//if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			bIsLanding = true;
		}
	}
	else
	{
	
	}

	return bIsLanding;
}



