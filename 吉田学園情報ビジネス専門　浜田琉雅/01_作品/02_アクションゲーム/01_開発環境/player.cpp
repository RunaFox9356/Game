//=============================================================================
//
// プレイヤー設定
// Author:hamada ryuuga
//
//=============================================================================
//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <assert.h>
#include "player.h"
#include "input.h"
#include "camera.h"
#include "motion.h"
#include "manager.h"
#include "motion.h"
#include "particle_manager.h"
#include "utility.h"
#include "game.h"
#include "tutorial.h"
#include "title.h"
#include "mesh.h"
#include "building.h"
#include "stage.h"
#include "ball.h"
#include "wood_spawn.h"
#include "Trajectory.h"
#include "jeteffect.h"
#include "reaction.h"
#include "effect.h"
#include "moveeffect.h"
#include "hamada.h"
#include "enemy.h"
#include "chaseenemy.h"
#include "inputkeyboard.h"
#include "shadow.h"
#include "sound.h"
#include "score.h"
//=============================================================================
// static変数
//=============================================================================
const int CPlayer::MAXLIFE = 300;				// 最大体力
const float CPlayer::MOVE_ATTENUATION = 0.1f;	// 移動減衰係数
const float CPlayer::MOVE_ROTATTENUATION = 0.5f;	// 移動減衰係数
const float CPlayer::SPEED = 1.0f;		 		// 移動量
const float CPlayer::WIDTH = 10.0f;				// モデルの半径
const int CPlayer::MAX_PRAYER = 16;				// 最大数
const int CPlayer::MAX_MOVE = 9;				// アニメーションの最大数
const int CPlayer::INVINCIBLE = 120;				// 無敵時間
const int CPlayer::GRAVITYTIME = 60;				// 無敵時間
const int CPlayer::MAX_MODELPARTS = 9;			// モデルの最大数

//=============================================================================
// コンストラクタ
//=============================================================================
CPlayer::CPlayer()
{
	m_lookmin = { -500.0f,-500.0f ,-500.0f };
	m_lookmax = { 500.0f,500.0f ,500.0f };
	m_loolHit = false;
}

//=============================================================================
// デストラクタ
//=============================================================================
CPlayer::~CPlayer()
{

}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CPlayer::Init()
{
	// 現在のモーション番号の保管

	CMotionModel3D::Init();
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	D3DXVECTOR3	Size(100.0f, 100.0f, 100.0f);
	m_moveDash = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_moveSpeed = 3.5f;
	SetSize(Size);
	//m_Trajectory = CTrajectory::Create();
	m_gravity = -1.0f;
	//m_Trajectory->SetMtx(GetMtxWorld());
	m_rotMove = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_action = TYPE_NEUTRAL;
	m_isGravity = true;
	m_attack = false;
	m_have = false;
	m_haveData = false;
	m_change = false;
	m_hit = false;
	m_moveLock = false;
	m_movePlayer = false;
	m_jump = false;
	m_setIsDeath = false;
	m_lockon = CReaction::Create();
	m_lockon->SetTexture(CTexture::TEXTURE_LOOKON);
	m_lockon->SetPos(GetPos());
	m_lockon->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));
	m_lockon->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	m_length = 10000.0f;
	m_moveSetX = 0.0f;
	m_moveSetZ = 0.0f;
	m_nouEnemy = 0;
	m_damage = 0;
	m_gravityTime = 0;
	SetUp(PLAYER);
	
	m_shadow = CShadow::Create(GetPos());
	m_shadow->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.7f));
	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CPlayer::Uninit()
{
	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();
}

//=============================================================================
// 更新
//=============================================================================
void CPlayer::Update()
{
	m_length = 10000.0f;
	m_meshHit = false;
	m_loolHit = false;
	if (!m_lineHit)
	{
	}
	else
	{
		m_move.y = 0.0f;
	}

	if (m_lineHit)
	{
		m_lineHitCount--;
		if (m_lineHitCount <= 0)
		{
			m_lineHit = false;
		}
		
	}

	//GetTrajectory()->SetIsDraw(true);

//	CMotion *pMotion = CMotionModel3D::GetMotion();
	
	if (m_movePlayer)
	{
		switch (*CManager::GetInstance()->GetMode())
		{
		case CManager::MODE_TITLE:
			TitleMove();	//動きセット
			break;
		case CManager::MODE_GAME:
		case CManager::MODE_BOSS:
			Move();	//動きセット	
			break;
		case CManager::MODE_RESULT:
			ResetMove();
			break;
		case CManager::MODE_RANKING:
			break;
		case CManager::MODE_TUTORIAL:
			TutorialMove();
			break;
		default:
			break;
		}
	}


	if (!m_isGravity)
	{
		m_gravityTime--;
		if (m_gravityTime <= 0)
		{
			m_gravityTime = 0;
			m_isGravity = true;
		}
	}
	//拘束判定
	if (m_moveLock)
	{
		m_lockTime--;
		float Timer = (m_lockTime % 60)/60.0f;
		Timer *= (D3DX_PI*2);
		m_rot.z= sinf(Timer)*0.8f;
		NormalizeAngle(&m_rot.z);
		SetRot(m_rot);
		if (m_lockTime <= 0)
		{
			m_rot.z = 0.0f;
			SetRot(m_rot);
			SetIsColor(false);
			m_lockTime = 0;
			m_moveLock = false;
		}
		

	}
	//Damage判定
	if (m_hit)
	{
		m_change = !m_change;
		if (m_change)
		{
			ChangeColar(D3DXCOLOR{ 1.0f,0.0f,0.0f,0.5f });
		}
		else
		{
			SetIsColor(false);
		}
		m_damage--;
		if (m_damage <= 0)
		{
			SetIsColor(false);
			m_damage = 0;
			m_hit = false;
		}
	}
	if (m_have)
	{
		m_haveTime--;
		if (m_haveTime <= 0)
		{
			m_haveTime = 0;
			m_have = false;
		}
	}
	// 現在のモーション番号の保管
	CMotionModel3D::Update();
}

//=============================================================================
// 描画
//=============================================================================
void CPlayer::Draw()
{
	if (m_setIsDeath)
	{
		D3DXVECTOR3 Rot = GetRot();
		CGame::GetStage()->GetPlayer()->SetRot(D3DXVECTOR3 (0.0f, 0.0f, D3DX_PI*0.5f));
	}
	if (m_lineHit == false&& *CManager::GetInstance()->GetMode() == CManager::MODE_GAME || *CManager::GetInstance()->GetMode() == CManager::MODE_BOSS)
	{
		D3DXQUATERNION quat;
		D3DXQuaternionIdentity(&quat);
		D3DXVECTOR3 vecY = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		D3DXQuaternionRotationAxis(&quat, &vecY, D3DX_PI);
		CGame::GetStage()->GetPlayer()->SetQuat(quat);
		
	}
	else
	{

	}
	m_shadow->SetQuat(GetQuat());
	CMotionModel3D::Draw();
}

//=============================================================================
// create
//=============================================================================
CPlayer *CPlayer::Create()
{
	CPlayer * pObject = nullptr;
	pObject = new CPlayer;

	if (pObject != nullptr)
	{
		pObject->Init();
	}

	return pObject;
}
//=============================================================================
// Move
//=============================================================================
void CPlayer::Move()	//動きセット
{
	m_friction = 0.0f;
	CInput *CInputpInput = CInput::GetKey();
	D3DXVECTOR3 *Camerarot = CRenderer::GetCamera()->GetRot();
	D3DXVECTOR3 Pos = GetPos();
	float consumption = 0.0f;
	float consumptionX = 0.0f;

	CMotion *pMotion = CMotionModel3D::GetMotion();

	if (!m_moveLock)
	{
		if (!m_attack&&!m_setIsDeath && !m_lineHit)
		{
			if (CInputpInput->Trigger(CInput::KEY_INVENTORY))
			{//回避
				m_move.x += (sinf(D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed) * 30.0f;
				m_move.z += (cosf(D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed) * 30.0f;

				consumptionX = m_rotMove.x + (D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
			}

			if (CInputpInput->Trigger(CInput::KEY_DROP))
			{//回避
				m_move.x += (sinf(-D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed)*30.0f;
				m_move.z += (cosf(-D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed)*30.0f;

				consumptionX = m_rotMove.x - (D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
			}

			if (CInputpInput->Press(CInput::KEY_RIGHT))
			{
				m_move.x += sinf(D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed;
				m_move.z += cosf(D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed;
				consumption = m_rotMove.x + (D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
			}
			if (CInputpInput->Press(CInput::KEY_LEFT))
			{
				m_move.x += sinf(-D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed;
				m_move.z += cosf(-D3DX_PI * 0.5f + Camerarot->y) * SPEED * m_moveSpeed;

				consumption = m_rotMove.x - (D3DX_PI*0.5f) - m_rot.y + Camerarot->y;
			}
			if (CInputpInput->Press(CInput::KEY_DOWN))
			{
				
					m_move.x += sinf(Camerarot->y - D3DX_PI) * SPEED * m_moveSpeed;
					m_move.z += cosf(Camerarot->y - D3DX_PI) * SPEED * m_moveSpeed;

					consumption = (m_rotMove.x - D3DX_PI - m_rot.y + Camerarot->y);
			}
			if (CInputpInput->Press(CInput::KEY_UP))
			{
				m_move.x += sinf(Camerarot->y) * SPEED * m_moveSpeed;
				m_move.z += cosf(Camerarot->y) * SPEED * m_moveSpeed;

				consumption = m_rotMove.x - m_rot.y + Camerarot->y;

			}
			if (CInputpInput->Trigger(CInput::KEY_SHIFT))
			{//ダッシュ

				CJetEffect*JetEffect = CJetEffect::Create(m_move);
				JetEffect->SetPos(Pos);
				JetEffect->SetRot(m_rot);
				D3DXVECTOR3 MoveVex = m_move;
				D3DXVec3Normalize(&MoveVex, &MoveVex);

				m_move.x += MoveVex.x*50.0f;
				m_move.z += MoveVex.z*50.0f;

				if (m_move.x >= 80.0f)
				{
					m_move.x = 80.0f;
				}
				if (m_move.z >= 80.0f)
				{
					m_move.z = 80.0f;
				}
			}
		}
		//int Size = CGame::GetStage()->GetNumAllBuilding();

		//ラインに当たってない時と重力が付いてる時
		if (m_isGravity && !m_lineHit)
		{
			m_move.y += m_gravity;
		}
		if (CInputpInput->Trigger(CInput::KEY_SHOT)&& !m_jump)
		{ 
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_JAMP);
			m_jump = true;
			//ジャンプ量
			m_move.y += 15.0f;
		}

		for (int LogCount = MAXLOG - 2; LogCount >= 0; LogCount--)
		{//バブルソート
			m_posLog[LogCount + 1] = m_posLog[LogCount];
		}
		m_posLog[0] = m_posOld;
		m_posOld = Pos;//移動を加算

		NormalizeAngle(&consumption);	//正規化
		NormalizeAngle(&consumptionX);	//正規化


		//動きの加速度
		if (!m_lineHit &&(CInputpInput->Press(CInput::KEY_LEFT)
			|| CInputpInput->Press(CInput::KEY_RIGHT)))
		{
			m_moveSetX += 0.1f;
			CMoveEffect*MoveEffect = CMoveEffect::Create(m_move);
			MoveEffect->SetPos({ Pos.x,Pos.y + 30.0f ,Pos.z });
			if (m_action == TYPE_NEUTRAL)
			{
				if (pMotion != nullptr)
				{//アニメーション設定
					m_action = TYPE_MOVE;
					pMotion->SetNumMotion(m_action);
				}
			}
		}
		else if(!m_lineHit && (CInputpInput->Press(CInput::KEY_UP) 
			|| CInputpInput->Press(CInput::KEY_DOWN)))
		{
			m_moveSetZ += 0.1f;
			CMoveEffect*MoveEffect = CMoveEffect::Create(m_move);
			MoveEffect->SetPos({ Pos.x,Pos.y + 30.0f ,Pos.z });
			if (m_action == TYPE_NEUTRAL)
			{
				if (pMotion != nullptr)
				{//アニメーション設定
					m_action = TYPE_MOVE;
					pMotion->SetNumMotion(m_action);
				}
			}
		}
		else if (CInputpInput->Trigger(CInput::KEY_INVENTORY)
			|| CInputpInput->Trigger(CInput::KEY_DROP))
		{
			m_moveSetX += 1.0f;
			CMoveEffect*MoveEffect = CMoveEffect::Create(m_move);
			MoveEffect->SetPos({ Pos.x,Pos.y + 30.0f ,Pos.z });
			if (m_action == TYPE_NEUTRAL)
			{
				if (pMotion != nullptr)
				{//アニメーション設定
					m_action = TYPE_ATTACK;
					pMotion->SetNumMotion(m_action);
				}
			}
		}
		else
		{
			m_action = TYPE_NEUTRAL;
			pMotion->SetNumMotion(m_action);
			m_moveSetX = 0.0f;
			m_moveSetZ = 0.0f;
		}

		if (!m_attack && !m_lineHit)
		{//加速してる時とラインに当たってない時の判定
			if (m_moveSetZ >= 1.0f)
			{
				m_moveSetZ = 1.0f;
			}
			float MoveSizeZ = hmd::easeInSine(m_moveSetZ);
			if (m_moveSetX >= 1.0f)
			{
				m_moveSetX = 1.0f;
			}
			float MoveSizeX = hmd::easeInSine(m_moveSetX);
			m_move.x *= MoveSizeX;
			m_move.z *= MoveSizeZ;
		}

		{
			m_moveDash.x += (0.0f - m_moveDash.x) * (MOVE_ATTENUATION + m_friction);
			m_moveDash.y += (0.0f - m_moveDash.y) * (MOVE_ATTENUATION + m_friction);
			m_moveDash.z += (0.0f - m_moveDash.z) * (MOVE_ATTENUATION + m_friction);

			m_move.x += (0.0f - m_move.x) * (MOVE_ATTENUATION + m_friction);	//（目的の値-現在の値）＊減衰係数
			m_move.z += (0.0f - m_move.z) * (MOVE_ATTENUATION + m_friction);
		}
		//	必殺技
		if (CInputpInput->Press(CInput::KEY_HAVE))
		{
			m_have = true;
			m_haveTime = 10;
		}
		// 移動用の箱の用意
		D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

		Pos += m_move + m_moveDash;//移動を加算

		//プレイヤーとメッシュのはんてい
		Pos = CGame::GetStage()->GetPlayerHitMesh(Pos, Pos);
		m_shadow->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		D3DXVECTOR3 ShadowPos = CGame::GetStage()->GetPlayerHitMesh(GetPos(), Pos, true, m_shadow);
		m_shadow->SetPos(ShadowPos);

		SearchModelObject(0, ENEMY, [this](CObject*Enemy)
		{
			CEnemy* pEnemy = dynamic_cast<CEnemy*>(Enemy);
			D3DXVECTOR3  Size = pEnemy->GetMax() - pEnemy->GetMin();
			Collision(&pEnemy->GetPos(), &Size, pEnemy);
		});

		//ターゲット
		m_lockon->PopEvent(m_enemy);

		//	必殺技
		if (CInputpInput->Trigger(CInput::KEY_DECISION))
		{
			if (!m_haveData)
			{
				if (*CManager::GetInstance()->GetMode() == CManager::MODE_GAME)
				{//ゲームに必要なもの
					if (m_loolHit)
					{//攻撃
						m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
						m_attack = true;
					}
				}

			}
			else
			{//敵を投げる
				if (*CManager::GetInstance()->GetMode() == CManager::MODE_GAME)
				{//ゲームのときの敵
				}
				else
				{//ホーミングの敵
					//CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_THROW);
					D3DXMATRIX mtx;	// 計算用マトリックス
					D3DXVECTOR3 centerMove(0.0f, 0.0f, 50.0f);
					D3DXMatrixRotationYawPitchRoll(&mtx, m_rot.y, m_rot.x, m_rot.z);
					D3DXVec3TransformCoord(&centerMove, &centerMove, &mtx);
					m_haveEnemy[0]->SetThrow(true);
					m_haveEnemy[0]->SetRot(m_rot);
					m_haveEnemy[0]->SetMove(centerMove);
					m_haveEnemy[0]->SetPos(GetPos());
					m_nouEnemy--;
					for (int i = 0; i < m_nouEnemy; i++)
					{
						m_haveEnemy[i] = m_haveEnemy[i + 1];
					}
				}

				if (m_nouEnemy <= 0)
				{
					m_haveData = false;
				}
			}

		}
		if (!m_loolHit)
		{//攻撃

			m_lockon->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			m_lockon->SetPos(D3DXVECTOR3(1.0f, -11111.0f, 1.0f));
		}

		if (m_attack)
		{//ホーミング設定

			D3DXVECTOR3 vecPlayerDist = m_goPos - GetPos();
			float distPlayer = D3DXVec3Length(&vecPlayerDist);

			m_move += vecPlayerDist / distPlayer * 15.0f;

			m_rot.y = (atan2f(vecPlayerDist.x, vecPlayerDist.z));

			for (int i = 0; i < 10; i++)
			{
				CEffect::Create(-m_move)->SetPos(GetPos());
			}
			m_attack = true;
		}
		//セット
		SetPos(Pos);

		SearchModelObject(PRIORITY_OBJECT, BUILDING, [this, &Pos](CObject*Building)
		{//建物との当たり判定
			CBuilding* Obj = dynamic_cast<CBuilding*>(Building);  // ダウンキャスト 

			if (Obj != nullptr)
			{
				if (Obj->CollisionModel(&Pos, &m_posOld, &GetSize()))
				{

				}
			}
		});

		if (Pos.y <= -500.0f)
		{
			m_move.y = 0.0f;
			SetPos(m_reSpoonPos);
			SetcheckHit(true);
			CGame::GetStage()->GetScore()->Add(-500);
		}
		//減算設定（感性）
		m_rot.y += (consumption)* MOVE_ROTATTENUATION;//目的の値-現在の値）＊減衰係数

		CMotionModel3D::SetRot(m_rot);

	}
}


//=============================================================================
// TitleのときのMove
//=============================================================================
void CPlayer::TitleMove()
{

}

//=============================================================================
// ResetのときのMove
//=============================================================================
void CPlayer::ResetMove()
{
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 *Camerarot = CRenderer::GetCamera()->GetRot();
	float consumption = 0.0f;

	m_move.x += sinf(-D3DX_PI *0.5f + Camerarot->y) * SPEED * m_moveSpeed;
	m_move.z += cosf(-D3DX_PI *0.5f + Camerarot->y) * SPEED * m_moveSpeed;
	consumption = m_rotMove.x + -(D3DX_PI*0.5f) - m_rot.y + Camerarot->y;

	Pos.y = 250.0f;

	m_move.x += (0.0f - m_move.x) * MOVE_ATTENUATION;	//（目的の値-現在の値）＊減衰係数
	m_move.z += (0.0f - m_move.z) * MOVE_ATTENUATION;
	m_move.y += (0.0f - m_move.y) * MOVE_ATTENUATION;

	Pos += m_move;	// 移動を加算

	// 正規化
	if (consumption > D3DX_PI)
	{
		consumption += D3DX_PI * 2.0f;
	}
	if (consumption < -D3DX_PI)
	{
		consumption += -D3DX_PI * 2.0f;
	}

	// 減算設定（感性）
	m_rot.y += (consumption)* MOVE_ROTATTENUATION;	// 目的の値-現在の値）＊減衰係数

	// 正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y += -D3DX_PI * 2;
	}
	if (m_rot.y <= -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2;
	}

	if (Pos.x <= -SCREEN_WIDTH * 0.5f - 100.0f)
	{
		Pos.x = SCREEN_WIDTH * 0.5f;
	}
}

//=============================================================================
// Move
//=============================================================================
void CPlayer::TutorialMove()	//動きセット
{


}

//--------------------------------------------------
// こりじょん判定
//--------------------------------------------------
bool CPlayer::Collision(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pSize,CObject*Enemy)
{


	bool bIsLanding = false;

	D3DXMATRIX mtxWorld = *CMotionModel3D::GetMtxWorld();

	D3DXVECTOR3 min = GetlookMin();
	D3DXVECTOR3 max = GetlookMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			m_loolHit = true;
			bIsLanding = true;
			D3DXVECTOR3 lengthPos = *pPos - pos;
			float length = D3DXVec3Length(&lengthPos);
			if (m_length > length)
			{
				//&&m_lockon->GetEnemy() != Enemy
				m_length = length;
				m_lockon->SetPos(*pPos);
				m_lockon->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
				m_enemy = Enemy;
			
				m_goPos = *pPos;
			}
		}
	}
	else
	{

	}

	return bIsLanding;
}

//=============================================================================
// 当たった時の判定
//=============================================================================
void CPlayer::SetcheckHit(bool length)
{
	m_hit = length;
	m_damage = 120;

	for (int i = 0; i < 5; i++)
	{
		CBuilding* Data = CBuilding::Create("data\\MODEL\\Bell\\suzu.x", &GetPos());
		float X = FloatRandom(50.0f, -50.0f);
		float Z = FloatRandom(50.0f, -50.0f);
		Data->SetMove(D3DXVECTOR3(X,20.0f, Z));
		Data->SetIsMove(true);
	}
}
