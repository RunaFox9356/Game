//**************************************************
//
// spline.Cpp
// Author  : hamada 
//
//**************************************************

#include "linemesh.h"
#include "spline.h"

//=========================================
//CLine* Line[MaxSplineSize];
// Ｂスプライン描画
// x[num], y[num], z[num] は座標の配列
//=========================================
void drowSpline(double *x, double *y, double *z, int num, CLineMesh*m_lineMesh)
{
	if (m_lineMesh == nullptr)
	{
		return;
	}
	Spline xs, ys, zs;
	double t, m;

	xs.init(x, num);
	ys.init(y, num);
	zs.init(z, num);
	
	m = (double)(num - 1);
	D3DXVECTOR3 moveTo(x[0], y[0], z[0]);
	D3DXVECTOR3 lineToOld = moveTo;
	D3DXVECTOR3 lineTo = moveTo;
	int Num = 0;

	for (t = 0.0; t <= m; t += 0.1)
	{
		lineToOld = lineTo;
		lineTo = D3DXVECTOR3(xs.culc(t), ys.culc(t), zs.culc(t));
		if (lineToOld != lineTo)
		{	
			m_lineMesh->VtxCreate(&lineTo, &lineToOld, Num);
			Num += 2;
		}
	}
	m_lineMesh->VtxChange();
}

//=========================================
//CLine* Line[MaxSplineSize];
// Ｂスプライン描画
// x[num], y[num], z[num] は座標の配列
//=========================================
void SetSpline(int size, std::vector <D3DXVECTOR3> &posPos, std::vector <D3DXVECTOR3> &OutPos)
{

	Spline xs, ys, zs;
	double t, m;

	double*x = new double[size];
	double*y = new double[size];
	double*z = new double[size];

	if (size != 0)
	{
		for (int i = 0; i < size; i++)
		{
			x[i] = posPos.at(i).x;
			y[i] = posPos.at(i).y;
			z[i] = posPos.at(i).z;
		}
	}

	xs.init(x, size);
	ys.init(y, size);
	zs.init(z, size);

	m = (double)(size - 1);
	D3DXVECTOR3 moveTo(x[0], y[0], z[0]);
	D3DXVECTOR3 lineToOld = moveTo;
	D3DXVECTOR3 lineTo = moveTo;
	int Num = 0;

	for (t = 0.0; t <= m; t += 0.1)
	{
		lineToOld = lineTo;
		lineTo = D3DXVECTOR3(xs.culc(t), ys.culc(t), zs.culc(t));

		if (Num >= OutPos.size())
		{
			OutPos.push_back(lineTo);
			
		}
		else
		{
			OutPos[Num] = lineTo;
		}
		Num++;
	}
	delete[] x;
	delete[] y;
	delete[] z;

}


//=========================================
//スプラインデータ初期化
//=========================================
void Spline::init(double *sp, int spnum)
{
	double tmp, w[MaxSplineSize + 1];
	int i;

	m_num = spnum - 1;

	// ３次多項式の0次係数(a)を設定
	for (i = 0; i <= m_num; i++)
	{
		a[i] = sp[i];
	}

	// ３次多項式の2次係数(c)を計算
	// 連立方程式を解く。
	// 但し、一般解法でなくスプライン計算にチューニングした方法
	c[0] = c[m_num] = 0.0;
	for (i = 1; i<m_num; i++)
	{
		c[i] = 3.0 * (a[i - 1] - 2.0 * a[i] + a[i + 1]);
	}
	// 左下を消す
	w[0] = 0.0;
	for (i = 1; i<m_num; i++)
	{
		tmp = 4.0 - w[i - 1];
		c[i] = (c[i] - c[i - 1]) / tmp;
		w[i] = 1.0 / tmp;
	}
	// 右上を消す
	for (i = m_num - 1; i>0; i--)
	{
		c[i] = c[i] - c[i + 1] * w[i];
	}

	// ３次多項式の1次係数(b)と3次係数(b)を計算
	b[m_num] = d[m_num] = 0.0;
	for (i = 0; i<m_num; i++)
	{
		d[i] = (c[i + 1] - c[i]) / 3.0;
		b[i] = a[i + 1] - a[i] - c[i] - d[i];
	}
}

//=========================================
//媒介変数(0〜num-1の実数）に対する値を計算
//=========================================
double Spline::culc(double t)
{
	int j;
	double dt;
	j = (int)floor(t); // 小数点以下切捨て
	if (j < 0) j = 0; else if (j >= m_num) j = m_num - 1; // 丸め誤差を考慮

	dt = t - (double)j;
	return a[j] + (b[j] + (c[j] + d[j] * dt) * dt) * dt;
}