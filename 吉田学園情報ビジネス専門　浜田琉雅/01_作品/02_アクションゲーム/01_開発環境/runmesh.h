//=============================================================================
//
// 説明書
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _RUNMESH_H_			// このマクロ定義がされてなかったら
#define _RUNMESH_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "mesh.h"

class CRunMesh : public CMesh
{
public:

	CRunMesh();
	~CRunMesh() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CRunMesh* Create();


private:
	D3DXVECTOR3 m_testrot;
	D3DXVECTOR3 m_movemesh;
	void move();
	void OnHit() override;
};
#endif

