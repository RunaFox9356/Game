//=============================================================================
//
// カメラ設定
// Author:hamada ryuuga
//
//=============================================================================
//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "camera.h"
#include "input.h"

#include "manager.h"
#include "player.h"
#include "stage.h"
#include "title.h"
#include "game.h"
#include "utility.h"
#include "camera_animation.h"
#include "object.h"
#include "enemy.h"

//-----------------------------------------------------------------------------
// 定数
//-----------------------------------------------------------------------------
const D3DXVECTOR3 CCamera::INIT_POSV(0.0f, 0.0f, -750.0f);	// 位置の初期位置
const float CCamera::MOVE_ATTENUATION = 0.1f;	// 減衰係数
const float CCamera::MOVE_ANGLE = 10.0f;	// 減衰係数
const float CCamera::FIELD_OF_VIEW = 45.0f;	// 視野角
const float CCamera::NEAR_VALUE = 10.0f;	// ニア
const float CCamera::FUR_VALUE = 100000.0f;	// ファー

//-----------------------------------------------------------------------------
// コンストラクト
//-----------------------------------------------------------------------------
CCamera::CCamera()
{
	
}

//-----------------------------------------------------------------------------
// デストラクト
//-----------------------------------------------------------------------------
CCamera::~CCamera()
{
}

//-----------------------------------------------------------------------------
// 初期化処理
//-----------------------------------------------------------------------------
void CCamera::Init(void)
{
	m_isFree = false;
	m_isAnimation = false;
	m_isLoop = false;
	m_bossEnd = false;
	m_nouAnimation = 0;
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	//視点　注視点　上方向　設定
	m_posV = D3DXVECTOR3(0.0f, -750.000, -750.0f);
	m_posR = D3DXVECTOR3(0.0f, 10.0f, 10.0f);
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_directionR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_directionV = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_isFrameCount = 0;
	m_isShakePow = 0.0f;

	m_rot.x = atan2f((m_posR.z - m_posV.z),
		(m_posR.y - m_posV.y));
}

//-----------------------------------------------------------------------------
// 終了処理
//-----------------------------------------------------------------------------
void CCamera::Uninit(void)
{

	for (int AnimationSize = 0; AnimationSize < (int)m_animation.size(); AnimationSize++)
	{
		delete m_animation.at(AnimationSize);
		m_animation.at(AnimationSize) = nullptr;
	}
	m_animation.clear();
	

}

//-----------------------------------------------------------------------------
// 更新処理
//-----------------------------------------------------------------------------
void CCamera::Update(void)
{
	if (!m_bossEnd)
	{
		if (*CManager::GetInstance()->GetMode() == CManager::MODE_GAME || *CManager::GetInstance()->GetMode() == CManager::MODE_BOSS)
		{
			if (m_isAnimation)
			{
				if (m_nouAnimation >= GetAnimationSize())
				{//Animationのさいず超えてたら再生しない
					m_isAnimation = false;
					return;
				}
				GetAnimation(m_nouAnimation)->Animation(&m_posV, &m_posR, &m_isAnimation, m_isLoop, m_func);

				//if(保管の時間を調整できるようにする)
			}
			else
			{

				const D3DXVECTOR3 centerMove = CGame::GetStage()->GetPlayer()->GetMoveDash();
				float MoveZVec=centerMove.z*0.1f;
				if (MoveZVec >= 1.5f)
				{
					MoveZVec = 1.5f;
				}
				if (MoveZVec <= 1.0f)
				{
					MoveZVec = 1.0f;
				}
				const D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
				const D3DXVECTOR3 Rot = CGame::GetStage()->GetPlayer()->GetRot();
				m_posRDest.x = centerPos.x + sinf(Rot.y - D3DX_PI) * MOVE_ANGLE;		//目的の値
				m_posRDest.y = centerPos.y + 255.0f;		//目的の値
				m_posRDest.z = centerPos.z + cosf(Rot.y - D3DX_PI) * MOVE_ANGLE;

				m_posVDest.x = centerPos.x - sinf(m_rot.y) * m_fDistance;			//目的の値
				m_posVDest.y = centerPos.y - sinf(m_rot.x - D3DX_PI*0.65f) * m_fDistance*1.5f;	//目的の値
				m_posVDest.z = centerPos.z - cosf(m_rot.y) * (m_fDistance* MoveZVec);

				m_posR.x += (m_posRDest.x - m_posR.x) * MOVE_ATTENUATION;
				m_posR.y += (m_posRDest.y - m_posR.y) * MOVE_ATTENUATION;
				m_posR.z += (m_posRDest.z - m_posR.z) * MOVE_ATTENUATION;

				m_posV.x += (m_posVDest.x - m_posV.x) * MOVE_ATTENUATION;
				m_posV.y += (m_posVDest.y - m_posV.y) * MOVE_ATTENUATION;
				m_posV.z += (m_posVDest.z - m_posV.z) * MOVE_ATTENUATION;
	
				//正規化
				NormalizeAngle(&m_rot.x);
				NormalizeAngle(&m_rot.y);
			}
		}
		else
		{

		}
	}
	else
	{

		CObject::SearchModelObject(0, CObject::BOSS, [this](CObject*Enemy)
		{
			CEnemy* pEnemy = dynamic_cast<CEnemy*>(Enemy);
			const D3DXVECTOR3 centerPos = pEnemy->GetPos();
			const D3DXVECTOR3 Rot = pEnemy->GetRot();

			m_posRDest.x = centerPos.x + sinf(Rot.y - D3DX_PI) * MOVE_ANGLE;		//目的の値
			m_posRDest.y = centerPos.y + 255.0f;		//目的の値
			m_posRDest.z = centerPos.z + cosf(Rot.y - D3DX_PI) * MOVE_ANGLE;

			m_posVDest.x = centerPos.x - sinf(m_rot.y) * m_fDistance;			//目的の値
			m_posVDest.y = centerPos.y - sinf(m_rot.x - D3DX_PI*0.65f) * m_fDistance*1.5f;	//目的の値
			m_posVDest.z = centerPos.z - cosf(m_rot.y) * m_fDistance;

			m_posR.x += (m_posRDest.x - m_posR.x) * MOVE_ATTENUATION;
			m_posR.y += (m_posRDest.y - m_posR.y) * MOVE_ATTENUATION;
			m_posR.z += (m_posRDest.z - m_posR.z) * MOVE_ATTENUATION;

			m_posV.x += (m_posVDest.x - m_posV.x) * MOVE_ATTENUATION;
			m_posV.y += (m_posVDest.y - m_posV.y) * MOVE_ATTENUATION;
			m_posV.z += (m_posVDest.z - m_posV.z) * MOVE_ATTENUATION;
			m_fDistance += 5.0f;
			//正規化
			NormalizeAngle(&m_rot.x);
			NormalizeAngle(&m_rot.y);
		});

	}
}

//-----------------------------------------------------------------------------
// 描画処理
//-----------------------------------------------------------------------------
void CCamera::Set(int Type)
{
	m_type = Type;
	LPDIRECT3DDEVICE9  pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();//デバイスのポインタ

	//ビューマトリックスを初期化
	D3DXMatrixIdentity(&m_mtxView);
	D3DXVECTOR3 RandomMove = {0.0f,0.0f,0.0f};

	if (m_isFrameCount >= 0)
	{
		m_isFrameCount--;
		RandomMove.x = FloatRandom(m_isShakePow, -m_isShakePow);
		RandomMove.y = FloatRandom(m_isShakePow, -m_isShakePow);
		RandomMove.z = FloatRandom(m_isShakePow, -m_isShakePow);
	}

	//ビューマトリックスの作成
	D3DXMatrixLookAtLH(&m_mtxView,
		&(m_posV+RandomMove),
		&(m_posR+RandomMove),
		&m_vecU);

	//適用
	pDevice->SetTransform(D3DTS_VIEW, &m_mtxView);

	//プロジェクションマトリックスを初期化
	D3DXMatrixIdentity(&m_mtxProje);

	// プロジェクションマトリックスの作成(透視投影)
	D3DXMatrixPerspectiveFovLH(&m_mtxProje,				// プロジェクションマトリックス
		D3DXToRadian(FIELD_OF_VIEW),					// 視野角
		(float)SCREEN_WIDTH / (float)SCREEN_HEIGHT,		// アスペクト比
		NEAR_VALUE,										// ニア
		FUR_VALUE);										// ファー

//else
//{
//	// プロジェクションマトリックスの作成(平行投影)
//	D3DXMatrixOrthoLH(&m_MtxProje,					// プロジェクションマトリックス
//		(float)SCREEN_WIDTH,								// 幅
//		(float)SCREEN_HEIGHT,								// 高さ
//		-100.0f,											// ニア
//		2000.0f);											// ファー
//}
//適用
	pDevice->SetTransform(D3DTS_PROJECTION, &m_mtxProje);
}


//-----------------------------------------------------------------------------
// データがあるときの読み込み処理
//-----------------------------------------------------------------------------
void CCamera::LandAnimation(std::string filepass)
{
	if (filepass != "")
	{
		for (int Animation = 0; Animation < (int)m_animation.size(); Animation++)
		{
			if (m_animation.at(Animation)->GetName().c_str() == filepass)
			{
				return;
			}
		}
	}
	CCameraAnimation* setAnimation =CCameraAnimation::create();
	setAnimation->Load(filepass);

	m_animation.push_back(setAnimation);
}
//-----------------------------------------------------------------------------
// 空のデータの作成処理
//-----------------------------------------------------------------------------
void CCamera::AddAnimation()
{
	m_animation.push_back(CCameraAnimation::create());
}

//-----------------------------------------------------------------------------
// データの取得処理
//-----------------------------------------------------------------------------
CCameraAnimation * CCamera::GetAnimation(const int IsPoptime)
{
	return m_animation.at(IsPoptime);
}

//-----------------------------------------------------------------------------
// データの取得処理
//-----------------------------------------------------------------------------
int CCamera::GetAnimationSize()
{
	return m_animation.size();
}

//-----------------------------------------------------------------------------
//	Animationの再生処理
//-----------------------------------------------------------------------------
void CCamera::PlayAnimation(int Animation, bool Isloop)
{
	
	m_nouAnimation = Animation;
	m_isAnimation = true;
	m_isLoop = Isloop;
	
}

//-----------------------------------------------------------------------------
//	Animationの再生処理(オーバーロード)
//-----------------------------------------------------------------------------
void CCamera::PlayAnimation(std::string AnimationName,bool Isloop, std::function<void()> func)
{
	if (AnimationName != "")
	{
		for (int Animation = 0; Animation < (int)m_animation.size(); Animation++)
		{
			if (m_animation.at(Animation)->GetName().c_str() == AnimationName)
			{
				m_nouAnimation = Animation;
				m_isAnimation = true;
				m_isLoop = Isloop;
				break;
			}

		}
	}
	if (func != nullptr)
	{
		m_func = func;
	}
	else
	{
		m_func = nullptr;
	}

}

//-----------------------------------------------------------------------------
//カメラゆらゆらする設定
//-----------------------------------------------------------------------------
void CCamera::ShakeCamera(int IsFrameCount, float IsShakePow)
{
	m_isFrameCount = IsFrameCount;
	m_isShakePow = IsShakePow;
}
//-----------------------------------------------------------------------------
// gameのときの初期化
//-----------------------------------------------------------------------------
void CCamera::GameInit()
{
	m_isAnimation = false;
	m_isLoop = false;
	m_nouAnimation = 0;
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	//視点　注視点　上方向　設定
	m_posV = D3DXVECTOR3(0.0f, 300.0f, -750.0f);
	m_posR = D3DXVECTOR3(0.0f, 250.0f, 0.0f);
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	m_directionR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_directionV = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	m_fDistance = sqrtf((m_posR.x - m_posV.x) *
		(m_posR.x - m_posV.x) +
		(m_posR.z - m_posV.z) *
		(m_posR.z - m_posV.z));

	m_fDistance = sqrtf((m_posR.y - m_posV.y)*
		(m_posR.y - m_posV.y) +
		(m_fDistance*m_fDistance));

	m_rot.x = atan2f((m_posR.z - m_posV.z),
		(m_posR.y - m_posV.y));
	for (int Animation = 0; Animation < GetAnimationSize(); Animation++)
	{
		GetAnimation(Animation)->AnimationReset();
	}

}