//=============================================================================
//
//かめらアニメーション再生機
// Author:hamada ryuuga
//
//=============================================================================

#include "camera_player.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"

#include "camera.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCamera_Player::CCamera_Player()
{

}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCamera_Player::~CCamera_Player()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CCamera_Player::Init()
{
	CObjectX::Init();

	const char* Model = "data\\MODEL\\Bell\\suzu.x";
	SetModel(Model);
	m_hit = false;
	m_isMove = false;
	for (int i = 0; i < 12; i++)
	{
		if (Line[i] != nullptr)
		{
			Line[i] = nullptr;
		}
	}
	CRenderer::GetCamera()->LandAnimation(animationpass);
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CCamera_Player::Uninit()
{
	CObjectX::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCamera_Player::Update()
{
	CObjectX::Update();

	// 動作
	Move();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CCamera_Player::Draw()
{
	CObjectX::NotDraw();
	//CCamera_Player::Setline();
	//Setline();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CCamera_Player *CCamera_Player::Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot, std::string pass)
{
	CCamera_Player *pBell = nullptr;

	pBell = new CCamera_Player;

	if (pBell != nullptr)
	{
		pBell->SetPos(pos);
		pBell->SetRot(rot);
		pBell->SetPass(pass);
		pBell->SetFileName("CameraAnimationPlayer");
		pBell->Init();
		pBell->SetVtxMax(D3DXVECTOR3 (500.0f,500.0f,100.0f));
		pBell->SetVtxMin(D3DXVECTOR3 (-500.0f, -500.0f, 0.0f));
	}

	return pBell;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CCamera_Player::Move()
{
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CCamera_Player::Hit()
{
	if (CGame::GetStage()->GetPlayer()->GetLineHit())
	{
		CRenderer::GetCamera()->PlayAnimation(animationpass.c_str(), false);
	}
	
	
}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CCamera_Player::NotHit()
{
	m_hit = false;
}

void CCamera_Player::Setline()
{
//	D3DXCOLOR col(1.0f, 1.0f, 1.0f, 1.0f);
//
//	for (int i = 0; i < 12; i++)
//	{
//		if (Line[i] == nullptr)
//		{
//			Line[i] = CLine::Create();
//		}
//	}
//	Line[0]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMin().y, GetVtxMax().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMin().y, GetVtxMax().z), col);
//	Line[1]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMin().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMin().x, GetVtxMin().y, GetVtxMax().z), col);
//	Line[2]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMin().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMin().y, GetVtxMin().z), col);
//	Line[3]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMax().x, GetVtxMin().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMin().y, GetVtxMax().z), col);
//	Line[4]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMax().y, GetVtxMax().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMax().y, GetVtxMax().z), col);
//	Line[5]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMax().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMin().x, GetVtxMax().y, GetVtxMax().z), col);
//	Line[6]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMax().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMax().y, GetVtxMin().z), col);
//	Line[7]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMax().x, GetVtxMax().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMax().y, GetVtxMax().z), col);
//	Line[8]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMin().y, GetVtxMax().z), D3DXVECTOR3(GetVtxMin().x, GetVtxMax().y, GetVtxMax().z), col);
//	Line[9]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMin().x, GetVtxMin().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMin().x, GetVtxMax().y, GetVtxMin().z), col);
//	Line[10]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMax().x, GetVtxMin().y, GetVtxMin().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMax().y, GetVtxMin().z), col);
//	Line[11]->SetLine(GetPos(), GetRot(), D3DXVECTOR3(GetVtxMax().x, GetVtxMin().y, GetVtxMax().z), D3DXVECTOR3(GetVtxMax().x, GetVtxMax().y, GetVtxMax().z), col);
}




