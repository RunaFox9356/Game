//============================
//
// メッシュ設定(まっすぐ)
// Author:hamada ryuuga
//
//============================


#include "linemesh.h"
#include "manager.h"
#include "utility.h"
#include "input.h"
#include "spline.h"
#include "game.h"
#include "player.h"
#include "stage.h"

#include "objectX.h"
#include "sound.h"

#include "effect.h"
#include "shadow.h"

CLineMesh::CLineMesh(int nPriority) : CObject(nPriority)
{

}
CLineMesh::~CLineMesh()
{

}

//=========================================
// 初期化処理
//=========================================
HRESULT CLineMesh::Init(void)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();

	m_pTextureEmesh = NULL;
	// 初期化処理
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	// 回転座標
	m_upMove = true;
	m_pVtxBuff = nullptr;	    // 頂点バッファーへのポインタ
	m_fastHit = false;
	m_pTextureEmesh = nullptr;        //テクスチャのポインタ
	m_nowAnimation = 0;
	m_pos = { 0.0f,0.0f,0.0f };	// 頂点座標
	m_posOrigin = { 0.0f,0.0f,0.0f };	// 頂点座標
	m_rot = { 0.0f,0.0f,0.0f };	// 回転座標
	m_maxVtx = MaxVtx;//頂点数	 

	/*m_Cross = CollisionLine(&D3DXVECTOR3(0.0f, 0.0f, 0.0f), &D3DXVECTOR3(0.0f, 0.0f, 1.0f), &D3DXVECTOR3(2.0f, 0.0f, 0.0f), &D3DXVECTOR3(2.0f, 0.0f, 1.0f));
	m_Cross = CollisionLine(&D3DXVECTOR3(0.0f, 0.0f, 0.0f), &D3DXVECTOR3(2.0f, 0.0f, 1.0f), &D3DXVECTOR3(2.0f, 0.0f, 0.0f), &D3DXVECTOR3(0.0f, 0.0f, 1.0f));*/
	
	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * m_maxVtx,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);


	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	for (int nVtx = 0; nVtx < m_maxVtx; nVtx++)
	{
		pVtx[nVtx].pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		pVtx[nVtx].col = D3DXCOLOR(1.0f, 0.3f, 0.6f, 1.0f);
		Line[nVtx] = nullptr;
	}
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
	return S_OK;
}

//=========================================
// 終了処理
//=========================================
void CLineMesh::Uninit(void)
{
	// 頂点バッファーの解放
	if (m_pVtxBuff != NULL)
	{
		m_pVtxBuff->Release();
		m_pVtxBuff = NULL;
	}
	if (m_pTextureEmesh != NULL)
	{
		m_pTextureEmesh->Release();
		m_pTextureEmesh = NULL;
	}


	Release();
}

//=========================================
// 更新処理
//=========================================
void CLineMesh::Update(void)
{
	if (*CManager::GetInstance()->GetMode() == CManager::MODE_GAME || *CManager::GetInstance()->GetMode() == CManager::MODE_BOSS)
	{
		D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
		CollisionMesh(&centerPos);
	}
	
}

//=========================================
// 描画処理
//=========================================
void CLineMesh::Draw(void)
{
	int poptimeSize = GetPoptimeSize();

	if (poptimeSize == 0)
	{
		return;
	}

	{
		double* x = new double[poptimeSize];
		double* y = new double[poptimeSize];
		double* z = new double[poptimeSize];

		for (int i = 0; i < poptimeSize; i++)
		{
			D3DXVECTOR3 vec = GetPoptime(i);

			x[i] = vec.x;
			y[i] = vec.y;
			z[i] = vec.z;
		}

		drowSpline(x, y, z, poptimeSize, this);

		delete[] x;
		delete[] y;
		delete[] z;
	}

	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();
	D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス

	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//ライト設定falseにするとライトと食らわない
	//pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映
	// 行列回転関数(第1引数にヨー(y)ピッチ(x)ロール(z)方向の回転行列を作成)
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールド座標行列の設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);
	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	// ポリゴンの描画
	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, m_nowVtx);

	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}

//=============================================================================
// GetPos関数
//=============================================================================
const D3DXVECTOR3 *CLineMesh::GetPos() const
{
	return &m_posOrigin;
}

//=============================================================================
// Create関数
//=============================================================================
CLineMesh* CLineMesh::Create()
{
	CLineMesh * pObject = nullptr;
	pObject = new CLineMesh;

	if (pObject != nullptr)
	{

		pObject->Init();

	}
	return pObject;
}

//=============================================================================
// VtxCreate関数　スプライン曲線
//============================================================================
void CLineMesh::VtxCreate(D3DXVECTOR3 * Pos, D3DXVECTOR3 * PosOld, int num/*この数値は基本点の数だから*/)
{
	if (m_nowVtx >= m_maxVtx)
	{
		return;
	}

	if (num/2 >= AnimationGetPoptimeSize())
	{
		AnimationAddPoptime(*PosOld);
	}
	else
	{
		AnimationSetPoptime(num / 2,*PosOld);
	}
	
	m_nowVtx = num;

	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	D3DXVECTOR3 Vec = D3DXVECTOR3(Pos->x - PosOld->x, Pos->y - PosOld->y, Pos->z - PosOld->z);


	D3DXVECTOR3 VecPoint1 = D3DXVECTOR3(Vec.z, 0.0f, -Vec.x);

	D3DXVec3Normalize(&VecPoint1, &VecPoint1);

	VecPoint1 = VecPoint1 * 150;
	
	D3DXVECTOR3 Normal;
	//AとBの法線を求めるやつ
	D3DXVec3Cross(&Normal, &Vec, &VecPoint1);

	//Normalをノーマライズして、長さ 1にする。
	D3DXVec3Normalize(&Normal, &Normal);


	if (GetReverse(num/20))
	{
		pVtx[num].pos = (*PosOld + -VecPoint1);
		pVtx[num + 1].pos = (*PosOld + VecPoint1);
		pVtx[num].nor -= Normal;
		pVtx[num + 1].nor -= Normal;

	}
	else
	{
		pVtx[num].pos = (*PosOld + VecPoint1);
		pVtx[num + 1].pos = (*PosOld + -VecPoint1);
		pVtx[num].nor += Normal;
		pVtx[num + 1].nor += Normal;

	}

	//norをノーマライズして、長さ 1にする。
	D3DXVec3Normalize(&pVtx[num].nor, &pVtx[num].nor);
	//norをノーマライズして、長さ 1にする。
	D3DXVec3Normalize(&pVtx[num + 1].nor, &pVtx[num + 1].nor);

	
	
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();

	
}

//=============================================================================
// Vtxがや法線の設定Change
//============================================================================
void CLineMesh::VtxChange()
{
	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	
	if ((GetPoptimeSize() - 1) == 0)
	{
		return;
	}
	int chip = (m_nowVtx / (GetPoptimeSize() - 1));

	for (int Change = 0; Change < m_nowVtx; Change+=2)
	{
		D3DXVECTOR3 Top = pVtx[chip*(Change/ chip)].nor;
		D3DXVECTOR3 Next = pVtx[chip*((Change / chip)+1)].nor;

		D3DXVECTOR3 Diff = Next - Top;
		D3DXVECTOR3 Normal;
		D3DXVECTOR3 OunDiff =(Diff / chip)*(Change%chip);
		Top += OunDiff;

		float Dot = D3DXVec3Dot(&pVtx[Change].nor, &Top);

		if (Dot < 0.0f)
		{
			D3DXVECTOR3 pos = pVtx[Change].pos;
			pVtx[Change].pos = pVtx[Change + 1].pos;
			pVtx[Change + 1].pos = pos;

			D3DXVECTOR3 nor = pVtx[Change].nor;
			pVtx[Change].nor = pVtx[Change + 1].nor;
			pVtx[Change + 1].nor = nor;
		}
	}
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();

}

//========================================
// 当たり判定
// Author: hamada ryuuga
//=========================================
bool CLineMesh::CollisionMesh(D3DXVECTOR3 *pPos)
{
	//CGame::GetStage()->GetPlayer()->SetIsGravity(true);
	bool bIsLanding = false;
	const int nTri = 3;
	// 頂点座標をロック
	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	
	D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス
	D3DXMATRIX mtxWorld;

	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&mtxWorld);

	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	// 行列掛け算関数(第2引数×第3引数を第１引数に格納)
	D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxTrans);

	for (int nCnt = 0; nCnt < m_nowVtx; nCnt++)
	{
		D3DXVECTOR3 posLineVec[nTri];

		posLineVec[0] = pVtx[nCnt].pos;
		posLineVec[1] = pVtx[nCnt+1].pos;
		posLineVec[2] = pVtx[nCnt+2].pos;

		for (int i = 0; i < nTri; i++)
		{//ベクトル３座標をマトリックスで変換する（乗算）
			D3DXVec3TransformCoord(&posLineVec[i], &posLineVec[i], &mtxWorld);
		}

		int  LineCout = 0;

		for (int i = 0; i < nTri; i++)
		{
			//ベクトルS2 V2												
			D3DXVECTOR3 vecWall = posLineVec[(i + 1) % nTri] - posLineVec[i];

			//ベクトル現在のPOSと始点までの距離
			D3DXVECTOR3 vecPos = *pPos - posLineVec[i];

			//外積計算//辺１
			float vecLine = Vec2Cross(&vecPos, &vecWall);

			//三角の中に入ってるときの判定向きによって右側か左側か違うため判定を二つ用意する
			if ((nCnt % 2 == 0 && vecLine >= 0.0f) ||
				(nCnt % 2 != 0 && vecLine <= 0.0f))
			{
				LineCout++;
			}
			else
			{
				break;
			}
		}
		if (LineCout == nTri)
		{
			bIsLanding = true;
			D3DXVECTOR3 V1 = posLineVec[1] - posLineVec[0];
			D3DXVECTOR3 V2 = posLineVec[2] - posLineVec[0];

			D3DXVECTOR3 Normal;
			//AとBの法線を求めるやつ
			D3DXVec3Cross(&Normal, &V1, &V2);

			//vecB をノーマライズして、長さ 1にする。
			D3DXVec3Normalize(&Normal, &Normal);

			D3DXVECTOR3 VecA = *pPos - posLineVec[0];
			//プレイヤーの位置補正
			SwitchCollision(true);
			if (pPos->y <= posLineVec[0].y - (Normal.x*(pPos->x - posLineVec[0].x) + Normal.z*(pPos->z - posLineVec[0].z)) / Normal.y+100.0f)
			{

				OnHit();

			}

			if (m_iscollision)
			{
				
			}
			
		}
		else
		{
		
		}
		
	}
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();

	if (!bIsLanding)
	{
		NoHit();
	}
	return bIsLanding;
}

//========================================
//　当たった時の判定
//=========================================
void CLineMesh::OnHit()
{
	if (!m_fastHit)
	{
		D3DXVECTOR3 PlayerPos = CGame::GetStage()->GetPlayer()->GetPos();
		D3DXVECTOR3 MinPos;
	
		m_fastHit = true;
		//MAXとMINを使って方向いじる；

		D3DXVECTOR3 maxMove = AnimationGetPoptime(AnimationGetPoptimeSize()-1) - PlayerPos;
		D3DXVECTOR3 minMove = AnimationGetPoptime(0) - PlayerPos;

		float MaxLength = D3DXVec3Length(&maxMove);
		float MinLength = D3DXVec3Length(&minMove);

			if (MaxLength < MinLength)
			{
				//m_NowAnimation = (int)AnimationGetPoptimeSize() - 1;
				//CGame::GetStage()->GetPlayer()->SetPos(AnimationGetPoptime(m_NowAnimation));
				m_upMove = false;
			}
			else
			{
				CGame::GetStage()->GetPlayer()->SetPos(AnimationGetPoptime(0));
				m_nowAnimation = 0;
				m_upMove = true;
			}
	}

	if (m_poptime.size() != 0)
	{
		if (m_upMove)
		{
		D3DXVECTOR3 centerPos = CGame::GetStage()->GetPlayer()->GetPos();
		D3DXVECTOR3 centerPosOld = CGame::GetStage()->GetPlayer()->GetPosOld();
		D3DXVECTOR3 rot = CGame::GetStage()->GetPlayer()->GetRot();
		CGame::GetStage()->GetPlayer()->SetIsGravity(false);
		//CGame::GetStage()->GetPlayer()->GetTrajectory()->SetIsDraw(false);
		SwitchCollision(false);

			CGame::GetStage()->GetPlayer()->SetAttack(false);
			D3DXVECTOR3 vec;
			
			vec = AnimationGetPoptime(m_nowAnimation) -centerPos;
			
			float fLength = (vec.x*vec.x) + (vec.z*vec.z);
			fLength = sqrtf(fLength);
			float Rot = atan2f(fLength, vec.y);
		
			//X = Z, Z= -X
			D3DXVECTOR3 axis;    // 回転軸
			D3DXVECTOR3 inverseVec = -(vec);        // move値を反対にする
			D3DXVECTOR3 vecY = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			D3DXVec3Cross(&axis, &inverseVec, &vecY);    // 外積で回転軸を算出。

		
			 // クオータニオンの計算
			D3DXQUATERNION quaternion;
			D3DXQuaternionRotationAxis(&quaternion, &axis, Rot-(D3DX_PI/2.0f));    // 回転軸と回転角度を指定
	
			
			//軸の反転
			D3DXQUATERNION quatTop;
			D3DXQuaternionIdentity(&quatTop);
			D3DXQuaternionRotationAxis(&quatTop, &vecY, D3DX_PI);

			//クオータニオン設定
			quaternion = (quatTop*quaternion);

			//クオータニオンノーマライズ
			D3DXQuaternionNormalize(&quaternion, &quaternion);
			
			//クオータニオン
			CGame::GetStage()->GetPlayer()->SetQuat(quaternion);
			CGame::GetStage()->GetPlayer()->SetLineHit(true);

			//床をホーミングさせる
			Homing(&centerPos, centerPos, AnimationGetPoptime(m_nowAnimation), (float)AnimationSpeed);
			
			D3DXVECTOR3 move = centerPos - CGame::GetStage()->GetPlayer()->GetPos();
			CEffect::Create(-move)->SetPos(centerPos);

			//座標設定
			CGame::GetStage()->GetPlayer()->SetPos(centerPos);
			CGame::GetStage()->GetPlayer()->GetShadow()->SetPos(D3DXVECTOR3(centerPos.x, centerPos.y+3.0f, centerPos.z));
			CGame::GetStage()->GetPlayer()->Setjump(false);

			if (centerPos == AnimationGetPoptime(m_nowAnimation))
			{
				CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_RELL);
				// 進む方向設定
				if (m_upMove)
				{
					m_nowAnimation++;
				}
				else
				{
					m_nowAnimation--;
				}
				if (m_nowAnimation >= (int)AnimationGetPoptimeSize())
				{//最大数を超えたら
				 //座標設定
					D3DXVECTOR3 centerMove(0.0f, 10.0f, 0.0f);
					m_nowAnimation = (int)AnimationGetPoptimeSize() - 1;
					//m_UpMove = !m_UpMove;
				
				}
				if (m_nowAnimation <= 0)
				{//最小になったらまたあげる
					m_nowAnimation = 0;
					//m_UpMove = !m_UpMove;
				}
			}
		}
	}
	else
	{
		m_fastHit = false;
		m_nowAnimation = 0;
	}
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CLineMesh::RotNormalization(D3DXVECTOR3 rot)
{
	// 現在の角度の正規化
	if (rot.y > D3DX_PI)
	{
		rot.y -= D3DX_PI * 2.0f;
	}
	else if (rot.y < -D3DX_PI)
	{
		rot.y += D3DX_PI * 2.0f;
	}
	return rot;
}

//----------------------------
//Wall動き
//----------------------------	
bool CLineMesh::CollisionLine(D3DXVECTOR3 *VecMeshCurrent, D3DXVECTOR3 *VecMeshNext, D3DXVECTOR3 *VecMeshCurrent1, D3DXVECTOR3 *VecMeshNext1)
{
	bool bIsLanding = false;

	//V1 move
	D3DXVECTOR3 vecMove = *VecMeshNext - *VecMeshCurrent;
	D3DXVECTOR3 vecWall;
	D3DXVECTOR3 vecPos;

	//ベクトルS2 V2
	vecWall = VecMeshNext1 - VecMeshCurrent1 + (vecMove);


	//ベクトル現在のPOSと始点までの距離
	vecPos = *VecMeshNext - *VecMeshCurrent1;

	//外積計算
	float vecLine = Vec2Cross(&vecPos, &vecWall);

	if (vecLine <= 0.0f)
	{
		D3DXVECTOR3 vecOld;

		//ベクトルV
		vecOld = *VecMeshCurrent1 - *VecMeshCurrent;

		//計算用の箱
		float vecCalculation[2];

		//T1求めるやつ
		vecCalculation[0] = Vec2Cross(&vecOld, &vecWall);
		vecCalculation[1] = Vec2Cross(&vecMove, &vecWall);

		//posOldから交点までの距離
		float t1 = vecCalculation[0] / vecCalculation[1];

		//T2求めるやつ
		vecCalculation[0] = Vec2Cross(&vecOld, &vecMove);
		vecCalculation[1] = Vec2Cross(&vecMove, &vecWall);

		//壁の視点から交点までの距離
		float t2 = vecCalculation[0] / vecCalculation[1];

		//差
		float eps = 0.00001f;

		if (t1 + eps < 0 || t1 - eps > 1 || t2 + eps < 0 || t2 - eps > 1)
		{//交差してないときの判
		}
		else
		{//交差してるとき

			bIsLanding = true;
		}
	}
	return bIsLanding;
}

