//=============================================================================
//
// dashitem
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _ENEMY_H_
#define	_ENEMY_H_

#include "motion_model3D.h"
#include "stage.h"

class CLine;
//**************************************************
// クラス
//**************************************************
class CEnemy : public CMotionModel3D
{
public:
	CEnemy();
	~CEnemy() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CEnemy *Create(D3DXVECTOR3 pos);
	void Move();//動き
	void quat() {}//クオータにおん
	virtual void Hit(CObject *object);//当たってるとき
	virtual void NotHit();//当たってない時

	void SetPass(std::string pass);//なんのModelを読み込むかどうか
	std::string GetPass(){ return m_myfilepass; }

	bool CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize, CObject *object);//当たり判定

	//Model　sizeの設定
	void SetMin(const D3DXVECTOR3 ismin) { m_min = ismin; }
	void SetMax(const D3DXVECTOR3 ismax) { m_max = ismax; }

	D3DXVECTOR3 GetMin() { return m_min; }
	D3DXVECTOR3 GetMax() { return m_max; }

	void SetPopPos(const D3DXVECTOR3 isPopPos) { m_popPos = isPopPos; SetPos(isPopPos); }
	D3DXVECTOR3 GetPopPos() { return m_popPos; }

	void SetEnemyType(CStage::ENEMYTYPE IsType) { m_enemyType = IsType; };
	CStage::ENEMYTYPE GetEnemyType() { return m_enemyType; };

	bool GetThrow() { return m_throw; }
	void SetThrow(bool IsThrow) { m_throw = IsThrow; }

	void SetMove(D3DXVECTOR3 move) { m_move = move; }


private:

	bool m_hit;//当たってるか
	bool m_isMove;//うごくかどうか
	bool m_get;
	bool m_throw;
	int m_nouHave;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_popPos;
	D3DXVECTOR3 m_min;
	D3DXVECTOR3 m_max;


	CStage::ENEMYTYPE m_enemyType;
	std::string m_myfilepass;

	
};

#endif // !_PENBULUM_H_

#pragma once
