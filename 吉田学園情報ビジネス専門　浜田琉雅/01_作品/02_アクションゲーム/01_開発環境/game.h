//============================
//
// ゲーム画面のヘッダー
// Author:hamada ryuuga
//
//============================
#ifndef _GAME_H_		//このマクロが定義されてなかったら
#define _GAME_H_		//2重インクルード防止のマクロ定義

#include "object.h"
#include "stage.h"

class CPause; 
class CStage;

class CGame :public CObject
{
public:
	CGame();
	~CGame();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CPause * GetPause() { return m_Pause; };

//	static CScore*GetScore() { return pScore; };

	static CStage* GetStage() { return m_Stage; }
	static int GetGameScore() { return m_GameScore; };


	void SetPatternStage(CStage::PATTERN Pattern) { m_pattern = Pattern; }
	
private:
	static CStage* m_Stage;
	static CPause *m_Pause;


	static int m_GameScore;
	CStage::PATTERN m_pattern;		//パターン

	int m_GameCount;
	int m_SpeedUp;
	int m_nCntSpawn;
};
#endif