//=============================================================================
//
// 反応設定
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _REACTION_H_			// このマクロ定義がされてなかったら
#define _REACTION_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "3dpolygon.h"
#include "texture.h"

class CObject;

class CReaction : public C3dpolygon
{
public:
	const float POPSIZE = 150.0f;//基本のsize
	const float DEFAULTSIZE = 100.0f;//基本のsize
	const int CONTRACTION = 3;//収縮回数
	static CReaction *CReaction::Create();

	CReaction(const int list);
	~CReaction() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	const D3DXVECTOR3 *GetPos() const override;
	void SetPos(const D3DXVECTOR3 &pos) override;
	void PopEvent(CObject* Enemy);//魔法陣発生
	CObject* GetEnemy() { return m_enemy; }
	void SetPopEvent(bool isPop) { m_pop = isPop; }

private:
	void move();
	CObject* m_enemy;
	bool m_pop;
	float m_size;
	int m_count;

};

#endif

