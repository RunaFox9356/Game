//============================
//
// カメラAnimation設定ヘッター
// Author:hamada ryuuga
//
//============================

#ifndef _CAMERA_ANIMATION_H_
#define _CAMERA_ANIMATION_H_

#include "renderer.h"
#include "line.h"

namespace nl = nlohmann;
class CCameraAnimation
{
public:
	CCameraAnimation();
	~CCameraAnimation();

	void Animation(D3DXVECTOR3 *posV, D3DXVECTOR3 *posR, bool*IsAnimation, bool IsLoop, std::function<void()> func);

	void Change();

	void Save(std::string);	// 画面設定
	void Load(std::string);	// 画面設定

	static CCameraAnimation*create();

	//	posVポイントを追加
	void AddPosVPoptime(const D3DXVECTOR3 IsPos) { m_posVAnimationPos.push_back(IsPos); }
	//	posVポイントをセット
	void SetPosVPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_posVAnimationPos.at(IsPoptime) = IsPos; }
	//	posVポイントをゲット
	D3DXVECTOR3 GetPosVPoptime(const int IsPoptime) { return m_posVAnimationPos.at(IsPoptime); }
	//	posVポイントのさいずの取得
	int GetPosVPoptimeSize() { return m_posVAnimationPos.size(); }


	//	PosRポイントを追加
	void AddPosRPoptime(const D3DXVECTOR3 IsPos) { m_posRAnimationPos.push_back(IsPos); }
	//	PosRポイントをセット
	void SetPosRPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_posRAnimationPos.at(IsPoptime) = IsPos; }
	//	PosRポイントをゲット
	D3DXVECTOR3 GetPosRPoptime(const int IsPoptime) { return m_posRAnimationPos.at(IsPoptime); }
	//	PosRポイントのさいずの取得
	int GetPosRPoptimeSize() { return m_posRAnimationPos.size(); }


	//	keySetポイントを追加
	void AddkeySetPoptime(const int IsPos) { keySet.push_back(IsPos); }
	//	keySetポイントをセット
	void SetkeySetPoptime(const int IsPoptime, const int IsPos) { keySet.at(IsPoptime) = IsPos; }
	//	keySetポイントをゲット
	int GetkeySetPoptime(const int IsPoptime) { return keySet.at(IsPoptime); }
	//	keySetポイントのさいずの取得
	int GetkeySetPoptimeSize() { return keySet.size(); }


	//	AnimationRポイントを追加
	void AddAnimationRPoptime(const D3DXVECTOR3 IsPos) { m_animationPoptimeR.push_back(IsPos); }
	//	AnimationRポイントをセット
	void SetAnimationRPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_animationPoptimeR.at(IsPoptime) = IsPos; }
	//	AnimationRポイントをゲット
	D3DXVECTOR3 GetAnimationRPoptime(const int IsPoptime) { return m_animationPoptimeR.at(IsPoptime); }
	//	AnimationRポイントのさいずの取得
	int GetAnimationRPoptimeSize() { return m_animationPoptimeR.size(); }

	//	AnimationRポイントを追加
	void AddAnimationVPoptime(const D3DXVECTOR3 IsPos) { m_animationPoptimeV.push_back(IsPos); }
	//	AnimationRポイントをセット
	void SetAnimationVPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_animationPoptimeV.at(IsPoptime) = IsPos; }
	//	AnimationRポイントをゲット
	D3DXVECTOR3 GetAnimationVPoptime(const int IsPoptime) { return m_animationPoptimeV.at(IsPoptime); }
	//	AnimationRポイントのさいずの取得
	int GetAnimationVPoptimeSize() { return m_animationPoptimeV.size(); }


	//	Izingポイントを追加
	void AddIzing(const bool IsIzing) { m_izing.push_back(IsIzing); }
	//	Izingポイントをセット
	void SetIzing(const int IsPoptime, const bool IsIzing) { m_izing.at(IsPoptime) = IsIzing; }
	//	Izingポイントをゲット
	bool GetIzing(const int IsPoptime) { return m_izing.at(IsPoptime); }
	//	Izingポイントのさいずの取得
	int GetIzingSize() { return m_izing.size(); }


	//	末端の削除
	void LastDelete();


	void AnimationReset();

	std::string GetName() { return m_name; }
private:

	float m_movespline;
	int m_cntspline;

	int m_cntFrame;
	int m_moveCount;

	int m_cntFrameV;
	int m_moveCountV;

	int m_cntFrameR;
	int m_moveCountR;
	std::vector <int>keySet;
	std::vector <D3DXVECTOR3> m_posVAnimationPos;
	std::vector <D3DXVECTOR3> m_posRAnimationPos;

	std::vector <D3DXVECTOR3> m_animationPoptimeV;
	std::vector <D3DXVECTOR3> m_animationPoptimeR;

	std::vector <bool> m_izing;
	std::vector <CLine*> m_lineV;
	std::vector <CLine*> m_lineR;
	D3DXVECTOR3 m_posVDest;
	D3DXVECTOR3 m_posRDest;
	D3DXVECTOR3 m_izingbaseV;
	D3DXVECTOR3 m_izingbaseR;
	std::string m_name;
	nl::json m_Camera;//リストの生成


};


#endif