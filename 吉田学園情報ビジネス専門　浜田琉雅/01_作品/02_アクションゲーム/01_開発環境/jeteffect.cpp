//============================
//
// エフェクト設定
// Author:hamada ryuuga
//
//============================

#include "jeteffect.h"
#include "hamada.h"
#include "manager.h"
#include "utility.h"
#include "sound.h"


//------------------------------------
// コンストラクタ
//------------------------------------
CJetEffect::CJetEffect(int list) :C3dpolygon(list)
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CJetEffect::~CJetEffect()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CJetEffect::Init()
{
	m_life = 100;

	C3dpolygon::Init();
	return E_NOTIMPL;
}

//------------------------------------
// 終了
//------------------------------------
void CJetEffect::Uninit()
{
	C3dpolygon::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CJetEffect::Update()
{

	//動き
	CJetEffect::move();

	C3dpolygon::Update();

}

//------------------------------------
// 描画
//------------------------------------
void CJetEffect::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();

	D3DXMATRIX mtxView;
	//アルファブレンディングを加算合成に設定
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, D3DXVECTOR3(m_rot.y, 0.0f, 0.0f),true);
	//m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, m_rot);
	C3dpolygon::Draw();

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//------------------------------------
// create
//------------------------------------
CJetEffect *CJetEffect::Create(D3DXVECTOR3 Move)
{
	CJetEffect * pObject = nullptr;
	pObject = new CJetEffect(PRIORITY_POPEFFECT);

	if (pObject != nullptr)
	{
		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_DASH);
		pObject->Init();
		pObject->SetTexture(CTexture::TEXTURE_JETEFECT);//テクスチャ選択
		pObject->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));//サイズ設定
		//pObject->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));//座標設定
		pObject->SetColar(D3DXCOLOR(0.0f, 1.0f, 1.0f, 1.0f));//色設定
		if (Move.y <= 0.0f)
		{
			Move.y = -Move.y;
		}
		pObject->SetMove(D3DXVECTOR3(-Move.x, 0.0f, -Move.z));//moveの設定
		pObject->SetLife(IntRandom(30, 10));
		//↓引数(1横の枚数,2縦の枚数,3Animation速度,４基本ゼロだけど表示するまでのタイムラグ,5無限にアニメーション再生するかどうか)
		//pObject->SetAnimation(7, 1, 0, 0, false);//Animation画像だった場合これを書く,一枚絵なら消さないとバグる
	}

	return pObject;
}

//------------------------------------
// Get＆Set 
//------------------------------------
const D3DXVECTOR3 * CJetEffect::GetPos() const
{
	return &m_pos;
}

void CJetEffect::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;
}

//------------------------------------
// 動き系統
//------------------------------------
void CJetEffect::move()
{
	m_pos += m_move;

	SetSize(m_size*1.1f);
	//動き入れたいときはここに	SetMove()で変えれるよ
	m_life--;

	if (m_life <= 0)
	{
		Uninit();
	}
}