//=============================================================================
//
// カメラ設定
// Author:hamada ryuuga
//
//=============================================================================
#include "camera_animation.h"
#include "spline.h"
#include "utility.h"
#include "hamada.h"

// ---------------------------------------------------------------------------- -
// コンストラクト
//-----------------------------------------------------------------------------
CCameraAnimation::CCameraAnimation()
{
	m_cntFrame = 0;
	m_moveCount = 0;

	m_cntFrameV = 0;
	m_moveCountV = 0;

	m_cntFrameR = 0;
	m_moveCountR = 0;

	m_movespline = 0.0f;
	m_cntspline = 0;

	m_izingbaseV = { 0.0f, 0.0f, 0.0f };
	m_izingbaseR = { 0.0f, 0.0f, 0.0f };
}

//-----------------------------------------------------------------------------
// デストラクト
//-----------------------------------------------------------------------------
CCameraAnimation::~CCameraAnimation()
{

}

//-----------------------------------------------------------------------------
// Animation処理
//-----------------------------------------------------------------------------
void CCameraAnimation::Animation(D3DXVECTOR3 *posV, D3DXVECTOR3 *posR, bool*IsAnimation, bool IsLoop, std::function<void()> func)
{
	if (!GetIzing(m_cntFrame))
	{
		if (m_movespline == 0)
		{// フレームカウントが0の時
		 // 目的の位置と向きの算出
			if (m_cntspline >= GetAnimationVPoptimeSize())
			{
				m_cntspline = 0;
			}
			m_posVDest = ((GetAnimationVPoptime(m_cntspline)) - *posV);
			m_posRDest = ((GetAnimationRPoptime(m_cntspline)) - *posR);
		}
		float key = (float)GetkeySetPoptime(m_cntFrame);
		float one = key / 10;
		// 変数宣言
		D3DXVECTOR3 addposV = m_posVDest / one;
		D3DXVECTOR3 addposR = m_posRDest / one;

		//	向きの加算
		*posV += addposV;
		*posR += addposR;
		m_movespline++;
		m_moveCount++;

		if (m_movespline >= one)
		{//splineのsize超えたら初期化
			m_cntspline++;
			m_movespline = 0.0f;
		}

		if (m_moveCount >= (float)GetkeySetPoptime(m_cntFrame))
		{//キーフレームを超えたら

			if (m_cntspline == ((GetkeySetPoptimeSize() - 1) * 10) - 1)
			{//最大数を超えたら
				m_cntspline = 0;
			}
			m_cntFrame++;
			m_moveCount = 0;
			if (m_cntFrame >= GetkeySetPoptimeSize() - 1)
			{//Animation終了m_Cntspline
				m_cntspline = 0;
				m_cntFrame = 0;
				if (!IsLoop)
				{
					if (func != nullptr)
					{
						func();
					}
					
					*IsAnimation = false;
				}
			}
		}
		else
		{

		}
	}
	else
	{

		if (m_moveCount == 0)
		{// フレームカウントが0の時
		 // 目的の位置と向きの算出
			m_posVDest = ((GetPosVPoptime(m_cntFrame)) - *posV);
			m_posRDest = ((GetPosRPoptime(m_cntFrame)) - *posR);
			//m_IzingbaseR = m_posRDest / (float)GetkeySetPoptime(m_nCntFrame);
			//m_IzingbaseV = m_posVDest / (float)GetkeySetPoptime(m_nCntFrame);
		}

		// 変数宣言イージングに必要な奴
		float ModelSize = (float)m_moveCount / (float)GetkeySetPoptime(m_cntFrame);
		float Size = hmd::easeInSine(ModelSize);

		// 1フレームの加算を算出
		m_izingbaseV = (m_posVDest * Size) / (float)GetkeySetPoptime(m_cntFrame);
		m_izingbaseR = (m_posRDest * Size) / (float)GetkeySetPoptime(m_cntFrame);

		//	向きの加算
		*posV += m_izingbaseV;
		*posR += m_izingbaseR;


		m_moveCount++;
		if (m_moveCount >= (float)GetkeySetPoptime(m_cntFrame))
		{
			m_cntFrame++;
			m_moveCount = 0;
			if (m_cntFrame >= GetkeySetPoptimeSize())
			{//Animation終了
				m_cntFrame = 0;
				*IsAnimation = false;
			}
		}
	}
}

//-----------------------------------------------------------------------------
// lineChange処理
//-----------------------------------------------------------------------------
void CCameraAnimation::Change()
{
	SetSpline(GetPosVPoptimeSize(), m_posVAnimationPos, m_animationPoptimeV);
	SetSpline(GetPosRPoptimeSize(), m_posRAnimationPos, m_animationPoptimeR);
}


//-----------------------------------------------------------------------------
// AnimationSave処理
//-----------------------------------------------------------------------------
void CCameraAnimation::Save(std::string filePass)
{
	int nIndex = 0;
	std::string MyfilePass = ("data/Camera/" + filePass + ".json");
	for (int nCnt = 0; nCnt < GetkeySetPoptimeSize(); nCnt++)
	{

		std::string name = "CAMERA";
		std::string Number = std::to_string(nIndex);
		name += Number;
		m_Camera[name] =
		{
			{ "VPOPTIME",{
				{ "X", GetPosVPoptime(nCnt).x } ,
				{ "Y", GetPosVPoptime(nCnt).y } ,
				{ "Z", GetPosVPoptime(nCnt).z } } },
				{ "RPOPTIME",{
					{ "X", GetPosRPoptime(nCnt).x } ,
					{ "Y", GetPosRPoptime(nCnt).y } ,
					{ "Z", GetPosRPoptime(nCnt).z } } },
					{ "KEY" ,GetkeySetPoptime(nCnt) },
					{ "EZING" ,GetIzing(nCnt) }
		};
		nIndex++;
	}
	m_Camera["INDEX"] = nIndex;

	auto jobj = m_Camera.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = MyfilePass.c_str();
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();

}

//-----------------------------------------------------------------------------
// AnimationLoad処理
//-----------------------------------------------------------------------------
void CCameraAnimation::Load(std::string filePass)
{
	std::string MyfilePass = ("data/Camera/" + filePass + ".json");
	std::ifstream ifs(MyfilePass.c_str());

	int nIndex = 0;

	if (ifs)
	{
		ifs >> m_Camera;
		nIndex = m_Camera["INDEX"];


		for (int nCnt = 0; nCnt < nIndex; nCnt++)
		{
			std::string name = "CAMERA";
			std::string Number = std::to_string(nCnt);
			name += Number;

			AddPosVPoptime(D3DXVECTOR3(m_Camera[name]["VPOPTIME"]["X"], m_Camera[name]["VPOPTIME"]["Y"], m_Camera[name]["VPOPTIME"]["Z"]));
			AddPosRPoptime(D3DXVECTOR3(m_Camera[name]["RPOPTIME"]["X"], m_Camera[name]["RPOPTIME"]["Y"], m_Camera[name]["RPOPTIME"]["Z"]));
			AddkeySetPoptime(m_Camera[name]["KEY"]);
			//AddIzing(false);
			AddIzing(m_Camera[name]["EZING"]);
			m_name = filePass;

		}
	}
	Change();
}

//-----------------------------------------------------------------------------
// 生成関数
//-----------------------------------------------------------------------------
CCameraAnimation * CCameraAnimation::create()
{
	CCameraAnimation * pObject = nullptr;
	pObject = new CCameraAnimation;

	if (pObject != nullptr)
	{

	}

	return pObject;
}

//-----------------------------------------------------------------------------
//　末端のデータ削除（みかん）
//-----------------------------------------------------------------------------
void CCameraAnimation::LastDelete()
{
	keySet.pop_back();
	m_posRAnimationPos.pop_back();
	m_posVAnimationPos.pop_back();
	Change();
}

//-----------------------------------------------------------------------------
// Animationに必要なデータの初期化
//-----------------------------------------------------------------------------
void CCameraAnimation::AnimationReset()
{
	m_movespline = 0.0f;
	m_moveCount = 0;
	m_cntFrame = 0;
	m_cntspline = 0;
	m_cntFrameV = 0;
	m_moveCountV = 0;
	m_cntFrameR = 0;
	m_moveCountR = 0;
}

